﻿using System.Reflection;

[assembly: AssemblyCompany( "Tecan Trading AG" )]
[assembly: AssemblyCopyright( "Copyright 2022 by Tecan Trading AG, Switzerland" )]
[assembly: AssemblyProduct( "Tecan AnIML SDK" )]

// those entries will be overridden by the build
[assembly: AssemblyVersion( "0.1.0" )]
[assembly: AssemblyFileVersion( "0.1.0" )]
[assembly: AssemblyInformationalVersion( "0.1.0" )] 