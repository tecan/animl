# Tecan AnIML SDK
This repository contains an SDK for working with AnIML using .NET. It is aimed to enable a fast development process for AnIML documents where developers can generate a type-safe environment for working with predefined techniques.

It is intended to provide the content for all operating systems that support the .NET Core or .NET Framework platform. The contents of the repository are open-source and are not limited to Tecan. No Tecan-specific code is involved.

|||
| ---------------| ----------------------------------------------------------- |
| AnIML Homepage | [https://www.animl.org](https://www.animl.org)      |
| Chat group     | [Join the group on Slack](https://join.slack.com/t/sila-standard/shared_invite/enQtNDI0ODcxMDg5NzkzLTBhOTU3N2I0NTc4NDcyMjg2ZDIwZDc1Yjg4N2FmYjZkMzljZDAyZjAwNTc5OTVjYjIwZWJjYjA0YTY0NTFiNDA)|
| Maintainer     | Georg Hinkel ([georg.hinkel@tecan.com](mailto:georg.hinkel@tecan.com)) of [Tecan](http://www.tecan.com)|


## Status
The repository is in an early alpha status.

## Cloning
Clone the repository either with HTTPs or SSH (choose the appropriate link in the clone button)
```bash
git clone https://gitlab.com/tecan/animl.git
```

As an entry point, refer to `AnIML.sln`.

### C# Style Guide

Please follow the [general C# coding guidelines](https://docs.microsoft.com/en-us/dotnet/standard/design-guidelines/).

## Development Guidelines

### Testing and Debugging
You can just execute the tests locally using dotnet test.

## Issues and Bugs
If you see an issue please feel free to file a bug on the project [list of issues](https://gitlab.com/tecan/animl/issues)

## License
This code is licensed under the [New BSD License](https://choosealicense.com/licenses/bsd-3-clause/)
