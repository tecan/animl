﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tecan.AnIML
{
    /// <summary>
    /// Denotes an item that can be signed
    /// </summary>
    public class SignableItem : ISignableItem
    {
        /// <summary>
        /// Gets or sets the Id for signatures
        /// </summary>
        public string Id { get; set; }

        /// <inheritdoc />
        public virtual string GetOrCreateId()
        {
            if(string.IsNullOrEmpty( Id ))
            {
                Id = GetType().Name + Guid.NewGuid().ToString().Replace( "-", "" );
            }
            return Id;
        }
    }
}
