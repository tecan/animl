﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tecan.AnIML
{
    /// <summary>
    /// Denotes a collection that is signable
    /// </summary>
    /// <typeparam name="T">The type of the collection</typeparam>
    public interface ISignableCollection<T> : IList<T>, ISignableItem
    {
    }
}
