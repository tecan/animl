﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tecan.AnIML.Categories
{
    /// <summary>
    /// Denotes an AnIML category
    /// </summary>
    public class Category
    {
        /// <summary>
        /// Creates a new category
        /// </summary>
        /// <param name="name">The name of the category</param>
        public Category( string name )
        {
            Name = name;
        }

        /// <summary>
        /// Creates a child category with the given name
        /// </summary>
        /// <param name="name">The name of the child category</param>
        /// <returns>A new category</returns>
        protected internal virtual Category CreateCategory( string name )
        {
            return new Category( name );
        }

        /// <summary>
        /// Creates a child series set with the given name
        /// </summary>
        /// <param name="name">The name of the series set</param>
        /// <returns>A new series set</returns>
        protected internal virtual SeriesSet CreateSeriesSet( string name )
        {
            return new SeriesSet( name );
        }

        /// <summary>
        /// Adds the provided parameter
        /// </summary>
        /// <param name="parameter">The parameter to add</param>
        /// <returns>True, if the parameter is accepted, otherwise False</returns>
        protected internal virtual bool AddParameter( Parameter parameter )
        {
            UndefinedParameters.Add( parameter );
            return true;
        }

        /// <summary>
        /// Adds the provided series set
        /// </summary>
        /// <param name="seriesSet">The series set to add</param>
        /// <returns>True, if the series set is accepted, otherwise False</returns>
        protected internal virtual bool AddSeriesSet( SeriesSet seriesSet )
        {
            UndefinedSeriesSets.Add( seriesSet );
            return true;
        }

        /// <summary>
        /// Adds the provided category
        /// </summary>
        /// <param name="subcategory">The category to add</param>
        /// <returns>True, if the category is accepted, otherwise False</returns>
        protected internal virtual bool AddCategory( Category subcategory )
        {
            UndefinedCategories.Add( subcategory );
            return true;
        }

        /// <summary>
        /// Gets the name of the category
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Gets a collection of undefined parameters
        /// </summary>
        public ICollection<Parameter> UndefinedParameters { get; } = new List<Parameter>();

        /// <summary>
        /// Gets a collection of undefined series sets
        /// </summary>
        public ICollection<SeriesSet> UndefinedSeriesSets { get; } = new List<SeriesSet>();

        /// <summary>
        /// Gets a collection of undefined categories
        /// </summary>
        public ICollection<Category> UndefinedCategories { get; } = new List<Category>();

        /// <summary>
        /// Gets a collection of all parameters
        /// </summary>
        /// <returns>A collection that can be enumerated</returns>
        protected internal virtual IEnumerable<Parameter> AllParameters() => UndefinedParameters;

        /// <summary>
        /// Gets a collection of all categories
        /// </summary>
        /// <returns>A collection that can be enumerated</returns>
        protected internal virtual IEnumerable<Category> AllCategories() => UndefinedCategories;

        /// <summary>
        /// Gets a collection of all series sets
        /// </summary>
        /// <returns>A collection that can be enumerated</returns>
        protected internal virtual IEnumerable<SeriesSet> AllSeriesSets() => UndefinedSeriesSets;
    }
}
