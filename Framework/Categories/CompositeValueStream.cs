﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tecan.AnIML.Categories
{
    /// <summary>
    /// Denotes a value stream that consists of multiple parts
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class CompositeValueStream<T> : ValueStream<T>
    {
        /// <summary>
        /// Gets a collection of child streams
        /// </summary>
        public ICollection<ValueStream<T>> Streams { get; } = new List<ValueStream<T>>();

        private bool HasLength() => Streams.All( s => s.Length.HasValue );

        /// <inheritdoc />
        public override int? Length => HasLength() ? Streams.Sum( s => s.Length.Value ) : default( int? );

        /// <inheritdoc />
        public override T[] ProduceValues( int length )
        {
            var result = new T[length];
            var index = 0;
            var remainingLength = length;
            foreach(var childStream in Streams)
            {
                var len = childStream.Length.GetValueOrDefault( remainingLength );
                var items = childStream.ProduceValues( len );
                if(items != null)
                {
                    len = items.Length;
                    Array.Copy( items, 0, result, index, len );
                }
                remainingLength -= len;
                index += len;
            }
            return result;
        }
    }
}
