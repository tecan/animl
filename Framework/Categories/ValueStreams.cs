﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tecan.AnIML.Categories
{
    /// <summary>
    /// Denotes a helper class to create value streams
    /// </summary>
    public static class ValueStreams
    {
        /// <summary>
        /// Creates a value stream for the given collection
        /// </summary>
        /// <typeparam name="T">The data type of the items</typeparam>
        /// <param name="values">The values</param>
        /// <returns>A value stream</returns>
        public static ValueStream<T> Create<T>( IEnumerable<T> values )
        {
            var data = values.ToArray();
            return new ExplicitValueStream<T>
            {
                Values = data,
            };
        }

        /// <summary>
        /// Creates a value stream for the given collection
        /// </summary>
        /// <typeparam name="T">The data type of the items</typeparam>
        /// <param name="values">The values</param>
        /// <returns>A value stream</returns>
        public static ValueStream<T> Create<T>( IEnumerable<T?> values ) where T : struct
        {
            var composite = null as CompositeValueStream<T>;
            var currentStartIndex = 0;
            var currentList = new List<T>();
            var index = 0;
            foreach(var value in values)
            {
                if(value.HasValue)
                {
                    currentList.Add( value.Value );
                }
                else
                {
                    if(currentList.Count > 0)
                    {
                        if(composite == null)
                        {
                            composite = new CompositeValueStream<T>();
                        }
                        composite.Streams.Add( new ExplicitValueStream<T>
                        {
                            StartIndex = currentStartIndex,
                            EndIndex = index - 1,
                            Values = currentList.ToArray()
                        } );
                        currentList.Clear();
                    }
                    currentStartIndex = index + 1;
                }
                index++;
            }
            if(composite != null)
            {
                if(currentList.Count > 0)
                {
                    composite.Streams.Add( new ExplicitValueStream<T>
                    {
                        StartIndex = currentStartIndex,
                        Values = currentList.ToArray()
                    } );
                }
                return composite;
            }
            return new ExplicitValueStream<T>
            {
                Values = currentList.ToArray(),
            };
        }
    }
}
