﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tecan.AnIML.Techniques.Serialization;

namespace Tecan.AnIML.Categories
{
    /// <summary>
    /// Denotes a series set with a blueprint
    /// </summary>
    public class DefinedSeriesSet : SeriesSet
    {
        private readonly SeriesSetBlueprintType _blueprint;

        /// <summary>
        /// Creates a new defined series
        /// </summary>
        /// <param name="blueprint"></param>
        public DefinedSeriesSet( SeriesSetBlueprintType blueprint ) : base( blueprint.name )
        {
            _blueprint = blueprint;
        }

        /// <summary>
        /// Gets the defined series per blueprint
        /// </summary>
        public IDictionary<SeriesBlueprintType, ICollection<Series>> DefinedSeries { get; } = new Dictionary<SeriesBlueprintType, ICollection<Series>>();

        /// <inheritdoc />
        protected internal override bool AddSeries( Series series )
        {
            var items = _blueprint.Items ?? Array.Empty<object>();
            var blueprint = items.OfType<SeriesBlueprintType>()
                .Concat( items.OfType<SeriesBlueprintChoiceType>().SelectMany( choice => choice.SeriesBlueprint ?? Array.Empty<SeriesBlueprintType>() ) )
                .FirstOrDefault( b => b.name == series.Name );

            if(blueprint != null)
            {
                if(!DefinedSeries.TryGetValue( blueprint, out var seriesOfBlueprint ))
                {
                    seriesOfBlueprint = new List<Series>();
                    DefinedSeries.Add( blueprint, seriesOfBlueprint );
                }
                seriesOfBlueprint.Add( series );
            }

            return base.AddSeries( series );
        }
    }
}
