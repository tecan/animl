﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tecan.AnIML.Categories
{
    /// <summary>
    /// Denotes a set of series that belong to the same diagram(s)
    /// </summary>
    public class SeriesSet : SignableItem
    {
        /// <summary>
        /// Creates a new series set with the given name
        /// </summary>
        /// <param name="name">The name of the series set</param>
        public SeriesSet( string name )
        {
            Name = name;
        }

        /// <summary>
        /// Gets the name of the series set
        /// </summary>
        public string Name
        {
            get;
        }

        /// <summary>
        /// Gets a collection of undefined series
        /// </summary>
        public ICollection<Series> UndefinedSeries { get; } = new List<Series>();

        /// <summary>
        /// Gets or sets the number of data points in this series set
        /// </summary>
        public int Length
        {
            get;
            set;
        }

        /// <summary>
        /// Gets a collection of all series in this series set
        /// </summary>
        /// <returns>A collection that can be iterated</returns>
        protected internal virtual IEnumerable<Series> AllSeries() => UndefinedSeries;

        /// <summary>
        /// Adds the provided series
        /// </summary>
        /// <param name="series">The series to add</param>
        /// <returns>True, if the series is accepted, otherwise False</returns>
        protected internal virtual bool AddSeries( Series series )
        {
            UndefinedSeries.Add( series );
            return true;
        }
    }
}
