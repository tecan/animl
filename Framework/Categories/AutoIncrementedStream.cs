﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Tecan.AnIML.Categories
{
    /// <summary>
    /// Denotes a value stream that is automatically incremented
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class AutoIncrementedStream<T> : ValueStream<T>
    {
        private static readonly Func<T, T, T> Add = (Func<T, T, T>)IncrementHelper.GetAddMethod( typeof( T ) ).CreateDelegate( typeof( Func<T, T, T> ) );

        /// <summary>
        /// Gets or sets the start value of the stream
        /// </summary>
        public T StartValue
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the increment of this stream
        /// </summary>
        public T Increment
        {
            get;
            set;
        }

        public override T[] ProduceValues( int length )
        {
            var result = new T[length];
            var value = StartValue;
            for(int i = 0; i < length; i++)
            {
                result[i] = value;
                value = Add( value, Increment );
            }
            return result;
        }
    }

    internal static class IncrementHelper
    {
        public static MethodInfo GetAddMethod( Type type )
        {
            if(type == typeof( int ))
            {
                return new Func<int, int, int>( AddInt32 ).Method;
            }
            else if(type == typeof( long ))
            {
                return new Func<long, long, long>( AddInt64 ).Method;
            }
            else if(type == typeof( float ))
            {
                return new Func<float, float, float>( AddFloat32 ).Method;
            }
            else if(type == typeof( double ))
            {
                return new Func<double, double, double>( AddFloat64 ).Method;
            }
            else
            {
                throw new NotSupportedException();
            }
        }

        private static int AddInt32( int a, int b )
        {
            return a + b;
        }
        private static long AddInt64( long a, long b )
        {
            return a + b;
        }
        private static float AddFloat32( float a, float b )
        {
            return a + b;
        }
        private static double AddFloat64( double a, double b )
        {
            return a + b;
        }
    }
}
