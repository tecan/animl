﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tecan.AnIML.Units;

namespace Tecan.AnIML.Categories
{
    /// <summary>
    /// Denotes extension method for series
    /// </summary>
    public static class SeriesExtensions
    {
        /// <summary>
        /// Adds the provided series to the given list of series
        /// </summary>
        /// <param name="series">The base series collection</param>
        /// <param name="name">The name of the series</param>
        /// <param name="values">The values</param>
        /// <param name="isDependent">True, if series is dependent, otherwise False</param>
        /// <param name="plotScale">The plotting scale for this series</param>
        /// <returns>An updated collection of series</returns>
        public static IEnumerable<Series> Add( this IEnumerable<Series> series, string name, ValueStream values, bool isDependent, PlotScale plotScale )
        {
            return Add( series, name, values, isDependent, plotScale, null );
        }

        /// <summary>
        /// Adds the provided series to the given list of series
        /// </summary>
        /// <param name="series">The base series collection</param>
        /// <param name="name">The name of the series</param>
        /// <param name="values">The values</param>
        /// <param name="isDependent">True, if series is dependent, otherwise False</param>
        /// <param name="plotScale">The plotting scale for this series</param>
        /// <param name="unit">The unit for this series</param>
        /// <returns>An updated collection of series</returns>
        public static IEnumerable<Series> Add( this IEnumerable<Series> series, string name, ValueStream values, bool isDependent, PlotScale plotScale, Unit unit )
        {
            if(values == null)
            {
                return series;
            }

            return series.Concat( Enumerable.Repeat( values.CreateSeries( name, isDependent, plotScale, unit ), 1 ) );
        }

        /// <summary>
        /// Adds all the provided series to the given list of series
        /// </summary>
        /// <param name="series">The base series collection</param>
        /// <param name="name">The name of the series</param>
        /// <param name="values">The values</param>
        /// <param name="isDependent">True, if series is dependent, otherwise False</param>
        /// <param name="plotScale">The plotting scale for this series</param>
        /// <returns>An updated collection of series</returns>
        public static IEnumerable<Series> AddAll( this IEnumerable<Series> series, string name, IEnumerable<ValueStream> values, bool isDependent, PlotScale plotScale )
        {
            return AddAll( series, name, values, isDependent, plotScale, null );
        }

        /// <summary>
        /// Adds all the provided series to the given list of series
        /// </summary>
        /// <param name="series">The base series collection</param>
        /// <param name="name">The name of the series</param>
        /// <param name="values">The values</param>
        /// <param name="isDependent">True, if series is dependent, otherwise False</param>
        /// <param name="plotScale">The plotting scale for this series</param>
        /// <param name="unit">The unit for this series</param>
        /// <returns>An updated collection of series</returns>
        public static IEnumerable<Series> AddAll( this IEnumerable<Series> series, string name, IEnumerable<ValueStream> values, bool isDependent, PlotScale plotScale, Unit unit )
        {
            if(values == null)
            {
                return series;
            }

            return series.Concat( values.Select( vals => vals.CreateSeries( name, isDependent, plotScale, unit ) ) );
        }
    }
}
