﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tecan.AnIML.Categories
{
    /// <summary>
    /// Denotes a helper class with extensions for categories
    /// </summary>
    public static class CategoryExtensions
    {
        /// <summary>
        /// Adds the given category to the provided collection of categories
        /// </summary>
        /// <param name="categories">The original categories</param>
        /// <param name="category">The category to add or null</param>
        /// <returns>An enumerable that will contain both the original category and the added</returns>
        public static IEnumerable<Category> Add( this IEnumerable<Category> categories, Category category )
        {
            if(category == null)
            {
                return categories;
            }
            return categories.Concat( Enumerable.Repeat( category, 1 ) );
        }

        /// <summary>
        /// Adds the given categories to the provided collection of categories
        /// </summary>
        /// <param name="categories">The original categories</param>
        /// <param name="additionalCategories">The categories to add or null</param>
        /// <returns>An enumerable that will contain both the original category and the added</returns>
        public static IEnumerable<Category> AddAll( this IEnumerable<Category> categories, IEnumerable<Category> additionalCategories )
        {
            if(additionalCategories == null)
            {
                return categories;
            }
            return categories.Concat( additionalCategories );
        }
    }
}
