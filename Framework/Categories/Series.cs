﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tecan.AnIML.Units;

namespace Tecan.AnIML.Categories
{
    /// <summary>
    /// Denotes a series of values
    /// </summary>
    public abstract class Series
    {
        /// <summary>
        /// Gets or sets the name of the series
        /// </summary>
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the id of the series
        /// </summary>
        public string SeriesId
        {
            get;
            set;
        }

        /// <summary>
        /// True, if the series is visible, otherwise false
        /// </summary>
        public bool Visible
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the unit of this series
        /// </summary>
        public Unit Unit
        {
            get;
            set;
        }

        /// <summary>
        /// True, if the series is dependent, otherwise False
        /// </summary>
        public bool IsDependent
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the plot scale of this series
        /// </summary>
        public PlotScale PlotScale
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the data type of this series
        /// </summary>
        public abstract Type Type { get; }

        /// <summary>
        /// Gets the value stream of this series as nongeneric thing
        /// </summary>
        public abstract object ValuesObject { get; }
    }

    /// <summary>
    /// Denotes a series of a given type
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Series<T> : Series
    {
        /// <summary>
        /// Gets or sets the values of this series
        /// </summary>
        public ValueStream<T> Values
        {
            get;
            set;
        }

        /// <inheritdoc />
        public override Type Type => typeof( T );

        /// <inheritdoc />
        public override object ValuesObject => Values;
    }
}
