﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tecan.AnIML.Categories
{
    /// <summary>
    /// Denotes a value stream with explicit values
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ExplicitValueStream<T> : ValueStream<T>
    {
        /// <summary>
        /// Gets or sets the values
        /// </summary>
        public T[] Values { get; set; }

        /// <inheritdoc />
        public override int? Length => Values != null ? Values.Length : 0;

        /// <inheritdoc />
        public override T[] ProduceValues( int length )
        {
            return Values;
        }
    }
}
