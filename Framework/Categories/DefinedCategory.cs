﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tecan.AnIML.Techniques.Serialization;

namespace Tecan.AnIML.Categories
{
    /// <summary>
    /// Denotes a category that references a blueprint
    /// </summary>
    public class DefinedCategory : Category
    {
        private readonly CategoryBlueprintType _blueprint;

        /// <summary>
        /// Creates a new defined category for the given blueprint
        /// </summary>
        /// <param name="blueprint"></param>
        public DefinedCategory( CategoryBlueprintType blueprint ) : base( GetName( blueprint ) )
        {
            _blueprint = blueprint;
        }

        private static string GetName( CategoryBlueprintType blueprint )
        {
            if(blueprint == null)
            {
                throw new ArgumentNullException( nameof( blueprint ) );
            }
            return blueprint.name;
        }

        /// <summary>
        /// Gets a collection of defined categories
        /// </summary>
        public ICollection<DefinedCategory> DefinedCategories { get; } = new List<DefinedCategory>();

        /// <summary>
        /// Gets a collection of defined series sets
        /// </summary>
        public ICollection<DefinedSeriesSet> DefinedSeriesSets { get; } = new List<DefinedSeriesSet>();

        /// <summary>
        /// Gets a hashtable of parameters per parameter blueprint
        /// </summary>
        public IDictionary<ParameterBlueprintType, ICollection<Parameter>> DefinedParameters { get; } = new Dictionary<ParameterBlueprintType, ICollection<Parameter>>();

        /// <inheritdoc />
        protected internal override bool AddCategory( Category subcategory )
        {
            if(subcategory is DefinedCategory definedCategory)
            {
                DefinedCategories.Add( definedCategory );
                return true;
            }
            return base.AddCategory( subcategory );
        }

        /// <inheritdoc />
        protected internal override bool AddParameter( Parameter parameter )
        {
            var blueprint = _blueprint.ParameterBlueprint?.FirstOrDefault( b => b.name == parameter.Name );
            if(blueprint != null)
            {
                if(!DefinedParameters.TryGetValue( blueprint, out var parameters ))
                {
                    parameters = new List<Parameter>();
                    DefinedParameters.Add( blueprint, parameters );
                }
                parameters.Add( parameter );
            }
            return base.AddParameter( parameter );
        }

        /// <inheritdoc />
        protected internal override bool AddSeriesSet( SeriesSet seriesSet )
        {
            if(seriesSet is DefinedSeriesSet definedSeriesSet)
            {
                DefinedSeriesSets.Add( definedSeriesSet );
            }
            return base.AddSeriesSet( seriesSet );
        }

        /// <inheritdoc />
        protected internal override IEnumerable<Category> AllCategories()
        {
            return base.AllCategories().AddAll( DefinedCategories );
        }

        /// <inheritdoc />
        protected internal override IEnumerable<Parameter> AllParameters()
        {
            return base.AllParameters().Concat( DefinedParameters.Values.SelectMany( pars => pars ) );
        }

        /// <inheritdoc />
        protected internal override IEnumerable<SeriesSet> AllSeriesSets()
        {
            return base.AllSeriesSets().Concat( DefinedSeriesSets );
        }

        /// <inheritdoc />
        protected internal override Category CreateCategory( string name )
        {
            if(_blueprint.CategoryBlueprint != null)
            {
                var blueprint = _blueprint.CategoryBlueprint.FirstOrDefault( b => b.name == name );
                if(blueprint != null)
                {
                    return new DefinedCategory( blueprint );
                }
            }
            return base.CreateCategory( name );
        }

        /// <inheritdoc />
        protected internal override SeriesSet CreateSeriesSet( string name )
        {
            if(_blueprint.SeriesSetBlueprint != null)
            {
                var blueprint = _blueprint.SeriesSetBlueprint.FirstOrDefault( b => b.name == name );
                if(blueprint != null)
                {
                    return new DefinedSeriesSet( blueprint );
                }
            }
            return base.CreateSeriesSet( name );
        }
    }
}
