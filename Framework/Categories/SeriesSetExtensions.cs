﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tecan.AnIML.Categories
{
    /// <summary>
    /// Denotes extensions for series sets
    /// </summary>
    public static class SeriesSetExtensions
    {
        /// <summary>
        /// Adds the provided series set to the given collection of series sets
        /// </summary>
        /// <param name="seriesSets">The base collection of series sets</param>
        /// <param name="seriesSet">The series set to add or null</param>
        /// <returns>An updated collection of series sets</returns>
        public static IEnumerable<SeriesSet> Add( this IEnumerable<SeriesSet> seriesSets, SeriesSet seriesSet )
        {
            if(seriesSets == null)
            {
                return seriesSets;
            }
            return seriesSets.Concat( Enumerable.Repeat( seriesSet, 1 ) );
        }
    }
}
