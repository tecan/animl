﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tecan.AnIML.Units;

namespace Tecan.AnIML.Categories
{
    /// <summary>
    /// Denotes an abstract value stream
    /// </summary>
    public abstract class ValueStream
    {
        /// <summary>
        /// Creates a series object with the given additional parameters
        /// </summary>
        /// <param name="name">the name of the series</param>
        /// <param name="isDependent">True, if the series is independent, otherwise False</param>
        /// <param name="plotScale">The scale in which the series can be printed</param>
        /// <param name="unit">The unit of the series or null if not applicable</param>
        /// <returns>The series that is created for this value stream</returns>
        public abstract Series CreateSeries( string name, bool isDependent, PlotScale plotScale, Unit unit );
    }

    /// <summary>
    /// Denotes an abstract value stream of a given data type
    /// </summary>
    /// <typeparam name="T">The data type of the value stream</typeparam>
    public abstract class ValueStream<T> : ValueStream
    {
        /// <inheritdoc />
        public override Series CreateSeries( string name, bool isDependent, PlotScale plotScale, Unit unit )
        {
            return new Series<T>
            {
                Name = name,
                IsDependent = isDependent,
                PlotScale = plotScale,
                Unit = unit,
                Values = this
            };
        }

        /// <summary>
        /// Gets or sets the start index of this stream or null if not applicable
        /// </summary>
        public int? StartIndex
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the end index of this stream or null if not applicable
        /// </summary>
        public int? EndIndex
        {
            get;
            set;
        }

        /// <inheritdoc />
        public virtual int? Length => null;

        /// <inheritdoc />
        public abstract T[] ProduceValues( int length );
    }
}
