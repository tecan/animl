﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tecan.AnIML.Units;

namespace Tecan.AnIML.Categories
{
    /// <summary>
    /// Denotes extensions for parameters
    /// </summary>
    public static class ParameterExtensions
    {
        /// <summary>
        /// Adds the provided parameter to the given list of parameters
        /// </summary>
        /// <typeparam name="T">The value type</typeparam>
        /// <param name="parameters">The base parameter collection</param>
        /// <param name="name">The name of the parameter</param>
        /// <param name="value">The value of the parameter</param>
        /// <returns>An updated collection of parameters</returns>
        public static IEnumerable<Parameter> Add<T>( this IEnumerable<Parameter> parameters, string name, T? value ) where T : struct
        {
            return parameters.Add( name, value, null );
        }

        /// <summary>
        /// Adds the provided parameter to the given list of parameters
        /// </summary>
        /// <typeparam name="T">The value type</typeparam>
        /// <param name="parameters">The base parameter collection</param>
        /// <param name="name">The name of the parameter</param>
        /// <param name="value">The value of the parameter</param>
        /// <param name="unit">The unit for this parameter</param>
        /// <returns>An updated collection of parameters</returns>
        public static IEnumerable<Parameter> Add<T>( this IEnumerable<Parameter> parameters, string name, T? value, Unit unit ) where T : struct
        {
            if(value == null)
            {
                return parameters;
            }
            if(typeof( T ).IsEnum)
            {
                return parameters.Concat( Enumerable.Repeat( new Parameter<string>
                {
                    Name = name,
                    Value = EnumParser.ToString( value.Value ),
                    Unit = unit
                }, 1 ) );
            }
            return parameters.Concat( Enumerable.Repeat( new Parameter<T>
            {
                Name = name,
                Value = value.Value,
                Unit = unit
            }, 1 ) );
        }

        /// <summary>
        /// Adds the provided parameter to the given list of parameters
        /// </summary>
        /// <typeparam name="T">The value type</typeparam>
        /// <param name="parameters">The base parameter collection</param>
        /// <param name="name">The name of the parameter</param>
        /// <param name="value">The value of the parameter</param>
        /// <returns>An updated collection of parameters</returns>
        public static IEnumerable<Parameter> Add<T>( this IEnumerable<Parameter> parameters, string name, T value )
        {
            return parameters.Add( name, value, null );
        }

        /// <summary>
        /// Adds the provided parameter to the given list of parameters
        /// </summary>
        /// <typeparam name="T">The value type</typeparam>
        /// <param name="parameters">The base parameter collection</param>
        /// <param name="name">The name of the parameter</param>
        /// <param name="value">The value of the parameter</param>
        /// <param name="unit">The unit for this parameter</param>
        /// <returns>An updated collection of parameters</returns>
        public static IEnumerable<Parameter> Add<T>( this IEnumerable<Parameter> parameters, string name, T value, Unit unit )
        {
            if(value == null)
            {
                return parameters;
            }
            if(typeof( T ) == typeof( object ))
            {
                return parameters.Concat( Enumerable.Repeat( new DynamicParameter
                {
                    Name = name,
                    ObjectValue = value,
                    Unit = unit
                }, 1 ) );
            }
            if(typeof( T ).IsEnum)
            {
                return parameters.Concat( Enumerable.Repeat( new Parameter<string>
                {
                    Name = name,
                    Value = EnumParser.ToString( value ),
                    Unit = unit
                }, 1 ) );
            }
            return parameters.Concat( Enumerable.Repeat( new Parameter<T>
            {
                Name = name,
                Value = value,
                Unit = unit
            }, 1 ) );
        }

        /// <summary>
        /// Adds all the provided parameter to the given list of parameters
        /// </summary>
        /// <typeparam name="T">The value type</typeparam>
        /// <param name="parameters">The base parameter collection</param>
        /// <param name="name">The name of the parameter</param>
        /// <param name="values">The values of the parameter</param>
        /// <returns>An updated collection of parameters</returns>
        public static IEnumerable<Parameter> AddAll<T>( this IEnumerable<Parameter> parameters, string name, IEnumerable<T> values )
        {
            return AddAll( parameters, name, values, null );
        }

        /// <summary>
        /// Adds all the provided parameter to the given list of parameters
        /// </summary>
        /// <typeparam name="T">The value type</typeparam>
        /// <param name="parameters">The base parameter collection</param>
        /// <param name="name">The name of the parameter</param>
        /// <param name="values">The values of the parameter</param>
        /// <param name="unit">The unit for this parameter</param>
        /// <returns>An updated collection of parameters</returns>
        public static IEnumerable<Parameter> AddAll<T>( this IEnumerable<Parameter> parameters, string name, IEnumerable<T> values, Unit unit )
        {
            if(values == null || !values.Any())
            {
                return parameters;
            }
            if(typeof( T ) == typeof( object ))
            {
                return parameters.Concat( values.Select( value => new DynamicParameter
                {
                    Name = name,
                    ObjectValue = value,
                    Unit = unit
                } ) );
            }
            if(typeof( T ).IsEnum)
            {
                return parameters.Concat( values.Select( v => new Parameter<string>
                {
                    Name = name,
                    Value = EnumParser.ToString( v ),
                    Unit = unit
                } ) );
            }
            return parameters.Concat( values.Select( v => new Parameter<T>
            {
                Name = name,
                Value = v,
                Unit = unit
            } ) );
        }
    }
}
