﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tecan.AnIML.Categories
{
    /// <summary>
    /// Denotes a scale in which a series should be plotted
    /// </summary>
    public enum PlotScale
    {
        /// <summary>
        /// No scale, just factorial values
        /// </summary>
        None,
        /// <summary>
        /// Linear scale
        /// </summary>
        Linear,
        /// <summary>
        /// Logarithmic scale with base 10
        /// </summary>
        Log,
        /// <summary>
        /// Logarithmic scale with base e
        /// </summary>
        Ln
    }
}
