﻿using System;
using System.Collections.Generic;
using System.Text;
using Tecan.AnIML.Units;

namespace Tecan.AnIML.Categories
{
    /// <summary>
    /// Denotes a parameter
    /// </summary>
    public abstract class Parameter
    {
        /// <summary>
        /// Gets or sets the name of the parameter
        /// </summary>
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the object value
        /// </summary>
        public abstract object ObjectValue
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the type of the parameter
        /// </summary>
        public abstract Type Type
        {
            get;
        }

        /// <summary>
        /// Gets or sets the unit of the parameter
        /// </summary>
        public Unit Unit
        {
            get;
            set;
        }
    }

    /// <summary>
    /// Denotes a parameter of a certain type
    /// </summary>
    /// <typeparam name="T">The type of the parameter</typeparam>
    public class Parameter<T> : Parameter
    {
        /// <summary>
        /// Gets or sets the value of the parameter
        /// </summary>
        public T Value
        {
            get;
            set;
        }

        /// <inheritdoc />
        public override object ObjectValue
        {
            get => Value;
            set => Value = (T)value;
        }

        /// <inheritdoc />
        public override Type Type => typeof( T );
    }

    internal class DynamicParameter : Parameter
    {
        public override object ObjectValue { get; set; }

        public override Type Type => ObjectValue?.GetType();
    }
}
