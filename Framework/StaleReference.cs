﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tecan.AnIML
{
    /// <summary>
    /// Denotes a stale reference
    /// </summary>
    public class StaleReference : ISignableItem
    {
        private readonly string _id;


        /// <summary>
        /// Creates a new stale reference to the provided id
        /// </summary>
        /// <param name="id">The referenced id</param>
        public StaleReference( string id )
        {
            _id = id;
        }

        /// <inheritdoc />
        public string Id { get => _id; }

        /// <inheritdoc />
        public string GetOrCreateId()
        {
            return _id;
        }
    }
}
