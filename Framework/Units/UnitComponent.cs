﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tecan.AnIML.Units
{
    /// <summary>
    /// Denotes a component of a unit
    /// </summary>
    public class UnitComponent
    {
        /// <summary>
        /// Gets or sets the dimension of the unit
        /// </summary>
        public SiUnit Dimension { get; set; }

        /// <summary>
        /// Gets or sets the exponent of the unit
        /// </summary>
        public double Exponent { get; set; }

        /// <summary>
        /// Gets or sets the factor of the unit
        /// </summary>
        public double Factor { get; set; } = 1;

        /// <summary>
        /// Gets or sets the offset of the unit
        /// </summary>
        public double Offset { get; set; }
    }
}
