﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tecan.AnIML.Units
{
    /// <summary>
    /// Declares that the provided field maps to a certain unit definition
    /// </summary>
    [AttributeUsage( AttributeTargets.Field )]
    public class UnitAttribute : Attribute
    {
        /// <summary>
        /// The XML representation of the unit
        /// </summary>
        public string Unit
        {
            get;
        }

        /// <summary>
        /// Initializes a new unit
        /// </summary>
        /// <param name="unit">The XML string for the unit</param>
        public UnitAttribute( string unit )
        {
            Unit = unit;
        }
    }
}
