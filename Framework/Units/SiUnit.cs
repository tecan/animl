﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tecan.AnIML.Units
{
    /// <summary>
    /// Denotes the different SI Units
    /// </summary>
    public enum SiUnit
    {
        /// <summary>
        /// Dimensionless
        /// </summary>
        One,
        Meter,
        Kilograms,
        Seconds,
        Ampere,
        Kelvin,
        Mole,
        Candela,
    }
}
