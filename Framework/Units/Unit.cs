﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tecan.AnIML.Units
{
    /// <summary>
    /// Denotes a unit
    /// </summary>
    public class Unit
    {
        /// <summary>
        /// The label of the unit
        /// </summary>
        public string Label
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the quantity
        /// </summary>
        public string Quantity
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets an array of unit components
        /// </summary>
        public UnitComponent[] Components
        {
            get;
            set;
        }
    }
}
