﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Tecan.AnIML.Serialization;

namespace Tecan.AnIML.Units
{
    /// <summary>
    /// Denotes a helper class to serialize and deserialize units
    /// </summary>
    public static class UnitSerializer
    {
        private static readonly XmlSerializer _unitSerializer = new XmlSerializer( typeof( UnitType ) );
        private static readonly Dictionary<object, Unit> _units = new Dictionary<object, Unit>();
        private static readonly Dictionary<string, object> _forwards = new Dictionary<string, object>();

        /// <summary>
        /// Parses the provided unit description into a unit
        /// </summary>
        /// <param name="unitDescription">The XML representation of a unit</param>
        /// <returns>The parsed unit</returns>
        public static Unit FromString( string unitDescription )
        {
            using(var reader = new StringReader( unitDescription ))
            {
                return DeserializeUnit( (UnitType)_unitSerializer.Deserialize( reader ) );
            }
        }

        /// <summary>
        /// Converts the provided enumeration value into a unit
        /// </summary>
        /// <typeparam name="TEnum">The enum type</typeparam>
        /// <param name="value">The enumeration value</param>
        /// <returns>A unit</returns>
        /// <exception cref="ArgumentException">Thrown if the value has no unit attached to it</exception>
        public static Unit FromEnum<TEnum>( TEnum value ) where TEnum : struct
        {
            if(!_units.TryGetValue( value, out var unit ))
            {
                var type = typeof( TEnum );
                if(!type.IsEnum)
                {
                    throw new ArgumentException( "EnumerationValue must be of Enum type", "enumerationValue" );
                }

                MemberInfo[] memberInfo = type.GetMember( value.ToString() );
                if(memberInfo != null && memberInfo.Length > 0)
                {
                    var unitAttribute = memberInfo[0].GetCustomAttribute<UnitAttribute>( false );
                    if(unitAttribute != null)
                    {
                        unit = FromString( unitAttribute.Unit );
                        _units.Add( value, unit );
                        _forwards.Add( GetLookupKey( unit, type ), value );
                        return unit;
                    }
                }
                return null;
            }
            return unit;
        }

        private static string GetLookupKey( Unit unit, Type type )
        {
            return type.FullName + "." + unit.Label;
        }

        /// <summary>
        /// Converts the provided unit into an enumeration value
        /// </summary>
        /// <typeparam name="TEnum">The enumeration type</typeparam>
        /// <param name="unit">The unit</param>
        /// <returns>The enumeration value</returns>
        public static TEnum ToEnum<TEnum>( Unit unit ) where TEnum : struct
        {
            var type = typeof( TEnum );
            if(!_forwards.TryGetValue( GetLookupKey( unit, type ), out var value ))
            {
                var members = type.GetFields();
                foreach(var member in members)
                {
                    var unitAttribute = member.GetCustomAttribute<UnitAttribute>( false );
                    if(unitAttribute != null)
                    {
                        var unitOfMember = FromString( unitAttribute.Unit );
                        if(unitOfMember.Label == unit.Label)
                        {
                            value = member.GetValue( null );
                            _units.Add( value, unitOfMember );
                            _forwards.Add( GetLookupKey( unit, type ), value );
                            break;
                        }
                    }
                }
            }
            return (TEnum)value;
        }

        /// <summary>
        /// Gets a string representation of the provided unit
        /// </summary>
        /// <param name="unit">The unit</param>
        /// <returns>A string reperesentation</returns>
        public static string ToString( Unit unit )
        {
            using(var sw = new StringWriter())
            {
                using(var xmlWriter = XmlWriter.Create( sw, new XmlWriterSettings
                {
                    OmitXmlDeclaration = true,
                } ))
                {
                    _unitSerializer.Serialize( xmlWriter, SerializeUnit( unit ) );
                    return sw.ToString();
                }
            }
        }


        internal static UnitType SerializeUnit( Unit unit )
        {
            if(unit == null)
            {
                return null;
            }

            return new UnitType
            {
                label = unit.Label,
                quantity = unit.Quantity,
                SIUnit = unit.Components.Select( SerializeComponent ).ToArray()
            };
        }

        private static SIUnitType SerializeComponent( UnitComponent component )
        {
            return new SIUnitType
            {
                exponent = component.Exponent,
                factor = component.Factor,
                offset = component.Offset,
                Value = ConvertSiUnit( component.Dimension )
            };
        }

        private static SIUnitNameList ConvertSiUnit( SiUnit dimension )
        {
            switch(dimension)
            {
                case SiUnit.One:
                    return SIUnitNameList.Item1;
                case SiUnit.Meter:
                    return SIUnitNameList.m;
                case SiUnit.Kilograms:
                    return SIUnitNameList.kg;
                case SiUnit.Seconds:
                    return SIUnitNameList.s;
                case SiUnit.Ampere:
                    return SIUnitNameList.A;
                case SiUnit.Kelvin:
                    return SIUnitNameList.K;
                case SiUnit.Mole:
                    return SIUnitNameList.mol;
                case SiUnit.Candela:
                    return SIUnitNameList.cd;
                default:
                    throw new ArgumentOutOfRangeException( nameof( dimension ) );
            }
        }

        private static SiUnit ConvertSiUnit( SIUnitNameList dimension )
        {
            switch(dimension)
            {
                case SIUnitNameList.Item1:
                    return SiUnit.One;
                case SIUnitNameList.m:
                    return SiUnit.Meter;
                case SIUnitNameList.kg:
                    return SiUnit.Kilograms;
                case SIUnitNameList.s:
                    return SiUnit.Seconds;
                case SIUnitNameList.A:
                    return SiUnit.Ampere;
                case SIUnitNameList.K:
                    return SiUnit.Kelvin;
                case SIUnitNameList.mol:
                    return SiUnit.Mole;
                case SIUnitNameList.cd:
                    return SiUnit.Candela;
                default:
                    throw new ArgumentOutOfRangeException( nameof( dimension ) );
            }
        }

        internal static Unit DeserializeUnit( UnitType unit )
        {
            if(unit == null)
            {
                return null;
            }

            return new Unit
            {
                Label = unit.label,
                Quantity = unit.quantity,
                Components = unit.SIUnit?.Select( DeserializeComponent ).ToArray()
            };
        }

        private static UnitComponent DeserializeComponent( SIUnitType component )
        {
            return new UnitComponent
            {
                Dimension = ConvertSiUnit( component.Value ),
                Exponent = component.exponent,
                Factor = component.factor,
                Offset = component.offset,
            };
        }
    }
}
