﻿using System;
using System.Collections.Generic;
using System.Text;
using Tecan.AnIML.ExperimentSteps;

namespace Tecan.AnIML.AuditTrail
{
    /// <summary>
    /// Denotes an entry of an audit trail
    /// </summary>
    public class AuditTrailEntry : SignableItem
    {
        /// <summary>
        /// Gets or sets the author of this audit trail entry
        /// </summary>
        public Author Author { get; set; }

        /// <summary>
        /// Gets or sets the software that was used to write this audit trail entry
        /// </summary>
        public Software Software { get; set; }

        /// <summary>
        /// Gets or sets the action that was audited
        /// </summary>
        public AuditTrailEntryAction Action { get; set; }

        /// <summary>
        /// Gets or sets a comment for the audited action
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// Gets or sets the reason why this action was performed
        /// </summary>
        public string Reason { get; set; }

        /// <summary>
        /// Gets or sets the timestamp of the audit trail
        /// </summary>
        public DateTime Timestamp { get; set; }

        /// <summary>
        /// Gets a collection of referenced elements for this audit trail
        /// </summary>
        public ICollection<ISignableItem> References { get; } = new List<ISignableItem>();

        /// <summary>
        /// Gets a collection of differences this audit trail represents
        /// </summary>
        public ICollection<AuditedDiff> Diffs { get; } = new List<AuditedDiff>();
    }
}
