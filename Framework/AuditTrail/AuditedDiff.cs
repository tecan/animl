﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tecan.AnIML.AuditTrail
{
    /// <summary>
    /// Denotes an audited diff
    /// </summary>
    public class AuditedDiff
    {
        /// <summary>
        /// Gets or sets the an element that is audited
        /// </summary>
        public ISignableItem Reference { get; set; }

        /// <summary>
        /// Gets or sets the old value
        /// </summary>
        public string OldValue { get; set; }

        /// <summary>
        /// Gets or sets the new value
        /// </summary>
        public string NewValue { get; set; }

        /// <summary>
        /// True, if the scope is an element, false if for attributes
        /// </summary>
        public bool IsElementScope { get; set; }
    }
}
