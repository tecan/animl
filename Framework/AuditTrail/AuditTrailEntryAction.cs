﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tecan.AnIML.AuditTrail
{
    /// <summary>
    /// Denotes an action represented by an audited trail entry
    /// </summary>
    public enum AuditTrailEntryAction
    {
        /// <summary>
        /// Denotes that an element has been created
        /// </summary>
        Created,

        /// <summary>
        /// Denotes that an element has been modified
        /// </summary>
        Modified,

        /// <summary>
        /// Denotes a conversion of an element
        /// </summary>
        Converted,

        /// <summary>
        /// Denotes that a read has taken place
        /// </summary>
        Read,

        /// <summary>
        /// Denotes that an element has been signed
        /// </summary>
        Signed,

        /// <summary>
        /// Denotes the deletion of an element
        /// </summary>
        Deleted,
    }
}
