﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Xml;

namespace Tecan.AnIML.AuditTrail
{
    /// <summary>
    /// Denotes a signature
    /// </summary>
    public class Signature
    {
        private XmlElement _serializedSignature;

        /// <summary>
        /// The key used for this signature, can be null if the signature is only read
        /// </summary>
        public AsymmetricAlgorithm Key { get; set; }

        /// <summary>
        /// The key info provided by the signature
        /// </summary>
        public KeyInfo KeyInfo { get; set; }

        /// <summary>
        /// Gets a collection of elements that have been signed
        /// </summary>
        public ICollection<ISignableItem> SignedItems { get; } = new List<ISignableItem>();

        private Signature( XmlElement signature )
        {
            _serializedSignature = signature;
        }

        /// <summary>
        /// Creates a signature from the given Xml entry
        /// </summary>
        /// <param name="signature">The signature element</param>
        /// <param name="referenceResolver">A function that resolves ids</param>
        /// <returns>A signature instance</returns>
        /// <exception cref="ArgumentException">Thrown if the reference contains URIs that do not start with a fragment</exception>
        public static Signature CreateFrom( XmlElement signature, Func<string, ISignableItem> referenceResolver )
        {
            Signature sig = new Signature( signature );
            var signedXml = new SignedXml( signature );
            sig.KeyInfo = signedXml.KeyInfo;
            foreach(Reference reference in signedXml.SignedInfo.References)
            {
                if(reference.Uri != null && reference.Uri.StartsWith( "#" ))
                {
                    var resolved = referenceResolver( reference.Uri.Substring( 1 ) );
                    if(resolved != null)
                    {
                        sig.SignedItems.Add( resolved );
                    }
                }
                else
                {
                    throw new ArgumentException( $"Signing reference '{reference.Uri}' is not supported." );
                }
            }
            return sig;
        }

        /// <summary>
        /// Creates a new signature
        /// </summary>
        public Signature()
        {
        }

        /// <summary>
        /// True, if the element already has a signature, otherwise false
        /// </summary>
        public bool HasSignature => _serializedSignature != null;

        /// <summary>
        /// Gets the current signature element
        /// </summary>
        public XmlElement CurrentSignature => _serializedSignature;

        /// <summary>
        /// Create a new signature for the provided XML document
        /// </summary>
        /// <param name="document">The XML document on the basis of which to create the signature</param>
        /// <returns>The signature element</returns>
        public XmlElement CreateSignature( XmlDocument document )
        {
            var signedXml = new SignedXml( document );
            signedXml.SigningKey = Key;
            foreach(var signed in SignedItems)
            {
                var reference = new Reference();
                reference.AddTransform( new XmlDsigC14NTransform() );
                reference.Uri = "#" + signed.GetOrCreateId();
                signedXml.AddReference( reference );
            }
            if(KeyInfo == null)
            {
                if(Key is RSA rsaKey)
                {
                    signedXml.KeyInfo.AddClause( new RSAKeyValue( rsaKey ) );
                }
            }
            else
            {
                signedXml.KeyInfo = KeyInfo;
            }
            signedXml.ComputeSignature();
            return _serializedSignature = signedXml.GetXml();
        }
    }
}
