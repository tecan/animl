﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tecan.AnIML
{
    /// <summary>
    /// Denotes an item that can be signed
    /// </summary>
    public interface ISignableItem
    {
        /// <summary>
        /// Gets the Identifier
        /// </summary>
        string Id { get; }

        /// <summary>
        /// Gets or creates an Identifier
        /// </summary>
        /// <remarks>If the Id is null, this method makes a modification and sets the Id to a distinct value.</remarks>
        /// <returns>The Identfier</returns>
        string GetOrCreateId();
    }
}
