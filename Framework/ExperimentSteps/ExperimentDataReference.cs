﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tecan.AnIML.ExperimentSteps
{
    /// <summary>
    /// Denotes a reference to the experiment data of a different experiment step
    /// </summary>
    public class ExperimentDataReference : SignableItem
    {

        /// <summary>
        /// Creates a new sample reference for a given role
        /// </summary>
        /// <param name="role">The role of the sample</param>
        public ExperimentDataReference( string role )
        {
            Role = role;
        }

        /// <summary>
        /// Gets the name of the role
        /// </summary>
        public string Role
        {
            get;
        }

        /// <summary>
        /// Gets or sets the purpose of the sample reference
        /// </summary>
        public Purpose Purpose
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the referenced experiment step
        /// </summary>
        public ExperimentStep ExperimentStep
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the prefix of all bulk referenced experiment steps
        /// </summary>
        public string ExperimentStepPrefix
        {
            get;
            set;
        }
    }
}
