﻿using System;
using System.Collections.Generic;
using System.Text;
using Tecan.AnIML.Categories;

namespace Tecan.AnIML.ExperimentSteps
{
    /// <summary>
    /// Denotes a result
    /// </summary>
    public class Result : SignableItem
    {
        /// <summary>
        /// Creates a new result
        /// </summary>
        /// <param name="name">The name of the result</param>
        public Result( string name )
        {
            Name = name;
        }

        /// <summary>
        /// Gets or sets the series set of the result
        /// </summary>
        public SeriesSet SeriesSet
        {
            get;
            set;
        }

        /// <summary>
        /// Gets a collection of child steps
        /// </summary>
        public ICollection<ExperimentStep> Steps { get; } = new List<ExperimentStep>();

        /// <summary>
        /// Gets a collection of undefined categories
        /// </summary>
        public ICollection<Category> UndefinedCategories { get; } = new List<Category>();

        /// <summary>
        /// Gets the name of this result
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Adds the provided category
        /// </summary>
        /// <param name="category">The category to add</param>
        /// <returns>True, if the category is accepted, otherwise False</returns>
        protected internal virtual bool AddCategory( Category category )
        {
            UndefinedCategories.Add( category );
            return true;
        }

        /// <summary>
        /// Creates a new category
        /// </summary>
        /// <param name="name">The name of the category</param>
        /// <returns>A new category</returns>
        protected internal virtual Category CreateCategory( string name )
        {
            return new Category( name );
        }

        /// <summary>
        /// Gets a collection of all categories for this result
        /// </summary>
        /// <returns>A collection that can be iterated</returns>
        protected internal virtual IEnumerable<Category> AllCategories() => UndefinedCategories;

        internal bool AddStep( ExperimentStep step )
        {
            Steps.Add( step );
            return true;
        }

        /// <summary>
        /// Creates the series set with the given name
        /// </summary>
        /// <param name="name">the name of the series set</param>
        /// <returns>The created series set</returns>
        protected internal virtual SeriesSet CreateSeriesSet( string name )
        {
            return new SeriesSet( name );
        }
    }
}
