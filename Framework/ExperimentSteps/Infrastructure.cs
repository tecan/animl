﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tecan.AnIML.ExperimentSteps
{
    /// <summary>
    /// Denotes an infrastructure
    /// </summary>
    public class Infrastructure : SignableItem
    {
        /// <summary>
        /// Gets a collection of undefined sample references
        /// </summary>
        public SignableList<SampleReference> UndefinedSampleReferences { get; } = new SignableList<SampleReference>();

        /// <summary>
        /// Gets a collection of undefined experiment data references
        /// </summary>
        public SignableList<ExperimentDataReference> UndefinedExperimentDataReferences { get; } = new SignableList<ExperimentDataReference>();

        /// <summary>
        /// Gets or sets the timestamp of this infrastructure
        /// </summary>
        public DateTime? Timestamp { get; set; }

        /// <summary>
        /// Adds the provided sample reference
        /// </summary>
        /// <param name="reference">The sample reference to add</param>
        /// <returns>True, if the sample reference is accepted, otherwise False</returns>
        protected internal virtual bool AddReferencedSample( SampleReference reference )
        {
            UndefinedSampleReferences.Add( reference );
            return true;
        }

        /// <summary>
        /// Adds the provided experiment data reference
        /// </summary>
        /// <param name="reference">The experiment data reference</param>
        /// <returns>True, if the experiment data reference is accepted, otherwise False</returns>
        protected internal virtual bool AddExperimentDataReference( ExperimentDataReference reference )
        {
            UndefinedExperimentDataReferences.Add( reference );
            return true;
        }

        /// <summary>
        /// Creates a new sample reference for the provided role
        /// </summary>
        /// <param name="role">The role</param>
        /// <returns>A new sample reference</returns>
        protected internal virtual SampleReference CreateSampleReference( string role )
        {
            return new SampleReference( role );
        }

        /// <summary>
        /// Creates a new experiment data reference for the provided role
        /// </summary>
        /// <param name="role">The role</param>
        /// <returns>A new experiment data reference</returns>
        protected internal virtual ExperimentDataReference CreateExperimentDataReference( string role )
        {
            return new ExperimentDataReference( role );
        }

        /// <summary>
        /// Gets a collection of all sample references for this infrastructure
        /// </summary>
        /// <returns>A collection that can be iterated</returns>
        protected internal virtual IEnumerable<SampleReference> AllSampleReferences() => UndefinedSampleReferences;

        /// <summary>
        /// Gets a collection of all experiment data references for this infrastructure
        /// </summary>
        /// <returns>A collection that can be iterated</returns>
        protected internal virtual IEnumerable<ExperimentDataReference> AllExperimentDataReferences() => UndefinedExperimentDataReferences;
    }
}
