﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tecan.AnIML.ExperimentSteps
{
    /// <summary>
    /// Helper class to deal with collections of results
    /// </summary>
    public static class ResultExtensions
    {

        /// <summary>
        /// Adds the given result to the provided collection of results
        /// </summary>
        /// <param name="results">The original results</param>
        /// <param name="result">The result to add or null</param>
        /// <returns>An enumerable that will contain both the original result and the added</returns>
        public static IEnumerable<Result> Add( this IEnumerable<Result> results, Result result )
        {
            if(result == null)
            {
                return results;
            }
            return results.Concat( Enumerable.Repeat( result, 1 ) );
        }

        /// <summary>
        /// Adds the given results to the provided collection of results
        /// </summary>
        /// <param name="results">The original results</param>
        /// <param name="additionalresults">The results to add or null</param>
        /// <returns>An enumerable that will contain both the original result and the added</returns>
        public static IEnumerable<Result> AddAll( this IEnumerable<Result> results, IEnumerable<Result> additionalresults )
        {
            if(additionalresults == null)
            {
                return results;
            }
            return results.Concat( additionalresults );
        }
    }
}
