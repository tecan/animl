﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tecan.AnIML.Techniques.Serialization;

namespace Tecan.AnIML.ExperimentSteps
{
    /// <summary>
    /// Denotes a defined infrastructure
    /// </summary>
    public class DefinedInfrastructure : Infrastructure
    {
        private readonly SampleRoleBlueprintType[] _blueprints;

        /// <summary>
        /// Creates a new defined infrastructure for the given sample roles
        /// </summary>
        /// <param name="blueprints">The sample roles</param>
        public DefinedInfrastructure( SampleRoleBlueprintType[] blueprints )
        {
            _blueprints = blueprints;
        }

        /// <summary>
        /// Gets a dictionary of defined sample references per sample role blueprint
        /// </summary>
        public IDictionary<SampleRoleBlueprintType, ICollection<DefinedSampleReference>> DefinedSampleReferences { get; } = new Dictionary<SampleRoleBlueprintType, ICollection<DefinedSampleReference>>();

        /// <inheritdoc />
        protected internal override bool AddReferencedSample( SampleReference reference )
        {
            if(reference is DefinedSampleReference definedSampleReference)
            {
                var blueprint = _blueprints.FirstOrDefault( b => b.name == reference.Role );
                if(blueprint != null)
                {
                    if(!DefinedSampleReferences.TryGetValue( blueprint, out var definedSampleReferences ))
                    {
                        definedSampleReferences = new List<DefinedSampleReference>();
                        DefinedSampleReferences.Add( blueprint, definedSampleReferences );
                    }
                    definedSampleReferences.Add( definedSampleReference );
                    return true;
                }
            }
            return base.AddReferencedSample( reference );
        }

        /// <inheritdoc />
        protected internal override IEnumerable<SampleReference> AllSampleReferences()
        {
            return base.AllSampleReferences().AddAll( DefinedSampleReferences.Values.SelectMany( r => r ) );
        }

        /// <inheritdoc />
        protected internal override SampleReference CreateSampleReference( string role )
        {
            var blueprint = _blueprints.FirstOrDefault( b => b.name == role );
            if(blueprint != null)
            {
                return new DefinedSampleReference( blueprint );
            }
            return base.CreateSampleReference( role );
        }
    }
}
