﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tecan.AnIML.ExperimentSteps
{
    /// <summary>
    /// Denotes a technique
    /// </summary>
    public class Technique
    {
        /// <summary>
        /// Gets or sets the name of the technique
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the uri of the technique
        /// </summary>
        public Uri Uri { get; set; }

        /// <summary>
        /// Gets or sets the checksum of the technique
        /// </summary>
        public string Checksum { get; set; }

        /// <summary>
        /// Initializes a new technique
        /// </summary>
        public Technique()
        {
        }

        /// <summary>
        /// Initializes a new technique
        /// </summary>
        /// <param name="name">The name of the technique</param>
        /// <param name="uri">The uri of the technique</param>
        /// <param name="checksum">The SHA256 hash of the technique</param>
        public Technique( string name, string uri, string checksum )
        {
            Name = name;
            Uri = new Uri( uri, UriKind.RelativeOrAbsolute );
            Checksum = checksum;
        }
    }
}
