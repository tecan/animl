﻿using System;
using System.Collections.Generic;
using System.Text;
using Tecan.AnIML.Categories;

namespace Tecan.AnIML.ExperimentSteps
{
    /// <summary>
    /// Denotes the description of a step
    /// </summary>
    public class StepDescription : SignableItem
    {
        /// <summary>
        /// Gets a collection of tags
        /// </summary>
        public IDictionary<string, string> Tags { get; } = new Dictionary<string, string>();

        /// <summary>
        /// Gets or sets the comment of this step
        /// </summary>
        public string Comment
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the source data (e.g. script file)
        /// </summary>
        public string SourceDataLocation
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the name of the step
        /// </summary>
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the technique that was used
        /// </summary>
        public Technique Technique { get; set; }

        /// <summary>
        /// Gets or sets the infrastructure that was used for this step
        /// </summary>
        public Infrastructure Infrastructure { get; set; }

        /// <summary>
        /// Gets or sets the method used for this step
        /// </summary>
        public Method Method { get; set; }

        /// <summary>
        /// Gets a collection of undefined results
        /// </summary>
        public ICollection<Result> UndefinedResults { get; } = new List<Result>();

        /// <summary>
        /// Creates the infrastructure for this step
        /// </summary>
        /// <returns>A new infrastructure object</returns>
        protected internal virtual Infrastructure CreateInfrastructure()
        {
            return new Infrastructure();
        }

        /// <summary>
        /// Creates the method object for this step
        /// </summary>
        /// <returns>A new Method instance</returns>
        protected internal virtual Method CreateMethod()
        {
            return new Method();
        }

        /// <summary>
        /// Create a new result object
        /// </summary>
        /// <param name="name">The name of the result</param>
        /// <returns>A new result object</returns>
        protected internal virtual Result CreateResult( string name )
        {
            return new Result( name );
        }

        /// <summary>
        /// Adds the provided result
        /// </summary>
        /// <param name="result">The result to add</param>
        /// <returns>True, if the result is accepted, otherwise False</returns>
        protected internal virtual bool AddResult( Result result )
        {
            UndefinedResults.Add( result );
            return true;
        }

        /// <summary>
        /// Gets all results for this step
        /// </summary>
        /// <returns>A collection that can be iterated</returns>
        protected internal virtual IEnumerable<Result> AllResults() => UndefinedResults;
    }
}
