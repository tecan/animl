﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tecan.AnIML.ExperimentSteps
{
    /// <summary>
    /// Denotes an author of a step or audit trail entry
    /// </summary>
    public class Author
    {
        /// <summary>
        /// Gets or sets the name of the author
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the affiliation of the author
        /// </summary>
        public string Affiliation { get; set; }

        /// <summary>
        /// Gets or sets the email address of the author
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the location of the author
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        /// Gets or sets the phone number of the author
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Gets or sets the role of the author
        /// </summary>
        public string Role { get; set; }

        /// <summary>
        /// Gets or sets the type of the author
        /// </summary>
        public AuthorType Type { get; set; }
    }
}
