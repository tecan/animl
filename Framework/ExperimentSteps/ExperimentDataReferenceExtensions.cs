﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tecan.AnIML.ExperimentSteps
{
    /// <summary>
    /// Denotes extensions for sample references
    /// </summary>
    public static class ExperimentDataReferenceExtensions
    {
        /// <summary>
        /// Adds the provided experiment data reference to the collection of experiment data references
        /// </summary>
        /// <param name="experimentDataReferences">The base collection of experiment data references</param>
        /// <param name="dataReference">The experiment data reference to add</param>
        /// <returns>An updated collection of experiment data references</returns>
        public static IEnumerable<ExperimentDataReference> Add( this IEnumerable<ExperimentDataReference> experimentDataReferences, ExperimentDataReference dataReference )
        {
            if(dataReference == null)
            {
                return experimentDataReferences;
            }
            return experimentDataReferences.Concat( Enumerable.Repeat( dataReference, 1 ) );
        }

        /// <summary>
        /// Adds the provided experiment data references to the collection of experiment data references
        /// </summary>
        /// <param name="experimentDataReferences">The base collection of sample references</param>
        /// <param name="additionalReferences">The experiment data references to add</param>
        /// <returns>An updated collection of experiment data references</returns>
        public static IEnumerable<ExperimentDataReference> AddAll( this IEnumerable<ExperimentDataReference> experimentDataReferences, IEnumerable<ExperimentDataReference> additionalReferences )
        {
            if(additionalReferences == null)
            {
                return experimentDataReferences;
            }
            return experimentDataReferences.Concat( additionalReferences );
        }
    }
}
