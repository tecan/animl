﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tecan.AnIML.ExperimentSteps
{
    /// <summary>
    /// Denotes the type of authors
    /// </summary>
    public enum AuthorType
    {
        /// <summary>
        /// Denotes that the author is a human
        /// </summary>
        Human,

        /// <summary>
        /// Denotes that the author is a device
        /// </summary>
        Device,

        /// <summary>
        /// Denotes that the author is a software
        /// </summary>
        Software,
    }
}
