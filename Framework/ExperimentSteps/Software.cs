﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tecan.AnIML.ExperimentSteps
{
    /// <summary>
    /// Denotes a software that was used
    /// </summary>
    public class Software : SignableItem
    {
        /// <summary>
        /// Gets or sets the manufacturer of the software
        /// </summary>
        public string Manufacturer { get; set; }

        /// <summary>
        /// Gets or sets the name of the software
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the operating system under which the software was operating
        /// </summary>
        public string OperatingSystem { get; set; }

        /// <summary>
        /// Gets the sets the version of the software
        /// </summary>
        public string Version { get; set; }
    }
}
