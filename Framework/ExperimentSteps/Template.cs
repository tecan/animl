﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tecan.AnIML.ExperimentSteps
{
    /// <summary>
    /// Denotes a template
    /// </summary>
    public class Template : StepDescription
    {
        /// <summary>
        /// Gets or sets the template Id
        /// </summary>
        public string TemplateId
        {
            get;
            set;
        }
    }
}
