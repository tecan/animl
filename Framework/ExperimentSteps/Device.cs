﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tecan.AnIML.ExperimentSteps
{
    /// <summary>
    /// Denotes a device
    /// </summary>
    public class Device
    {
        /// <summary>
        /// Gets or sets the identifier of the device
        /// </summary>
        public string Identifier { get; set; }

        /// <summary>
        /// Gets or sets the firmware version of the device
        /// </summary>
        public string FirmwareVersion { get; set; }

        /// <summary>
        /// Gets or sets the manufacturer of the device
        /// </summary>
        public string Manufacturer { get; set; }

        /// <summary>
        /// Gets or sets the name of the device
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the serial number of the device
        /// </summary>
        public string SerialNumber { get; set; }
    }
}
