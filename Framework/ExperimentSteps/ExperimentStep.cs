﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tecan.AnIML.ExperimentSteps
{
    /// <summary>
    /// Denotes an experiment step
    /// </summary>
    public class ExperimentStep : StepDescription
    {
        /// <summary>
        /// Gets or sets the id of the experiment step
        /// </summary>
        public string ExperimentStepId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the id of the used template
        /// </summary>
        public Template TemplateUsed
        {
            get;
            set;
        }
    }
}
