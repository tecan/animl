﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tecan.AnIML.Categories;
using Tecan.AnIML.Samples;

namespace Tecan.AnIML.ExperimentSteps
{
    /// <summary>
    /// Denotes a reference to a sample
    /// </summary>
    public class SampleReference : SignableItem
    {
        private Sample _sample;

        /// <summary>
        /// Creates a new sample reference for a given role
        /// </summary>
        /// <param name="role">The role of the sample</param>
        public SampleReference( string role )
        {
            Role = role;
        }

        /// <summary>
        /// Gets the name of the role
        /// </summary>
        public string Role
        {
            get;
        }

        /// <summary>
        /// Gets or sets the purpose of the sample reference
        /// </summary>
        public Purpose Purpose
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the sample that is referenced
        /// </summary>
        public Sample Sample
        {
            get => _sample;
            set
            {
                if(_sample != value)
                {
                    _sample?.Dereference( this );
                    _sample = value;
                    _sample?.Reference( this );
                }
            }
        }

        /// <summary>
        /// True, if the sample reference is inherited, otherwise False
        /// </summary>
        public bool IsInherited
        {
            get;
            set;
        }

        /// <summary>
        /// Creates a new category with the given name
        /// </summary>
        /// <param name="name">The name of the category</param>
        /// <returns>A new category</returns>
        protected internal virtual Category CreateCategory( string name )
        {
            return null;
        }

        /// <summary>
        /// Adds the provided category
        /// </summary>
        /// <param name="category">The category to add</param>
        /// <returns>True, if the category is accepted, otherwise False</returns>
        protected internal virtual bool AddCategory( Category category )
        {
            return false;
        }

        /// <summary>
        /// Gets all categories for this sample reference
        /// </summary>
        /// <returns>A collection that can be iterated</returns>
        protected internal virtual IEnumerable<Category> AllCategories()
        {
            return Enumerable.Empty<Category>();
        }
    }
}
