﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tecan.AnIML.Categories;
using Tecan.AnIML.Techniques.Serialization;

namespace Tecan.AnIML.ExperimentSteps
{
    public class DefinedResult : Result
    {
        private readonly ResultBlueprintType _blueprint;

        public DefinedResult( ResultBlueprintType blueprint ) : base( blueprint.name )
        {
            _blueprint = blueprint;
        }

        public ICollection<DefinedCategory> DefinedCategories { get; } = new List<DefinedCategory>();


        /// <inheritdoc />
        protected internal override bool AddCategory( Category subcategory )
        {
            if(subcategory is DefinedCategory definedCategory)
            {
                DefinedCategories.Add( definedCategory );
                return true;
            }
            return base.AddCategory( subcategory );
        }

        /// <inheritdoc />
        protected internal override IEnumerable<Category> AllCategories()
        {
            return base.AllCategories().AddAll( DefinedCategories );
        }

        /// <inheritdoc />
        protected internal override Category CreateCategory( string name )
        {
            if(_blueprint.CategoryBlueprint != null)
            {
                var blueprint = _blueprint.CategoryBlueprint.FirstOrDefault( b => b.name == name );
                if(blueprint != null)
                {
                    return new DefinedCategory( blueprint );
                }
            }
            return base.CreateCategory( name );
        }
    }
}
