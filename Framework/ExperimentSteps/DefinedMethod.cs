﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tecan.AnIML.Categories;
using Tecan.AnIML.Techniques.Serialization;

namespace Tecan.AnIML.ExperimentSteps
{
    /// <summary>
    /// Denotes a method that is aware of category blueprints
    /// </summary>
    public class DefinedMethod : Method
    {
        private readonly CategoryBlueprintType[] _blueprints;

        /// <summary>
        /// Creates a defined method for the provided blueprints
        /// </summary>
        /// <param name="blueprints"></param>
        public DefinedMethod( CategoryBlueprintType[] blueprints )
        {
            _blueprints = blueprints;
        }

        /// <summary>
        /// Gets a collection of defined categories
        /// </summary>
        public ICollection<DefinedCategory> DefinedCategories { get; } = new List<DefinedCategory>();


        /// <inheritdoc />
        protected internal override bool AddCategory( Category subcategory )
        {
            if(subcategory is DefinedCategory definedCategory)
            {
                DefinedCategories.Add( definedCategory );
                return true;
            }
            return base.AddCategory( subcategory );
        }

        /// <inheritdoc />
        protected internal override IEnumerable<Category> AllCategories()
        {
            return base.AllCategories().AddAll( DefinedCategories );
        }

        /// <inheritdoc />
        protected internal override Category CreateCategory( string name )
        {
            if(_blueprints != null)
            {
                var blueprint = _blueprints.FirstOrDefault( b => b.name == name );
                if(blueprint != null)
                {
                    return new DefinedCategory( blueprint );
                }
            }
            return base.CreateCategory( name );
        }
    }
}
