﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tecan.AnIML.ExperimentSteps
{
    /// <summary>
    /// Denotes extensions for sample references
    /// </summary>
    public static class SampleReferenceExtensions
    {
        /// <summary>
        /// Adds the provided sample reference to the collection of sample references
        /// </summary>
        /// <param name="sampleReferences">The base collection of sample references</param>
        /// <param name="sampleReference">The sample reference to add</param>
        /// <returns>An updated collection of sample references</returns>
        public static IEnumerable<SampleReference> Add( this IEnumerable<SampleReference> sampleReferences, SampleReference sampleReference )
        {
            if(sampleReference == null)
            {
                return sampleReferences;
            }
            return sampleReferences.Concat( Enumerable.Repeat( sampleReference, 1 ) );
        }

        /// <summary>
        /// Adds the provided sample references to the collection of sample references
        /// </summary>
        /// <param name="sampleReferences">The base collection of sample references</param>
        /// <param name="additionalReferences">The sample references to add</param>
        /// <returns>An updated collection of sample references</returns>
        public static IEnumerable<SampleReference> AddAll( this IEnumerable<SampleReference> sampleReferences, IEnumerable<SampleReference> additionalReferences )
        {
            if(additionalReferences == null)
            {
                return sampleReferences;
            }
            return sampleReferences.Concat( additionalReferences );
        }
    }
}
