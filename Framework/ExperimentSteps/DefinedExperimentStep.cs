﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tecan.AnIML.Techniques.Serialization;

namespace Tecan.AnIML.ExperimentSteps
{
    /// <summary>
    /// Denotes an experiment step that is based on a technique definition
    /// </summary>
    public class DefinedExperimentStep : ExperimentStep
    {
        private readonly TechniqueType _technique;

        /// <summary>
        /// Creates a new defined experiment step
        /// </summary>
        /// <param name="technique">The technique this step is referencing</param>
        /// <exception cref="ArgumentNullException"></exception>
        public DefinedExperimentStep( TechniqueType technique )
        {
            if(technique == null)
            {
                throw new ArgumentNullException( nameof( technique ) );
            }

            _technique = technique;
        }

        /// <inheritdoc />
        protected internal override Infrastructure CreateInfrastructure()
        {
            if(_technique.SampleRoleBlueprint == null)
            {
                return base.CreateInfrastructure();
            }
            return new DefinedInfrastructure( _technique.SampleRoleBlueprint );
        }

        /// <inheritdoc />
        protected internal override Method CreateMethod()
        {
            if(_technique.MethodBlueprint == null)
            {
                return base.CreateMethod();
            }
            return new DefinedMethod( _technique.MethodBlueprint );
        }

        /// <inheritdoc />
        protected internal override Result CreateResult( string name )
        {
            var blueprint = _technique.ResultBlueprint?.FirstOrDefault( r => r.name == name );
            if(blueprint != null)
            {
                return new DefinedResult( blueprint );
            }
            return base.CreateResult( name );
        }
    }
}
