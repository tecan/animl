﻿using System;
using System.Collections.Generic;
using System.Text;
using Tecan.AnIML.Categories;

namespace Tecan.AnIML.ExperimentSteps
{
    /// <summary>
    /// Denotes a method
    /// </summary>
    public class Method : SignableItem
    {
        /// <summary>
        /// Gets or sets the name of the method
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets a collection of undefined categories
        /// </summary>
        public ICollection<Category> UndefinedCategories { get; } = new List<Category>();

        /// <summary>
        /// Gets or sets the author of the method
        /// </summary>
        public Author Author { get; set; }

        /// <summary>
        /// Gets or sets the device of the method
        /// </summary>
        public Device Device { get; set; }

        /// <summary>
        /// Gets or sets the software used in the method
        /// </summary>
        public Software Software { get; set; }

        /// <summary>
        /// Adds the provided category
        /// </summary>
        /// <param name="category">The provided category</param>
        /// <returns>True, if the category is accepted, otherwise False</returns>
        protected internal virtual bool AddCategory( Category category )
        {
            UndefinedCategories.Add( category );
            return true;
        }

        /// <summary>
        /// Creates a new category with the provided name
        /// </summary>
        /// <param name="name">The name of the category</param>
        /// <returns>A new category</returns>
        protected internal virtual Category CreateCategory( string name )
        {
            return new Category( name );
        }

        protected internal virtual IEnumerable<Category> AllCategories() => UndefinedCategories;
    }
}
