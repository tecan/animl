﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tecan.AnIML.Samples
{
    /// <summary>
    /// Describes what kind of container the current sample is.
    /// </summary>
    public enum ContainerType
    {
        /// <summary>
        /// This container holds exactly one sample (e.g. well, vial, tube).
        /// </summary>
        Simple,
        /// <summary>
        /// Positions within this container are precisely defined and known in advance (e.g. racks, ...)
        /// </summary>
        Determinate,
        /// <summary>
        /// Positions within this container are not precisely defined or known in advance (e.g. gels, surfaces)
        /// </summary>
        Indeterminate,
        /// <summary>
        /// Rectangular tray with unknown (or other) number of positions.
        /// </summary>
        RectangularTray,
        /// <summary>
        /// Microtiter plate or tray with 6 positions.
        /// </summary>
        Plate6,
        /// <summary>
        /// Microtiter plate or tray with 24 positions.
        /// </summary>
        Plate24,
        /// <summary>
        /// Microtiter plate or tray with 96 positions.
        /// </summary>
        Plate96,
        /// <summary>
        /// Microtiter plate or tray with 384 positions.
        /// </summary>
        Plate384,
        /// <summary>
        /// Microtiter plate or tray with 1536 positions.
        /// </summary>
        Plate1536,
    }
}
