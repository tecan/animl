﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tecan.AnIML.Samples
{
    /// <summary>
    /// Denotes a sample position
    /// </summary>
    public struct SamplePosition
    {
        private const int AlphabetLength = 25;

        /// <summary>
        /// Gets the column for this position
        /// </summary>
        public int Column
        {
            get;
        }

        /// <summary>
        /// Gets the row for this position
        /// </summary>
        public int Row
        {
            get;
        }

        /// <summary>
        /// Creates a new sample position
        /// </summary>
        /// <param name="column">The column</param>
        /// <param name="row">The row</param>
        public SamplePosition(int column, int row) : this()
        {
            Column = column;
            Row = row;
        }

        /// <summary>
        /// Gets a string representation of this sample position
        /// </summary>
        /// <returns>A string representation</returns>
        public override string ToString()
        {
            return PrintColumns(Column) + (Row + 1).ToString();
        }

        private string PrintColumns(int columns)
        {
            if (columns < AlphabetLength)
            {
                return ((char)('A' + columns)).ToString();
            }
            return PrintColumns(columns / AlphabetLength) + PrintColumns(columns % AlphabetLength);
        }

        /// <summary>
        /// Tries to parse the provided input as sample position
        /// </summary>
        /// <param name="input">The input string</param>
        /// <param name="samplePosition">The sample position</param>
        /// <returns>True, if the parsing was successful, otherwise False</returns>
        /// <exception cref="ArgumentNullException">Thrown if the input string is null</exception>
        public static bool TryParse(string input, out SamplePosition samplePosition)
        {
            if (input == null)
            {
                throw new ArgumentNullException(nameof(input));
            }

            if (input.Length < 2)
            {
                samplePosition = default;
                return false;
            }

            var column = 0;
            var index = 0;
            while (char.IsLetter(input[index]))
            {
                column = (column * AlphabetLength) + ParseColumn(input[index]);
                index++;
            }
            if (index == 0 || input.Length < index + 2 || !int.TryParse(input.Substring(index), out var row))
            {
                samplePosition = default;
                return false;
            }
            samplePosition = new SamplePosition(column, row - 1);
            return true;
        }

        /// <summary>
        /// Parses the provided input string to a sample position
        /// </summary>
        /// <param name="input">The input string</param>
        /// <returns>The parsed sample position</returns>
        /// <exception cref="ArgumentException">Thrown if the input string is not a valid sample position</exception>
        public static SamplePosition Parse(string input)
        {
            if (TryParse(input, out SamplePosition position))
            {
                return position;
            }
            throw new ArgumentException($"{input} is not a valid sample position.", nameof(input));
        }

        private static int ParseColumn(char columnChar)
        {
            return (columnChar - 'A');
        }
    }
}
