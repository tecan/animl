﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tecan.AnIML.Categories;
using Tecan.AnIML.ExperimentSteps;

namespace Tecan.AnIML.Samples
{
    /// <summary>
    /// Individual Sample, referenced from other parts of this AnIML document.
    /// </summary>
    public class Sample : SignableItem
    {
        private string _customPositionInContainer;
        private readonly Dictionary<SampleReference, int> _references = new Dictionary<SampleReference, int>();

        /// <summary>
        /// Gets a collection of tags for the sample
        /// </summary>
        public IDictionary<string, string> Tags
        {
            get;
        } = new Dictionary<string, string>();

        /// <summary>
        /// Gets or sets the name of the sample
        /// </summary>
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the id of the sample
        /// </summary>
        public string SampleId
        {
            get;
            set;
        }

        /// <summary>
        /// Value of barcode label that is attached to sample container.
        /// </summary>
        public string Barcode
        {
            get;
            set;
        }

        /// <summary>
        /// Sample ID of container in which this sample is located.
        /// </summary>
        public string Container
        {
            get;
            set;
        }

        internal void Dereference( SampleReference sampleReference )
        {
            if(_references.TryGetValue( sampleReference, out var references ))
            {
                references--;
                if(references == 0)
                {
                    _references.Remove( sampleReference );
                }
                else
                {
                    _references[sampleReference] = references;
                }
            }
        }

        internal void Reference( SampleReference sampleReference )
        {
            if(_references.TryGetValue( sampleReference, out var reference ))
            {
                _references[sampleReference] = reference + 1;
            }
            else
            {
                _references.Add( sampleReference, 1 );
                if(string.IsNullOrEmpty( SampleId ))
                {
                    SampleId = Guid.NewGuid().ToString();
                }
            }
        }

        /// <summary>
        /// Whether this sample is also a container for other samples. Set to "simple" if not.
        /// </summary>
        public ContainerType ContainerType
        {
            get;
            set;
        }

        /// <summary>
        /// Coordinates of this sample within the enclosing container. In case of microplates or trays, the row is identified by letters and the column is identified by numbers (1-based) while in landscape orientation. Examples: A10 = 1st row, 10th column, Z1 = 26th row, 1st column, AB2 = 28th row, 2nd column.
        /// </summary>
        public SamplePosition? PositionInContainer
        {
            get;
            set;
        }

        /// <summary>
        /// Coordinates of this sample within the enclosing container. In case of microplates or trays, the row is identified by letters and the column is identified by numbers (1-based) while in landscape orientation. Examples: A10 = 1st row, 10th column, Z1 = 26th row, 1st column, AB2 = 28th row, 2nd column.
        /// </summary>
        public string PositionInContainerString
        {
            get => PositionInContainer.HasValue ? PositionInContainer.ToString() : _customPositionInContainer;
            set
            {
                if(SamplePosition.TryParse( value, out var parsedPosition ))
                {
                    PositionInContainer = parsedPosition;
                }
                else
                {
                    _customPositionInContainer = value;
                    PositionInContainer = null;
                }
            }
        }

        public ICollection<Category> UndefinedCategories { get; } = new List<Category>();

        internal bool AddCategory( Category category )
        {
            foreach(var reference in _references.Keys)
            {
                if(reference.AddCategory( category ))
                {
                    return true;
                }
            }
            UndefinedCategories.Add( category );
            return true;
        }

        internal IEnumerable<Category> AllCategories()
        {
            return UndefinedCategories.Concat( _references.Keys.SelectMany( r => r.AllCategories() ) );
        }

        /// <summary>
        /// Unstructured text comment to further describe the Sample.
        /// </summary>
        public string Comment
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the source data location for this sample
        /// </summary>
        public string SourceDataLocation
        {
            get;
            set;
        }

        /// <summary>
        /// Indicates whether this is a derived Sample. A derived Sample is a Sample that has been created by applying a Technique. (Sub-Sampling, Processing, ...)
        /// </summary>
        public bool IsDerived
        {
            get;
            set;
        }
    }
}
