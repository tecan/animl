﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tecan.AnIML
{
    /// <summary>
    /// Denotes a list that can be signed
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class SignableList<T> : List<T>, ISignableCollection<T>
    {
        /// <summary>
        /// Gets or sets the Id for signatures
        /// </summary>
        public string Id { get; set; }

        /// <inheritdoc />
        public string GetOrCreateId()
        {
            if(string.IsNullOrEmpty( Id ))
            {
                Id = typeof( T ).Name + "s" + Guid.NewGuid().ToString().Replace( "-", "" );
            }
            return Id;
        }
    }
}
