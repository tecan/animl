﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tecan.AnIML
{
    /// <summary>
    /// Denotes a helper class to deal with enumerations
    /// </summary>
    public static class EnumParser
    {
        private static readonly Dictionary<Type, EnumInfo> _enums = new Dictionary<Type, EnumInfo>();

        /// <summary>
        /// Parses the provided value into an enumeration
        /// </summary>
        /// <typeparam name="T">The enumeration type</typeparam>
        /// <param name="value">The value</param>
        /// <returns>An enumeration value</returns>
        public static T Parse<T>( object value ) where T : struct
        {
            return (T)(GetEnumInfo( typeof( T ) ).Parse( value ));
        }

        /// <summary>
        /// Gets a string representation of the enumeration value
        /// </summary>
        /// <typeparam name="T">The enumeration type</typeparam>
        /// <param name="value">The value</param>
        /// <returns>A string representation</returns>
        public static string ToString<T>( T value )
        {
            return GetEnumInfo( typeof( T ) ).ToString( value );
        }

        private static EnumInfo GetEnumInfo( Type type )
        {
            if(_enums.TryGetValue( type, out EnumInfo enumInfo ))
            {
                return enumInfo;
            }

            if(!type.IsEnum)
            {
                throw new ArgumentOutOfRangeException( "Expected an enumeration type", nameof( type ) );
            }

            lock(_enums)
            {
                if(_enums.TryGetValue( type, out EnumInfo enumInfo2 ))
                {
                    return enumInfo2;
                }

                enumInfo2 = new EnumInfo( type );
                _enums.Add( type, enumInfo2 );
                return enumInfo2;
            }
        }

        private class EnumInfo
        {
            private readonly Dictionary<object, object> _forwardDict = new Dictionary<object, object>();
            private readonly Dictionary<object, string> _reverseDict = new Dictionary<object, string>();

            public EnumInfo( Type type )
            {
            }

            public object Parse( object value )
            {
                if(value != null && _forwardDict.TryGetValue( value, out object obj ))
                {
                    return obj;
                }
                return null;
            }

            public string ToString( object value )
            {
                if(value != null && _reverseDict.TryGetValue( value, out string repr ))
                {
                    return repr;
                }
                return value?.ToString();
            }
        }
    }
}
