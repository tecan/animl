﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tecan.AnIML
{
    /// <summary>
    /// Denotes the purpose of a sample
    /// </summary>
    public enum Purpose
    {
        /// <summary>
        /// Denotes that the sample is produced
        /// </summary>
        Produced,
        /// <summary>
        /// Denotes that the sample is consumed
        /// </summary>
        Consumed
    }
}
