﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using Tecan.AnIML.AuditTrail;
using Tecan.AnIML.ExperimentSteps;
using Tecan.AnIML.Samples;
using Tecan.AnIML.Serialization;

namespace Tecan.AnIML
{
    /// <summary>
    /// Denotes an AnIML document
    /// </summary>
    public class AniMlDocument
    {
        private readonly SignableList<AuditTrailEntry> _auditTrailEntries = new SignableList<AuditTrailEntry>();

        /// <summary>
        /// Gets the collection of samples in this document
        /// </summary>
        public SignableList<Sample> Samples { get; } = new SignableList<Sample>();

        /// <summary>
        /// Gets the collection of templates in this document
        /// </summary>
        public IList<Template> Templates { get; } = new List<Template>();

        /// <summary>
        /// Gets the collection of steps in this document
        /// </summary>
        public SignableList<ExperimentStep> Steps { get; } = new SignableList<ExperimentStep>();

        /// <summary>
        /// Gets the audit trail
        /// </summary>
        public IReadOnlyList<AuditTrailEntry> AuditTrailEntries => _auditTrailEntries.AsReadOnly();

        /// <summary>
        /// Gets the signatures on this document
        /// </summary>
        public IList<Signature> Signatures { get; } = new List<Signature>();

        /// <summary>
        /// Writes the provided entry to the audit trail
        /// </summary>
        /// <param name="entry">The entry to write</param>
        /// <returns>True if the message was written, otherwise False</returns>
        public bool WriteToAuditTrail( AuditTrailEntry entry )
        {
            if(entry == null)
            {
                return false;
            }
            _auditTrailEntries.Add( entry );
            return true;
        }

        internal bool AddSample( Sample sample )
        {
            Samples.Add( sample );
            return true;
        }

        internal bool AddTemplate( Template template )
        {
            Templates.Add( template );
            return true;
        }

        internal bool AddStep( ExperimentStep step )
        {
            Steps.Add( step );
            return true;
        }

        internal bool AddSignature( Signature signature )
        {
            Signatures.Add( signature );
            return true;
        }

        /// <summary>
        /// Saves the document to the specified location
        /// </summary>
        /// <param name="path">The location where to save the document to</param>
        public void Save( string path )
        {
            var serializer = new AnIMLSerializer();
            using(var fs = File.Create( path ))
            {
                serializer.Serialize( this, fs );
            }
        }

        /// <summary>
        /// Loads the document contained in the given path
        /// </summary>
        /// <param name="path">The path of the file</param>
        /// <returns>The deserialized AnIML document</returns>
        public static AniMlDocument LoadFrom( string path )
        {
            var serializer = new AnIMLSerializer();
            using(var fs = File.OpenRead( path ))
            {
                return serializer.Deserialize( fs );
            }
        }
    }
}
