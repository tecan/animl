﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Tecan.AnIML.Categories;
using Tecan.AnIML.Units;

namespace Tecan.AnIML.Serialization
{
    internal class CategorySerializer
    {
        private static readonly Dictionary<Type, ParameterTypeType> _typeNames = new Dictionary<Type, ParameterTypeType>
        {
            [typeof( string )] = ParameterTypeType.String,
            [typeof( int )] = ParameterTypeType.Int32,
            [typeof( long )] = ParameterTypeType.Int64,
            [typeof( float )] = ParameterTypeType.Float32,
            [typeof( double )] = ParameterTypeType.Float64,
            [typeof( DateTime )] = ParameterTypeType.DateTime,
            [typeof( bool )] = ParameterTypeType.Boolean,
            [typeof( XmlElement )] = ParameterTypeType.EmbeddedXML
        };

        private static readonly Dictionary<Type, ItemsChoiceType> _itemsChoiceTypes = new Dictionary<Type, ItemsChoiceType>
        {
            [typeof( string )] = ItemsChoiceType.S,
            [typeof( int )] = ItemsChoiceType.I,
            [typeof( long )] = ItemsChoiceType.L,
            [typeof( float )] = ItemsChoiceType.F,
            [typeof( double )] = ItemsChoiceType.D,
            [typeof( DateTime )] = ItemsChoiceType.DateTime,
            [typeof( bool )] = ItemsChoiceType.Boolean,
            [typeof( XmlElement )] = ItemsChoiceType.EmbeddedXML
        };

        public CategoryType Serialize( Category category )
        {
            if(category == null)
            {
                throw new ArgumentNullException( nameof( category ) );
            }

            return new CategoryType
            {
                name = category.Name,
                Category = category.AllCategories().Select( Serialize ).ToArray(),
                Parameter = category.AllParameters().Select( Serialize ).ToArray(),
                SeriesSet = category.AllSeriesSets().Select( Serialize ).ToArray()
            };
        }

        public Category Deserialize( CategoryType category )
        {
            return Deserialize( category, null );
        }

        private Category Deserialize( CategoryType category, Func<string, Category> categoryResolver )
        {
            if(category == null)
            {
                throw new ArgumentNullException( nameof( category ) );
            }

            var result = categoryResolver?.Invoke( category.name ) ?? new Category( category.name );
            SerializationHelper.DeserializeAll( category.Parameter, result.AddParameter, Deserialize );
            SerializationHelper.DeserializeAll( category.SeriesSet, result.AddSeriesSet, result.CreateSeriesSet, Deserialize );
            SerializationHelper.DeserializeAll( category.Category, result.AddCategory, result.CreateCategory, Deserialize );
            return result;
        }

        private ParameterType Serialize( Parameter parameter )
        {
            if(parameter == null)
            {
                throw new ArgumentNullException( nameof( parameter ) );
            }

            return new ParameterType
            {
                name = parameter.Name,
                parameterType = ConvertParameterType( parameter.Type ),
                Unit = UnitSerializer.SerializeUnit( parameter.Unit ),
                Items = new[] { parameter.ObjectValue },
                ItemsElementName = new[] { _itemsChoiceTypes[parameter.Type] }
            };
        }

        private Parameter Deserialize( ParameterType parameter )
        {
            if(parameter == null)
            {
                throw new ArgumentNullException( nameof( parameter ) );
            }

            switch(parameter.parameterType)
            {
                case ParameterTypeType.Int32:
                    return new Parameter<int>
                    {
                        Name = parameter.name,
                        Value = (int)parameter.Items[0],
                        Unit = UnitSerializer.DeserializeUnit( parameter.Unit )
                    };
                case ParameterTypeType.Int64:
                    return new Parameter<long>
                    {
                        Name = parameter.name,
                        Value = (long)parameter.Items[0],
                        Unit = UnitSerializer.DeserializeUnit( parameter.Unit )
                    };
                case ParameterTypeType.Float32:
                    return new Parameter<float>
                    {
                        Name = parameter.name,
                        Value = (float)parameter.Items[0],
                        Unit = UnitSerializer.DeserializeUnit( parameter.Unit )
                    };
                case ParameterTypeType.Float64:
                    return new Parameter<double>
                    {
                        Name = parameter.name,
                        Value = (double)parameter.Items[0],
                        Unit = UnitSerializer.DeserializeUnit( parameter.Unit )
                    };
                case ParameterTypeType.String:
                    return new Parameter<string>
                    {
                        Name = parameter.name,
                        Value = (string)parameter.Items[0],
                        Unit = UnitSerializer.DeserializeUnit( parameter.Unit )
                    };
                case ParameterTypeType.Boolean:
                    return new Parameter<bool>
                    {
                        Name = parameter.name,
                        Value = (bool)parameter.Items[0],
                        Unit = UnitSerializer.DeserializeUnit( parameter.Unit )
                    };
                case ParameterTypeType.DateTime:
                    return new Parameter<DateTime>
                    {
                        Name = parameter.name,
                        Value = (DateTime)parameter.Items[0],
                        Unit = UnitSerializer.DeserializeUnit( parameter.Unit )
                    };
                case ParameterTypeType.EmbeddedXML:
                case ParameterTypeType.PNG:
                case ParameterTypeType.SVG:
                    throw new NotSupportedException();
                default:
                    throw new InvalidOperationException();
            }
        }

        private ParameterTypeType ConvertParameterType( Type type )
        {
            if(_typeNames.TryGetValue( type, out var parameterType ))
            {
                return parameterType;
            }
            else if(type.IsEnum)
            {
                return ParameterTypeType.String;
            }
            else
            {
                throw new ArgumentOutOfRangeException( nameof( type ) );
            }
        }

        public SeriesSetType Serialize( SeriesSet seriesSet )
        {
            if(seriesSet == null)
            {
                return null;
            }

            return new SeriesSetType
            {
                name = seriesSet.Name,
                length = seriesSet.Length,
                id = seriesSet.Id,
                Series = seriesSet.AllSeries().Select( Serialize ).ToArray()
            };
        }

        public SeriesSet Deserialize( SeriesSetType seriesSet, Func<string, SeriesSet> seriesSetResolver )
        {
            if(seriesSet == null)
            {
                throw new ArgumentNullException( nameof( seriesSet ) );
            }
            var result = seriesSetResolver?.Invoke( seriesSet.name ) ?? new SeriesSet( seriesSet.name );
            result.Length = seriesSet.length;
            result.Id = seriesSet.id;
            SerializationHelper.DeserializeAll( seriesSet.Series, result.AddSeries, Deserialize );
            return result;
        }

        public SeriesType Serialize( Series series )
        {
            if(series == null)
            {
                return null;
            }

            return new SeriesType
            {
                seriesID = series.SeriesId,
                Unit = UnitSerializer.SerializeUnit( series.Unit ),
                visible = series.Visible,
                seriesType = ConvertParameterType( series.Type ),
                plotScale = ConvertPlotScale( series.PlotScale ),
                name = series.Name,
                dependency = series.IsDependent ? DependencyType.dependent : DependencyType.independent,
                Items = CreateSeriesMethods( series )
            };
        }

        private object[] CreateSeriesMethods( Series series )
        {
            var items = new List<object>();
            switch(series)
            {
                case Series<string> stringSeries:
                    AddValueGenerator( stringSeries.Values, items, null, ItemsChoiceType.S );
                    return items.ToArray();
                case Series<int> intSeries:
                    AddValueGenerator( intSeries.Values, items, sizeof( int ), ItemsChoiceType.S );
                    return items.ToArray();
                case Series<long> longSeries:
                    AddValueGenerator( longSeries.Values, items, sizeof( long ), ItemsChoiceType.L );
                    return items.ToArray();
                case Series<bool> boolSeries:
                    AddValueGenerator( boolSeries.Values, items, null, ItemsChoiceType.Boolean );
                    return items.ToArray();
                case Series<double> doubleSeries:
                    AddValueGenerator( doubleSeries.Values, items, sizeof( double ), ItemsChoiceType.D );
                    return items.ToArray();
                case Series<float> floatSeries:
                    AddValueGenerator( floatSeries.Values, items, sizeof( float ), ItemsChoiceType.F );
                    return items.ToArray();
                case Series<XmlElement> stringSeries:
                    AddValueGenerator( stringSeries.Values, items, null, ItemsChoiceType.EmbeddedXML );
                    return items.ToArray();
                default:
                    throw new NotSupportedException();
            }
        }

        private void AddValueGenerator<T>( ValueStream<T> values, List<object> target, int? size, ItemsChoiceType choice )
        {
            switch(values)
            {
                case AutoIncrementedStream<T> autoIncrement:
                    target.Add( new AutoIncrementedValueSetType
                    {
                        startIndex = autoIncrement.StartIndex.GetValueOrDefault(),
                        startIndexSpecified = autoIncrement.StartIndex.HasValue,
                        endIndex = autoIncrement.EndIndex.GetValueOrDefault(),
                        endIndexSpecified = autoIncrement.EndIndex.HasValue,
                        StartValue = new StartValueType
                        {
                            Items = new object[] { autoIncrement.StartValue },
                            ItemsElementName = new[] { choice }
                        },
                        Increment = new IncrementType
                        {
                            Items = new object[] { autoIncrement.Increment },
                            ItemsElementName = new[] { choice }
                        }
                    } );
                    break;
                case ExplicitValueStream<T> explicitValues:
                    if(size.HasValue)
                    {
                        var vals = new byte[explicitValues.Values.Length * size.Value];
                        Buffer.BlockCopy( explicitValues.Values, 0, vals, 0, vals.Length );
                        target.Add( new EncodedValueSetType
                        {
                            startIndex = explicitValues.StartIndex.GetValueOrDefault(),
                            startIndexSpecified = explicitValues.StartIndex.HasValue,
                            endIndex = explicitValues.EndIndex.GetValueOrDefault(),
                            endIndexSpecified = explicitValues.EndIndex.HasValue,
                            Value = vals
                        } );
                    }
                    else
                    {
                        target.Add( new IndividualValueSetType
                        {
                            startIndex = explicitValues.StartIndex.GetValueOrDefault(),
                            startIndexSpecified = explicitValues.StartIndex.HasValue,
                            endIndex = explicitValues.EndIndex.GetValueOrDefault(),
                            endIndexSpecified = explicitValues.EndIndex.HasValue,
                            Items = Array.ConvertAll<T, object>( explicitValues.Values, v => v ),
                            ItemsElementName = Enumerable.Repeat( choice, explicitValues.Values.Length ).ToArray()
                        } );
                    }
                    break;
                case CompositeValueStream<T> composite:
                    foreach(var item in composite.Streams)
                    {
                        AddValueGenerator( item, target, size, choice );
                    }
                    break;
                default:
                    throw new NotSupportedException();
            }
        }

        private PlotScaleType ConvertPlotScale( PlotScale plotScale )
        {
            switch(plotScale)
            {
                case PlotScale.None:
                    return PlotScaleType.none;
                case PlotScale.Linear:
                    return PlotScaleType.linear;
                case PlotScale.Log:
                    return PlotScaleType.log;
                case PlotScale.Ln:
                    return PlotScaleType.ln;
                default:
                    throw new ArgumentOutOfRangeException( nameof( plotScale ) );
            }
        }

        private Series Deserialize( SeriesType series )
        {
            if(series == null)
            {
                throw new ArgumentNullException( nameof( series ) );
            }

            switch(series.seriesType)
            {
                case ParameterTypeType.Int32:
                    return FillSeries( new Series<int>(), series, BitConverter.ToInt32, sizeof( int ) );
                case ParameterTypeType.Int64:
                    return FillSeries( new Series<long>(), series, BitConverter.ToInt64, sizeof( long ) );
                case ParameterTypeType.Float32:
                    return FillSeries( new Series<float>(), series, BitConverter.ToSingle, sizeof( float ) );
                case ParameterTypeType.Float64:
                    return FillSeries( new Series<double>(), series, BitConverter.ToDouble, sizeof( double ) );
                case ParameterTypeType.String:
                    return FillSeries( new Series<string>(), series, null, 0 );
                case ParameterTypeType.Boolean:
                    return FillSeries( new Series<bool>(), series, BitConverter.ToBoolean, sizeof( bool ) );
                case ParameterTypeType.DateTime:
                case ParameterTypeType.EmbeddedXML:
                case ParameterTypeType.PNG:
                case ParameterTypeType.SVG:
                    throw new NotSupportedException();
                default:
                    throw new InvalidOperationException();
            }
        }

        private Series<T> FillSeries<T>( Series<T> series, SeriesType original, Func<byte[], int, T> bitConvert, int size )
        {
            if(original.Items != null)
            {
                foreach(var item in original.Items)
                {
                    CompositeValueStream<T> composite = null;
                    ValueStream<T> stream;
                    switch(item)
                    {
                        case AutoIncrementedValueSetType autoIncrement:
                            stream = new AutoIncrementedStream<T>
                            {
                                StartValue = (T)autoIncrement.StartValue.Items[0],
                                Increment = (T)autoIncrement.Increment.Items[0],
                                StartIndex = ReadNullable( autoIncrement.startIndex, autoIncrement.startIndexSpecified ),
                                EndIndex = ReadNullable( autoIncrement.endIndex, autoIncrement.endIndexSpecified )
                            };
                            break;
                        case EncodedValueSetType encoded:
                            stream = new ExplicitValueStream<T>
                            {
                                Values = Chunk( encoded.Value, bitConvert, size ),
                                StartIndex = ReadNullable( encoded.startIndex, encoded.startIndexSpecified ),
                                EndIndex = ReadNullable( encoded.endIndex, encoded.endIndexSpecified )
                            };
                            break;
                        case IndividualValueSetType individual:
                            stream = new ExplicitValueStream<T>
                            {
                                Values = Array.ConvertAll( individual.Items, o => (T)o ),
                                StartIndex = ReadNullable( individual.startIndex, individual.startIndexSpecified ),
                                EndIndex = ReadNullable( individual.endIndex, individual.endIndexSpecified )
                            };
                            break;
                        default:
                            throw new NotSupportedException();
                    }
                    if(composite == null)
                    {
                        composite = new CompositeValueStream<T>();
                        series.Values = stream;
                    }
                    else
                    {
                        series.Values = composite;
                    }
                    composite.Streams.Add( stream );
                }
            }
            series.Name = original.name;
            series.PlotScale = ConvertPlotScale( original.plotScale );
            series.SeriesId = original.seriesID;
            series.Unit = UnitSerializer.DeserializeUnit( original.Unit );
            series.Visible = original.visible;
            series.IsDependent = original.dependency == DependencyType.dependent;
            return series;
        }

        private static PlotScale ConvertPlotScale( PlotScaleType plotScale )
        {
            switch(plotScale)
            {
                case PlotScaleType.linear:
                    return PlotScale.Linear;
                case PlotScaleType.log:
                    return PlotScale.Log;
                case PlotScaleType.ln:
                    return PlotScale.Ln;
                case PlotScaleType.none:
                    return PlotScale.None;
                default:
                    throw new ArgumentOutOfRangeException( nameof( plotScale ) );
            }
        }

        private static T? ReadNullable<T>( T value, bool valueSpecified ) where T : struct
        {
            return valueSpecified ? value : default( T? );
        }

        private static T[] Chunk<T>( byte[] array, Func<byte[], int, T> conversion, int size )
        {
            var result = new T[array.Length / size];
            for(int i = 0; i < result.Length; i++)
            {
                result[i] = conversion( array, i * size );
            }
            return result;
        }
    }
}
