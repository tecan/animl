﻿using System;
using System.Collections.Generic;
using System.Text;
using Tecan.AnIML.Categories;

namespace Tecan.AnIML.Serialization
{
    internal static class SerializationHelper
    {

        public static void DeserializeAll<TSource, TTarget>( TSource[] source, Func<TTarget, bool> target, Func<string, TTarget> resolver, Func<TSource, Func<string, TTarget>, TTarget> deserializer )
        {
            if(source != null)
            {
                foreach(var item in source)
                {
                    target( deserializer( item, resolver ) );
                }
            }
        }

        public static void DeserializeAll<TSource, TTarget>( TSource[] source, Func<TTarget, bool> target, Func<TSource, TTarget> deserializer )
        {
            if(source != null)
            {
                foreach(var item in source)
                {
                    target( deserializer( item ) );
                }
            }
        }
    }
}
