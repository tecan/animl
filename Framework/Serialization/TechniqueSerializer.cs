﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Technique = Tecan.AnIML.Techniques.Serialization.TechniqueType;

namespace Tecan.AnIML.Serialization
{
    /// <summary>
    /// Denotes a helper class to load technique definitions
    /// </summary>
    public static class TechniqueSerializer
    {
        private static readonly XmlReaderSettings _defaultReaderSettings = new XmlReaderSettings
        {
            DtdProcessing = DtdProcessing.Prohibit,
            CheckCharacters = false,
            ConformanceLevel = ConformanceLevel.Auto,
            IgnoreComments = true,
            ValidationFlags = System.Xml.Schema.XmlSchemaValidationFlags.None,
        };

        /// <summary>
        /// Loads the technique definition from the provided path
        /// </summary>
        /// <param name="path">The path from which to load technique definitions</param>
        /// <param name="xxe">True, if external XML Entities should be resolved, otherwise False</param>
        /// <returns>The deserialized technique</returns>
        public static Technique Load( string path, bool xxe = false )
        {
            var serializer = new XmlSerializer( typeof( Technique ) );
            using(var fs = File.OpenRead( path ))
            {
                var directory = Path.GetDirectoryName( Path.GetFullPath( path ) );
                Environment.CurrentDirectory = directory;
                var reader = XmlReader.Create( fs, xxe ? CreateXxeReaderSettings( directory ) : _defaultReaderSettings );
                return (Technique)serializer.Deserialize( reader );
            }
        }

        private static XmlReaderSettings CreateXxeReaderSettings( string directory )
        {
            return new XmlReaderSettings
            {
                DtdProcessing = DtdProcessing.Parse,
                CheckCharacters = false,
                ConformanceLevel = ConformanceLevel.Auto,
                IgnoreComments = true,
                ValidationFlags = System.Xml.Schema.XmlSchemaValidationFlags.None,
                XmlResolver = new EntityResolver( directory )
            };
        }
    }

    internal class EntityResolver : XmlResolver
    {
        private readonly string _directory;

        public EntityResolver( string directory )
        {
            _directory = directory;
        }

        public override object GetEntity( Uri absoluteUri, string role, Type ofObjectToReturn )
        {
            if(ofObjectToReturn == typeof( Stream ) && absoluteUri != null && absoluteUri.IsFile)
            {
                var relative = absoluteUri.MakeRelativeUri( new Uri( Environment.CurrentDirectory ) );
                var absoluteBasedOnDirectory = new Uri( new Uri( _directory ), relative );
                if(File.Exists( absoluteBasedOnDirectory.LocalPath ))
                {
                    return File.OpenRead( absoluteBasedOnDirectory.LocalPath );
                }
                if(File.Exists( absoluteUri.LocalPath ))
                {
                    return File.OpenRead( absoluteUri.LocalPath );
                }
            }
            return null;
        }
    }
}
