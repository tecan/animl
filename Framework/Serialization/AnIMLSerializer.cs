﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Tecan.AnIML.AuditTrail;
using Tecan.AnIML.ExperimentSteps;
using Tecan.AnIML.Samples;

namespace Tecan.AnIML.Serialization
{
    /// <summary>
    /// Denotes a serializer for AnIML files
    /// </summary>
    public class AnIMLSerializer
    {
        private readonly CategorySerializer _categorySerializer = new CategorySerializer();
        private static readonly XmlSerializer _serializer = new XmlSerializer( typeof( AnIMLType ) );


        private readonly Dictionary<string, List<SampleReference>> _staleReferences = new Dictionary<string, List<SampleReference>>();
        private readonly Dictionary<string, ISignableItem> _signableItems = new Dictionary<string, ISignableItem>();
        private AniMlDocument _deserializedDocument;

        /// <summary>
        /// Serializes the provided document to the given target stream
        /// </summary>
        /// <param name="document">The document</param>
        /// <param name="target">The target stream</param>
        public void Serialize( AniMlDocument document, Stream target )
        {
            using(var xmlWriter = XmlWriter.Create( target, new XmlWriterSettings
            {
                Indent = true,
                Encoding = Encoding.UTF8,
                IndentChars = "  ",
                NewLineChars = Environment.NewLine
            } ))
            {
                _serializer.Serialize( xmlWriter, SerializeCore( document ) );
            }
        }

        /// <summary>
        /// Deserializes the provided stream into an AnIML document
        /// </summary>
        /// <param name="target">To stream from which to deserialize</param>
        /// <returns>A parsed AnIML document</returns>
        public AniMlDocument Deserialize( Stream target )
        {
            return DeserializeCore( (AnIMLType)_serializer.Deserialize( target ) );
        }

        private AnIMLType SerializeCore( AniMlDocument aniMlDocument )
        {
            foreach(var entry in aniMlDocument.AuditTrailEntries)
            {
                foreach(var referenced in entry.References)
                {
                    referenced.GetOrCreateId();
                }
                foreach(var diff in entry.Diffs)
                {
                    diff.Reference?.GetOrCreateId();
                }
            }
            foreach(var reference in aniMlDocument.Signatures.SelectMany( s => s.SignedItems ))
            {
                reference.GetOrCreateId();
            }

            var result = new AnIMLType
            {
                SampleSet = new SampleSetType
                {
                    Sample = aniMlDocument.Samples.Select( Serialize ).ToArray(),
                    id = aniMlDocument.Samples.Id
                },
                ExperimentStepSet = new ExperimentStepSetType
                {
                    Template = aniMlDocument.Templates.Select( Serialize ).ToArray(),
                    ExperimentStep = aniMlDocument.Steps.Select( Serialize ).ToArray(),
                    id = aniMlDocument.Steps.Id
                },
                AuditTrailEntrySet = aniMlDocument.AuditTrailEntries.Any()
                ? new AuditTrailEntrySetType
                {
                    AuditTrailEntry = aniMlDocument.AuditTrailEntries.Select( Serialize ).ToArray()
                }
                : null,
            };

            var signatures = new List<XmlElement>();
            if(aniMlDocument.Signatures.Any( s => !s.HasSignature ))
            {
                result.SignatureSet = new SignatureSetType();
                var document = new XmlDocument();
                var nav = document.CreateNavigator();
                using(var writer = nav.AppendChild())
                {
                    _serializer.Serialize( writer, result );
                }
                var signaturesElement = document.DocumentElement.ChildNodes[document.DocumentElement.ChildNodes.Count - 1];
                foreach(var signature in aniMlDocument.Signatures)
                {
                    XmlElement sig;
                    if(signature.HasSignature)
                    {
                        sig = signature.CurrentSignature;
                    }
                    else
                    {
                        sig = signature.CreateSignature( document );
                    }
                    signaturesElement.AppendChild( sig );
                    signatures.Add( sig );
                }
            }
            else
            {
                foreach(var signature in aniMlDocument.Signatures)
                {
                    signatures.Add( signature.CurrentSignature );
                }
            }
            if(signatures.Count > 0)
            {
                result.SignatureSet = new SignatureSetType
                {
                    Any = signatures.ToArray()
                };
            }

            return result;
        }

        private AuditTrailEntryType Serialize( AuditTrailEntry entry )
        {
            return new AuditTrailEntryType
            {
                id = entry.Id,
                Action = ConvertAction( entry.Action ),
                Author = SerializeAuthor( entry.Author ),
                Comment = entry.Comment,
                Reason = entry.Reason,
                Software = SerializeSoftware( entry.Software ),
                Timestamp = entry.Timestamp,
                Reference = entry.References.Select( r => r.GetOrCreateId() ).ToArray(),
                Diff = entry.Diffs.Select( Serialize ).ToArray()
            };
        }

        private DiffType Serialize( AuditedDiff diff )
        {
            return new DiffType
            {
                changedItem = diff.Reference?.GetOrCreateId(),
                NewValue = diff.NewValue,
                OldValue = diff.OldValue,
                scope = diff.IsElementScope ? ScopeType.element : ScopeType.attributes
            };
        }

        private AuditTrailEntry Deserialize( AuditTrailEntryType entry )
        {
            var e = new AuditTrailEntry
            {
                Action = ConvertAction( entry.Action ),
                Author = DeserializeAuthor( entry.Author ),
                Comment = entry.Comment,
                Reason = entry.Reason,
                Software = DeserializeSoftware( entry.Software ),
                Timestamp = entry.Timestamp,
                Id = entry.id
            };
            SerializationHelper.DeserializeAll( entry.Reference, r =>
             {
                 e.References.Add( r );
                 return true;
             }, ResolveReference );
            SerializationHelper.DeserializeAll( entry.Diff, d =>
            {
                e.Diffs.Add( d );
                return true;
            }, DeserializeDiff );
            Register( e );
            return e;
        }

        private AuditedDiff DeserializeDiff( DiffType diff )
        {
            ISignableItem referenced = null;
            if(!string.IsNullOrEmpty( diff.changedItem ))
            {
                _signableItems.TryGetValue( diff.changedItem, out referenced );
            }
            return new AuditedDiff
            {
                IsElementScope = diff.scope == ScopeType.element,
                NewValue = diff.NewValue,
                OldValue = diff.OldValue,
                Reference = referenced
            };
        }

        private ISignableItem ResolveReference( string reference )
        {
            if(_signableItems.TryGetValue( reference, out var signableItem ))
            {
                return signableItem;
            }
            return new StaleReference( reference );
        }



        private void Register( ISignableItem signableItem )
        {
            if(!string.IsNullOrEmpty( signableItem.Id ))
            {
                _signableItems[signableItem.Id] = signableItem;
            }
        }

        private static ActionType ConvertAction( AuditTrailEntryAction action )
        {
            switch(action)
            {
                case AuditTrailEntryAction.Created:
                    return ActionType.created;
                case AuditTrailEntryAction.Modified:
                    return ActionType.modified;
                case AuditTrailEntryAction.Converted:
                    return ActionType.converted;
                case AuditTrailEntryAction.Read:
                    return ActionType.read;
                case AuditTrailEntryAction.Signed:
                    return ActionType.signed;
                case AuditTrailEntryAction.Deleted:
                    return ActionType.deleted;
                default:
                    throw new ArgumentOutOfRangeException( nameof( action ) );
            }
        }

        private static AuditTrailEntryAction ConvertAction( ActionType action )
        {
            switch(action)
            {
                case ActionType.created:
                    return AuditTrailEntryAction.Created;
                case ActionType.modified:
                    return AuditTrailEntryAction.Modified;
                case ActionType.converted:
                    return AuditTrailEntryAction.Converted;
                case ActionType.read:
                    return AuditTrailEntryAction.Read;
                case ActionType.signed:
                    return AuditTrailEntryAction.Signed;
                case ActionType.deleted:
                    return AuditTrailEntryAction.Deleted;
                default:
                    throw new ArgumentOutOfRangeException( nameof( action ) );
            }
        }

        private AniMlDocument DeserializeCore( AnIMLType aniMlDocument )
        {
            var result = new AniMlDocument();

            _deserializedDocument = result;

            result.Samples.Id = aniMlDocument.SampleSet?.id;
            result.Steps.Id = aniMlDocument.ExperimentStepSet?.id;

            SerializationHelper.DeserializeAll( aniMlDocument.ExperimentStepSet?.Template, result.AddTemplate, Deserialize );
            SerializationHelper.DeserializeAll( aniMlDocument.ExperimentStepSet?.ExperimentStep, result.AddStep, Deserialize );
            SerializationHelper.DeserializeAll( aniMlDocument.SampleSet?.Sample, result.AddSample, Deserialize );

            SerializationHelper.DeserializeAll( aniMlDocument.AuditTrailEntrySet?.AuditTrailEntry, result.WriteToAuditTrail, Deserialize );
            SerializationHelper.DeserializeAll( aniMlDocument.SignatureSet?.Any, result.AddSignature, Deserialize );

            _staleReferences.Clear();
            _signableItems.Clear();

            return result;
        }

        private Signature Deserialize( XmlElement element )
        {
            return Signature.CreateFrom( element, ResolveReference );
        }

        private SampleType Serialize( Sample sample )
        {
            return new SampleType
            {
                id = sample.Id,
                derived = sample.IsDerived,
                barcode = sample.Barcode,
                Category = sample.AllCategories().Select( _categorySerializer.Serialize ).ToArray(),
                comment = sample.Comment,
                containerID = sample.Container,
                containerType = ConvertContainerType( sample.ContainerType ),
                locationInContainer = sample.PositionInContainerString,
                sampleID = sample.SampleId,
                name = sample.Name ?? sample.SampleId,
                sourceDataLocation = sample.SourceDataLocation,
                TagSet = ConvertTags( sample.Tags ),
            };
        }

        private Sample Deserialize( SampleType sample )
        {
            var result = new Sample
            {
                Id = sample.id,
                IsDerived = sample.derived,
                Barcode = sample.barcode,
                Comment = sample.comment,
                Container = sample.containerID,
                ContainerType = ConvertContainerType( sample.containerType ),
                Name = sample.name,
                PositionInContainerString = sample.locationInContainer,
                SampleId = sample.sampleID,
                SourceDataLocation = sample.sourceDataLocation
            };
            if(_staleReferences.TryGetValue( sample.sampleID, out var sampleReferences ))
            {
                foreach(var reference in sampleReferences)
                {
                    reference.Sample = result;
                }
            }
            SerializationHelper.DeserializeAll( sample.Category, result.AddCategory, _categorySerializer.Deserialize );
            AddTags( sample.TagSet, result.Tags );
            return result;
        }

        private TagType[] ConvertTags( IDictionary<string, string> tags )
        {
            if(tags == null || tags.Count == 0)
            {
                return null;
            }
            var result = new TagType[tags.Count];
            var index = 0;
            foreach(var pair in tags)
            {
                result[index] = new TagType
                {
                    name = pair.Key,
                    value = pair.Value
                };
                index++;
            }
            return result;
        }

        private void AddTags( TagType[] tags, IDictionary<string, string> target )
        {
            if(tags != null)
            {
                foreach(var tag in tags)
                {
                    target[tag.name] = tag.value;
                }
            }
        }

        private ContainerTypeType ConvertContainerType( ContainerType containerType )
        {
            switch(containerType)
            {
                case ContainerType.Simple:
                    return ContainerTypeType.simple;
                case ContainerType.Determinate:
                    return ContainerTypeType.determinate;
                case ContainerType.Indeterminate:
                    return ContainerTypeType.indeterminate;
                case ContainerType.RectangularTray:
                    return ContainerTypeType.rectangulartray;
                case ContainerType.Plate6:
                    return ContainerTypeType.Item6wells;
                case ContainerType.Plate24:
                    return ContainerTypeType.Item24wells;
                case ContainerType.Plate96:
                    return ContainerTypeType.Item96wells;
                case ContainerType.Plate384:
                    return ContainerTypeType.Item384wells;
                case ContainerType.Plate1536:
                    return ContainerTypeType.Item1536wells;
                default:
                    throw new ArgumentOutOfRangeException( nameof( containerType ) );
            }
        }


        private ContainerType ConvertContainerType( ContainerTypeType containerType )
        {
            switch(containerType)
            {
                case ContainerTypeType.simple:
                    return ContainerType.Simple;
                case ContainerTypeType.determinate:
                    return ContainerType.Determinate;
                case ContainerTypeType.indeterminate:
                    return ContainerType.Indeterminate;
                case ContainerTypeType.rectangulartray:
                    return ContainerType.RectangularTray;
                case ContainerTypeType.Item6wells:
                    return ContainerType.Plate6;
                case ContainerTypeType.Item24wells:
                    return ContainerType.Plate24;
                case ContainerTypeType.Item96wells:
                    return ContainerType.Plate96;
                case ContainerTypeType.Item384wells:
                    return ContainerType.Plate384;
                case ContainerTypeType.Item1536wells:
                    return ContainerType.Plate1536;
                default:
                    throw new ArgumentOutOfRangeException( nameof( containerType ) );
            }
        }


        private ExperimentStepType Serialize( ExperimentStep experimentStep )
        {
            return new ExperimentStepType
            {
                id = experimentStep.Id,
                comment = experimentStep.Comment,
                experimentStepID = experimentStep.ExperimentStepId,
                Infrastructure = Serialize( experimentStep.Infrastructure ),
                Method = Serialize( experimentStep.Method ),
                name = experimentStep.Name,
                Result = experimentStep.AllResults().Select( Serialize ).ToArray(),
                sourceDataLocation = experimentStep.SourceDataLocation,
                TagSet = ConvertTags( experimentStep.Tags ),
                Technique = Serialize( experimentStep.Technique ),
                templateUsed = experimentStep.TemplateUsed?.TemplateId
            };
        }

        /// <summary>
        /// Creates an experiment step for the provided technique 
        /// </summary>
        /// <param name="technique">The technique</param>
        /// <returns>An experiment step</returns>
        protected virtual ExperimentStep CreateExperimentStep( TechniqueType technique )
        {
            return new ExperimentStep();
        }

        /// <summary>
        /// Creates a template for the provided technique 
        /// </summary>
        /// <param name="technique">The technique</param>
        /// <returns>An experiment step</returns>
        protected virtual Template CreateTemplate( TechniqueType technique )
        {
            return new Template();
        }

        private ExperimentStep Deserialize( ExperimentStepType experimentStep )
        {
            var result = CreateExperimentStep( experimentStep.Technique );
            result.Comment = experimentStep.comment;
            result.Id = experimentStep.id;
            result.ExperimentStepId = experimentStep.experimentStepID;
            result.Name = experimentStep.name;
            result.SourceDataLocation = experimentStep.sourceDataLocation;
            result.Method = Deserialize( experimentStep.Method, result.CreateMethod );
            result.Technique = Deserialize( experimentStep.Technique );
            result.TemplateUsed = _deserializedDocument.Templates.FirstOrDefault( t => t.TemplateId == experimentStep.templateUsed )
                ?? CreateMockTemplate( experimentStep.templateUsed );
            AddTags( experimentStep.TagSet, result.Tags );
            SerializationHelper.DeserializeAll( experimentStep.Result, result.AddResult, result.CreateResult, Deserialize );
            Register( result );
            return result;
        }

        private Template CreateMockTemplate( string templateUsed )
        {
            return new Template
            {
                TemplateId = templateUsed
            };
        }

        private MethodType Serialize( Method method )
        {
            if(method == null)
            {
                return null;
            }

            return new MethodType
            {
                name = method.Name,
                id = method.Id,
                Author = SerializeAuthor( method.Author ),
                Device = SerializeDevice( method.Device ),
                Category = method.AllCategories().Select( _categorySerializer.Serialize ).ToArray(),
                Software = SerializeSoftware( method.Software )
            };
        }

        private Method Deserialize( MethodType method, Func<Method> methodCreator )
        {
            if(method == null)
            {
                return null;
            }

            var result = methodCreator();
            result.Id = method.id;
            result.Author = DeserializeAuthor( method.Author );
            result.Device = DeserializeDevice( method.Device );
            result.Name = method.name;
            result.Software = DeserializeSoftware( method.Software );
            SerializationHelper.DeserializeAll( method.Category, result.AddCategory, _categorySerializer.Deserialize );
            Register( result );
            return result;
        }

        private SoftwareType SerializeSoftware( Software software )
        {
            if(software == null)
            {
                return null;
            }

            return new SoftwareType
            {
                Manufacturer = software.Manufacturer,
                Name = software.Name,
                OperatingSystem = software.OperatingSystem,
                Version = software.Version
            };
        }

        private Software DeserializeSoftware( SoftwareType software )
        {
            if(software == null)
            {
                return null;
            }

            return new Software
            {
                Manufacturer = software.Manufacturer,
                Name = software.Name,
                OperatingSystem = software.OperatingSystem,
                Version = software.Version
            };
        }

        private DeviceType SerializeDevice( Device device )
        {
            if(device == null)
            {
                return null;
            }

            return new DeviceType
            {
                DeviceIdentifier = device.Identifier,
                FirmwareVersion = device.FirmwareVersion,
                Manufacturer = device.Manufacturer,
                Name = device.Name,
                SerialNumber = device.SerialNumber
            };
        }

        private Device DeserializeDevice( DeviceType device )
        {
            if(device == null)
            {
                return null;
            }

            return new Device
            {
                FirmwareVersion = device.FirmwareVersion,
                Name = device.Name,
                Manufacturer = device.Manufacturer,
                Identifier = device.DeviceIdentifier,
                SerialNumber = device.SerialNumber
            };
        }

        private AuthorType SerializeAuthor( Author author )
        {
            if(author == null)
            {
                return null;
            }

            return new AuthorType
            {
                Name = author.Name,
                Affiliation = author.Affiliation,
                Email = author.Email,
                Location = author.Location,
                Phone = author.Phone,
                Role = author.Role,
                userType = ConvertAuthorType( author.Type )
            };
        }

        private Author DeserializeAuthor( AuthorType author )
        {
            if(author == null)
            {
                return null;
            }

            return new Author
            {
                Name = author.Name,
                Affiliation = author.Affiliation,
                Email = author.Email,
                Location = author.Location,
                Phone = author.Phone,
                Role = author.Role,
                Type = ConvertAuthorType( author.userType )
            };
        }

        private UserTypeType ConvertAuthorType( ExperimentSteps.AuthorType type )
        {
            switch(type)
            {
                case ExperimentSteps.AuthorType.Human:
                    return UserTypeType.human;
                case ExperimentSteps.AuthorType.Device:
                    return UserTypeType.device;
                case ExperimentSteps.AuthorType.Software:
                    return UserTypeType.software;
                default:
                    throw new ArgumentOutOfRangeException( nameof( type ) );
            }
        }

        private ExperimentSteps.AuthorType ConvertAuthorType( UserTypeType type )
        {
            switch(type)
            {
                case UserTypeType.human:
                    return ExperimentSteps.AuthorType.Human;
                case UserTypeType.device:
                    return ExperimentSteps.AuthorType.Device;
                case UserTypeType.software:
                    return ExperimentSteps.AuthorType.Software;
                default:
                    throw new ArgumentOutOfRangeException( nameof( type ) );
            }
        }

        private TemplateType Serialize( Template template )
        {
            if(template == null)
            {
                return null;
            }
            return new TemplateType
            {
                id = template.Id,
                Infrastructure = Serialize( template.Infrastructure ),
                Method = Serialize( template.Method ),
                name = template.Name,
                Result = template.AllResults().Select( Serialize ).ToArray(),
                sourceDataLocation = template.SourceDataLocation,
                TagSet = ConvertTags( template.Tags ),
                Technique = Serialize( template.Technique ),
                templateID = template.TemplateId
            };
        }

        private Template Deserialize( TemplateType template )
        {
            if(template == null)
            {
                return null;
            }
            var result = CreateTemplate( template.Technique );
            result.Id = template.id;
            result.Infrastructure = Deserialize( template.Infrastructure, result.CreateInfrastructure );
            result.Method = Deserialize( template.Method, result.CreateMethod );
            result.Name = template.name;
            result.SourceDataLocation = template.sourceDataLocation;
            result.Technique = Deserialize( template.Technique );
            result.TemplateId = template.templateID;
            AddTags( template.TagSet, result.Tags );
            SerializationHelper.DeserializeAll( template.Result, result.AddResult, result.CreateResult, Deserialize );
            Register( result );
            return result;
        }

        private ResultType Serialize( Result result )
        {
            if(result == null)
            {
                return null;
            }

            return new ResultType
            {
                Category = result.UndefinedCategories.Select( _categorySerializer.Serialize ).ToArray(),
                name = result.Name,
                id = result.Id,
                SeriesSet = _categorySerializer.Serialize( result.SeriesSet ),
                ExperimentStepSet = result.Steps.Count > 0
                    ? new ExperimentStepSetType
                    {
                        ExperimentStep = result.Steps.Select( Serialize ).ToArray(),
                    }
                    : null
            };
        }

        private Result Deserialize( ResultType result, Func<string, Result> resolver )
        {
            if(result == null)
            {
                return null;
            }

            var res = resolver?.Invoke( result.name ) ?? new Result( result.name );
            res.Id = result.id;
            SerializationHelper.DeserializeAll( result.Category, res.AddCategory, _categorySerializer.Deserialize );
            SerializationHelper.DeserializeAll( result.ExperimentStepSet?.ExperimentStep, res.AddStep, Deserialize );
            res.SeriesSet = _categorySerializer.Deserialize( result.SeriesSet, null );
            Register( res );
            return res;
        }

        private TechniqueType Serialize( Technique technique )
        {
            if(technique == null)
            {
                return null;
            }

            return new TechniqueType
            {
                name = technique.Name,
                sha256 = technique.Checksum,
                uri = technique.Uri.ToString(),
            };
        }

        private Technique Deserialize( TechniqueType technique )
        {
            if(technique == null)
            {
                return null;
            }

            return new Technique
            {
                Name = technique.name,
                Checksum = technique.sha256,
                Uri = new Uri( technique.uri, UriKind.RelativeOrAbsolute ),
            };
        }

        private InfrastructureType Serialize( Infrastructure infrastructure )
        {
            if(infrastructure == null)
            {
                return null;
            }

            return new InfrastructureType
            {
                SampleReferenceSet = infrastructure.AllSampleReferences().Any()
                ? new SampleReferenceSetType
                {
                    id = infrastructure.UndefinedSampleReferences.Id,
                    SampleReference = infrastructure.AllSampleReferences().Where( r => !r.IsInherited ).Select( SerializeAsReference ).ToArray(),
                    SampleInheritance = infrastructure.AllSampleReferences().Where( r => r.IsInherited ).Select( SerializeInherited ).ToArray()
                }
                : null,
                ExperimentDataReferenceSet = infrastructure.AllExperimentDataReferences().Any()
                ? new ExperimentDataReferenceSetType
                {
                    id = infrastructure.UndefinedExperimentDataReferences.Id,
                    ExperimentDataReference = infrastructure.AllExperimentDataReferences().Where( r => r.ExperimentStep != null ).Select( SerializeExperimentDataReference ).ToArray(),
                    ExperimentDataBulkReference = infrastructure.AllExperimentDataReferences().Where( r => r.ExperimentStepPrefix != null ).Select( SerializeBulkExperimentDataReference ).ToArray()
                }
                : null,
                Timestamp = infrastructure.Timestamp.GetValueOrDefault(),
                TimestampSpecified = infrastructure.Timestamp.HasValue,
                id = infrastructure.Id
            };
        }

        private ExperimentDataReferenceType SerializeExperimentDataReference( ExperimentDataReference reference )
        {
            return new ExperimentDataReferenceType
            {
                role = reference.Role,
                experimentStepID = reference.ExperimentStep.ExperimentStepId,
                dataPurpose = ConvertPurpose( reference.Purpose ),
                id = reference.Id
            };
        }

        private ExperimentDataBulkReferenceType SerializeBulkExperimentDataReference( ExperimentDataReference reference )
        {
            return new ExperimentDataBulkReferenceType
            {
                role = reference.Role,
                experimentStepIDPrefix = reference.ExperimentStepPrefix,
                dataPurpose = ConvertPurpose( reference.Purpose ),
                id = reference.Id
            };
        }

        private Infrastructure Deserialize( InfrastructureType infrastructure, Func<Infrastructure> inrastructureCreator )
        {
            if(infrastructure == null)
            {
                return null;
            }

            var result = inrastructureCreator();
            result.UndefinedSampleReferences.Id = infrastructure.SampleReferenceSet?.id;
            result.Id = infrastructure.id;

            result.Timestamp = infrastructure.TimestampSpecified ? infrastructure.Timestamp : default( DateTime? );

            SerializationHelper.DeserializeAll( infrastructure.SampleReferenceSet?.SampleReference, result.AddReferencedSample, result.CreateSampleReference, DeserializeReference );
            SerializationHelper.DeserializeAll( infrastructure.SampleReferenceSet?.SampleInheritance, result.AddReferencedSample, result.CreateSampleReference, DeserializeReference );

            SerializationHelper.DeserializeAll( infrastructure.ExperimentDataReferenceSet?.ExperimentDataReference, result.AddExperimentDataReference, result.CreateExperimentDataReference, DeserializeReference );
            SerializationHelper.DeserializeAll( infrastructure.ExperimentDataReferenceSet?.ExperimentDataBulkReference, result.AddExperimentDataReference, result.CreateExperimentDataReference, DeserializeReference );
            Register( result );
            return result;
        }

        private ExperimentDataReference DeserializeReference( ExperimentDataBulkReferenceType reference, Func<string, ExperimentDataReference> resolver )
        {
            var result = resolver( reference.role );
            result.Id = reference.id;
            result.ExperimentStepPrefix = reference.experimentStepIDPrefix;
            result.Purpose = ConvertPurpose( reference.dataPurpose );
            Register( result );
            return result;
        }

        private ExperimentDataReference DeserializeReference( ExperimentDataReferenceType reference, Func<string, ExperimentDataReference> resolver )
        {
            var result = resolver( reference.role );
            result.Id = reference.id;
            result.ExperimentStep = FindExperimentStep( reference.experimentStepID );
            result.Purpose = ConvertPurpose( reference.dataPurpose );
            Register( result );
            return result;
        }

        private ExperimentStep FindExperimentStep( string experimentStepID )
        {
            // bfs
            var processQueue = new Queue<ExperimentStep>( _deserializedDocument.Steps );
            while(processQueue.Count > 0)
            {
                var step = processQueue.Dequeue();
                if(step != null)
                {
                    if(step.ExperimentStepId == experimentStepID)
                    {
                        return step;
                    }

                    foreach(var subStep in step.AllResults().SelectMany( r => r.Steps ))
                    {
                        processQueue.Enqueue( subStep );
                    }
                }
            }

            return null;
        }

        private SampleReference DeserializeReference( SampleInheritanceType reference, Func<string, SampleReference> resolver )
        {
            var result = resolver( reference.role );
            result.IsInherited = true;
            result.Id = reference.id;
            result.Purpose = ConvertPurpose( reference.samplePurpose );
            Register( result );
            return result;
        }

        private SampleReference DeserializeReference( SampleReferenceType reference, Func<string, SampleReference> resolver )
        {
            var result = resolver( reference.role );
            result.IsInherited = false;
            result.Id = reference.id;
            result.Purpose = ConvertPurpose( reference.samplePurpose );
            if(!_staleReferences.TryGetValue( reference.sampleID, out var sampleReferencesOfSample ))
            {
                sampleReferencesOfSample = new List<SampleReference>();
                _staleReferences.Add( reference.sampleID, sampleReferencesOfSample );
            }
            sampleReferencesOfSample.Add( result );
            Register( result );
            return result;
        }

        private SampleInheritanceType SerializeInherited( SampleReference reference )
        {
            return new SampleInheritanceType
            {
                id = reference.Id,
                role = reference.Role,
                samplePurpose = ConvertPurpose( reference.Purpose )
            };
        }


        private SamplePurposeType ConvertPurpose( Purpose purpose )
        {
            switch(purpose)
            {
                case Purpose.Produced:
                    return SamplePurposeType.produced;
                case Purpose.Consumed:
                    return SamplePurposeType.consumed;
                default:
                    throw new ArgumentOutOfRangeException( nameof( purpose ) );
            }
        }


        private Purpose ConvertPurpose( SamplePurposeType purpose )
        {
            switch(purpose)
            {
                case SamplePurposeType.produced:
                    return Purpose.Produced;
                case SamplePurposeType.consumed:
                    return Purpose.Consumed;
                default:
                    throw new ArgumentOutOfRangeException( nameof( purpose ) );
            }
        }

        private SampleReferenceType SerializeAsReference( SampleReference reference )
        {
            return new SampleReferenceType
            {
                id = reference.Id,
                role = reference.Role,
                samplePurpose = ConvertPurpose( reference.Purpose ),
                sampleID = reference.Sample?.SampleId
            };
        }
    }
}
