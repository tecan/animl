﻿using Microsoft.CSharp;
using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tecan.AnIML.Generator.Helpers
{
    public static class CodeGenerationHelper
    {
        private static readonly CSharpCodeProvider CSharp = new CSharpCodeProvider();

        /// <summary>
        /// Generates the C# code of the given compilation unit to the given file path
        /// </summary>
        /// <param name="unit">The CodeDOM unit that contains the generated code</param>
        /// <param name="path">The file path where the code should be generated to</param>
        public static void GenerateCSharp( CodeCompileUnit unit, string path )
        {
            var options = new CodeGeneratorOptions
            {
                BlankLinesBetweenMembers = true,
                BracingStyle = "C",
                ElseOnClosing = true,
                IndentString = "    ",
                VerbatimOrder = false
            };
            var directory = Path.GetDirectoryName( Path.GetFullPath( path ) );
            if(directory != null && !Directory.Exists( directory )) Directory.CreateDirectory( directory );
            using(var writer = new StreamWriter( Path.GetFullPath( path ) ))
            {
                CSharp.GenerateCodeFromCompileUnit( unit, writer, options );
            }
        }

        /// <summary>
        /// Converts the given identifier to Pascal case
        /// </summary>
        /// <param name="phrase">A phrase (i.e. can contain whitespaces)</param>
        /// <returns>A Pascal case representation of the phrase</returns>
        public static string ToPascalCase( this string phrase )
        {
            var words = phrase
                .Replace( "²", " squared " )
                .Replace( "³", " cube " )
                .Replace( "°", " degree " )
                .Replace( "%", " percent " )
                .Split( new[] { ' ', '\t', '-', '/', '\\', '(', ')', '[', ']', '{', '}', '"', '\'' }, StringSplitOptions.RemoveEmptyEntries );
            return String.Join( string.Empty, words.Select( ToPascalWord ) );
        }

        private static string ToPascalWord( string word )
        {
            if(string.IsNullOrEmpty( word ))
            {
                return string.Empty;
            }
            if(word.Length > 1)
            {
                return char.ToUpperInvariant( word[0] ) + word.Substring( 1 ).ToLowerInvariant();
            }
            return word.ToUpperInvariant();
        }

        public static CodeMemberField CreateBackingField( CodeMemberProperty property )
        {
            var fieldName = MakeFieldName( property.Name );
            var fieldRef = new CodeFieldReferenceExpression( null, fieldName );
            property.GetStatements.Add( new CodeMethodReturnStatement( fieldRef ) );
            if(property.HasSet)
            {
                property.SetStatements.Add( new CodeAssignStatement( fieldRef, new CodePropertySetValueReferenceExpression() ) );
            }
            return new CodeMemberField( property.Type, fieldName )
            {
                Attributes = property.HasSet ? MemberAttributes.Private : MemberAttributes.Private | MemberAttributes.Final
            };
        }

        private static string MakeFieldName( string memberName )
        {
            return "_" + char.ToLowerInvariant( memberName[0] ) + memberName.Substring( 1 );
        }

        public static bool MeansCollection( string maxOccurs )
        {
            return maxOccurs != "1" && !string.IsNullOrEmpty( maxOccurs );
        }

        /// <summary>
        /// Generates documentation for the given code member
        /// </summary>
        /// <param name="member">The member for which code should be generated</param>
        /// <param name="summary">The summary that should be generated</param>
        /// <param name="returns">The returns value that should be generated</param>
        /// <param name="parameters">A dictionary with parameter definitions</param>
        /// <returns>The member (for chaining purposes)</returns>
        public static CodeTypeMember WriteDocumentation( this CodeTypeMember member, string summary, string returns = null, IReadOnlyDictionary<string, string> parameters = null )
        {
            var documentationWriter = new StringBuilder();
            documentationWriter.AppendLine( " <summary>" );
            if(summary != null)
            {
                foreach(var line in summary.Split( '\n' ))
                {
                    var trimmed = line.Trim();
                    if(!String.IsNullOrEmpty( trimmed ))
                    {
                        documentationWriter.AppendLine( " " + trimmed );
                    }
                }
            }

            documentationWriter.Append( " </summary>" );
            if(parameters != null)
            {
                foreach(var parameter in parameters)
                {
                    documentationWriter.AppendLine();
                    documentationWriter.Append( $" <param name=\"{parameter.Key}\">{parameter.Value}</param>" );
                }
            }

            if(returns != null)
            {
                documentationWriter.AppendLine();
                documentationWriter.Append( $" <returns>{returns}</returns>" );
            }

            member.Comments.Add( new CodeCommentStatement( documentationWriter.ToString(), true ) );
            return member;
        }

        /// <summary>
        /// Writes a comment that documentation is inherited
        /// </summary>
        /// <param name="member">The member to which the documentation tag should be written</param>
        public static CodeTypeMember WriteInheritDoc( this CodeTypeMember member )
        {
            member.Comments.Add( new CodeCommentStatement( " <inheritdoc />", true ) );
            return member;
        }
    }
}
