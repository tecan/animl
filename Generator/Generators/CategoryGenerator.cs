﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using Tecan.AnIML.Categories;
using Tecan.AnIML.Generator.Helpers;
using Tecan.AnIML.Techniques.Serialization;
using Tecan.AnIML.Units;

namespace Tecan.AnIML.Generator.Generators
{
    /// <summary>
    /// Denotes a class used for code generation for categories
    /// </summary>
    public class CategoryGenerator
    {
        /// <summary>
        /// Adds the given category to the code namespace
        /// </summary>
        /// <param name="blueprint">The category blueprint</param>
        /// <param name="ns">The namespace to which the generated code should be added</param>
        /// <param name="prefix">A name prefix</param>
        public void AddCategory( CategoryBlueprintType blueprint, CodeNamespace ns, string prefix )
        {
            var codeType = new CodeTypeDeclaration
            {
                Name = GetTypeName( prefix, blueprint.name ),
                Attributes = MemberAttributes.Public,
                IsClass = true,
                IsPartial = true
            };
            codeType.BaseTypes.Add( nameof( Category ) );
            codeType.WriteDocumentation( blueprint.Documentation?.Value );

            var constructor = new CodeConstructor { Attributes = MemberAttributes.Public };
            constructor.BaseConstructorArgs.Add( new CodePrimitiveExpression( blueprint.name ) );
            codeType.Members.Add( constructor );

            AddParameters( blueprint.ParameterBlueprint, codeType, ns );
            AddSeriesSets( blueprint.SeriesSetBlueprint, ns, codeType );
            AddCategories( blueprint.CategoryBlueprint, codeType, ns );
            ns.Types.Add( codeType );
        }

        /// <summary>
        /// Adds the given series set to the code namespace
        /// </summary>
        /// <param name="blueprint">The blueprint of the series set</param>
        /// <param name="ns">The namespace to which the generated code should be added to</param>
        /// <param name="prefix">A name prefix</param>
        /// <returns>The generated code declaration</returns>
        public CodeTypeDeclaration AddSeriesSet( SeriesSetBlueprintType blueprint, CodeNamespace ns, string prefix )
        {
            var codeType = new CodeTypeDeclaration
            {
                Name = GetTypeName( prefix, blueprint.name ),
                Attributes = MemberAttributes.Public,
                IsClass = true,
                IsPartial = true
            };
            codeType.BaseTypes.Add( nameof( SeriesSet ) );
            codeType.WriteDocumentation( blueprint.Documentation?.Value );

            var constructor = new CodeConstructor { Attributes = MemberAttributes.Public };
            constructor.BaseConstructorArgs.Add( new CodePrimitiveExpression( blueprint.name ) );
            codeType.Members.Add( constructor );

            var allSeries = new CodeMemberMethod
            {
                Name = "AllSeries",
                Attributes = MemberAttributes.Family | MemberAttributes.Override,
                ReturnType = new CodeTypeReference( typeof( IEnumerable<> ).Name, new CodeTypeReference( nameof( Series ) ) )
            };
            var allSeriesExpression = new CodeMethodInvokeExpression( new CodeBaseReferenceExpression(), allSeries.Name );

            var addSeries = new CodeMemberMethod
            {
                Name = "AddSeries",
                Attributes = MemberAttributes.Family | MemberAttributes.Override,
                ReturnType = new CodeTypeReference( typeof( bool ) )
            };
            var seriesRef = new CodeArgumentReferenceExpression( "series" );
            addSeries.Parameters.Add( new CodeParameterDeclarationExpression( nameof( Series ), seriesRef.ParameterName ) );

            if(blueprint.Items != null)
            {
                foreach(var seriesBlueprint in blueprint.Items.OfType<SeriesBlueprintType>())
                {
                    AddSeries( seriesBlueprint, codeType, ns, addSeries, seriesRef, ref allSeriesExpression );
                }

                foreach(var seriesBlueprintChoice in blueprint.Items.OfType<SeriesBlueprintChoiceType>())
                {
                    if(seriesBlueprintChoice.SeriesBlueprint != null)
                    {
                        foreach(var series in seriesBlueprintChoice.SeriesBlueprint)
                        {
                            AddSeries( series, codeType, ns, addSeries, seriesRef, ref allSeriesExpression );
                        }
                    }
                }
            }

            if(allSeriesExpression.Method.MethodName != allSeries.Name)
            {
                allSeries.WriteInheritDoc();
                codeType.Members.Add( allSeries );
                allSeries.Statements.Add( new CodeMethodReturnStatement( allSeriesExpression ) );
                addSeries.WriteInheritDoc();
                addSeries.Statements.Add( new CodeMethodReturnStatement( new CodeMethodInvokeExpression( new CodeBaseReferenceExpression(), addSeries.Name, seriesRef ) ) );
                codeType.Members.Add( addSeries );
            }

            ns.Types.Add( codeType );
            return codeType;
        }

        private void AddSeries( SeriesBlueprintType seriesBlueprint, CodeTypeDeclaration codeType, CodeNamespace parentNamespace, CodeMemberMethod addSeries, CodeExpression seriesRef, ref CodeMethodInvokeExpression allSeriesExpression )
        {
            var baseType = ConvertType( seriesBlueprint.seriesType, false );
            var seriesProperty = new CodeMemberProperty
            {
                Name = seriesBlueprint.name.ToPascalCase(),
                Attributes = MemberAttributes.Public | MemberAttributes.Final,
                Type = baseType != null ? new CodeTypeReference( typeof( ValueStream<> ).Name, baseType ) : new CodeTypeReference( nameof( ValueStream ) ),
                HasGet = true,
                HasSet = true,
            };

            allSeriesExpression = new CodeMethodInvokeExpression( allSeriesExpression, nameof( SeriesExtensions.Add ),
                new CodePrimitiveExpression( seriesBlueprint.name ),
                new CodePropertyReferenceExpression( null, seriesProperty.Name ),
                new CodePrimitiveExpression( seriesBlueprint.dependency == DependencyType.dependent ),
                new CodeFieldReferenceExpression( new CodeTypeReferenceExpression( nameof( PlotScale ) ), seriesBlueprint.plotScale.ToString().ToPascalCase() ) );

            codeType.Members.Add( seriesProperty );
            codeType.Members.Add( CodeGenerationHelper.CreateBackingField( seriesProperty ) );

            var condition = new CodeConditionStatement
            {
                Condition = new CodeBinaryOperatorExpression
                {
                    Left = new CodePropertyReferenceExpression( seriesRef, nameof( Series.Name ) ),
                    Operator = CodeBinaryOperatorType.ValueEquality,
                    Right = new CodePrimitiveExpression( seriesBlueprint.name )
                },
                TrueStatements =
                {
                    new CodeAssignStatement
                    {
                        Left = new CodePropertyReferenceExpression(null, seriesProperty.Name),
                        Right = new CodeCastExpression(seriesProperty.Type, new CodePropertyReferenceExpression(seriesRef, nameof(Series.ValuesObject)))
                    },
                }
            };

            SupportQuantities( seriesBlueprint.Quantity, seriesBlueprint.name, seriesProperty, codeType, parentNamespace, allSeriesExpression, condition.TrueStatements, seriesRef );

            condition.TrueStatements.Add( new CodeMethodReturnStatement( new CodePrimitiveExpression( true ) ) );
            addSeries.Statements.Add( condition );
        }

        private string GetTypeName( string prefix, string name )
        {
            var nameAsPascalCase = name.ToPascalCase();
            return prefix + nameAsPascalCase;
        }

        private void AddParameters( ParameterBlueprintType[] blueprints, CodeTypeDeclaration codeType, CodeNamespace parentNamespace )
        {
            if(blueprints != null)
            {
                var addParameter = new CodeMemberMethod
                {
                    Name = "AddParameter",
                    Attributes = MemberAttributes.Family | MemberAttributes.Override,
                    ReturnType = new CodeTypeReference( typeof( bool ) )
                };
                addParameter.WriteInheritDoc();
                var parameterRef = new CodeArgumentReferenceExpression( "parameter" );
                addParameter.Parameters.Add( new CodeParameterDeclarationExpression( nameof( Parameter ), parameterRef.ParameterName ) );
                var parameterName = new CodePropertyReferenceExpression( parameterRef, nameof( Parameter.Name ) );

                var allParametersExpression = new CodeMethodInvokeExpression( new CodeBaseReferenceExpression(), "AllParameters" );
                foreach(var parameter in blueprints)
                {
                    var propertyName = parameter.name.ToPascalCase();
                    var isCollection = CodeGenerationHelper.MeansCollection( parameter.maxOccurs );
                    var property = new CodeMemberProperty
                    {
                        Name = propertyName,
                        Attributes = MemberAttributes.Public | MemberAttributes.Final,
                        HasGet = true,
                        HasSet = !isCollection,
                        Type = ConvertType( parameter.parameterType, !isCollection && parameter.modality == ModalityType.optional )
                                ?? new CodeTypeReference( typeof( object ) )
                    };
                    if(propertyName == nameof( Category.Name ))
                    {
                        property.Attributes |= MemberAttributes.New;
                    }
                    property.WriteDocumentation( parameter.Documentation?.Value );
                    CodeExpression valueExpression = new CodeCastExpression( property.Type, new CodePropertyReferenceExpression( parameterRef, nameof( Parameter.ObjectValue ) ) );
                    var allowedValues = parameter.Items?.OfType<AllowedValueType>();
                    if(allowedValues != null && allowedValues.Any())
                    {
                        var enumeration = GenerateEnum( allowedValues.Select( a => new LiteralInfo( a ) ), GetTypeName( codeType.Name, parameter.name ), $"Denotes the possible values for {parameter.name}" );
                        parentNamespace.Types.Add( enumeration );
                        property.Type = new CodeTypeReference( enumeration.Name );
                        valueExpression = new CodeMethodInvokeExpression(
                            new CodeMethodReferenceExpression(
                                new CodeTypeReferenceExpression( typeof( EnumParser ).Name ),
                                nameof( EnumParser.Parse ),
                                property.Type ),
                            new CodePropertyReferenceExpression( parameterRef, nameof( Parameter.ObjectValue ) ) );
                    }
                    var conditional = new CodeConditionStatement( new CodeBinaryOperatorExpression(
                        parameterName, CodeBinaryOperatorType.ValueEquality, new CodePrimitiveExpression( parameter.name ) ) );
                    var backingField = CodeGenerationHelper.CreateBackingField( property );
                    if(!isCollection)
                    {
                        property.HasSet = true;
                        allParametersExpression = new CodeMethodInvokeExpression( allParametersExpression, nameof( ParameterExtensions.Add ),
                            new CodePrimitiveExpression( parameter.name ), new CodePropertyReferenceExpression( null, propertyName ) );
                        conditional.TrueStatements.Add( new CodeAssignStatement( new CodePropertyReferenceExpression( null, propertyName ), valueExpression ) );
                    }
                    else
                    {
                        allParametersExpression = new CodeMethodInvokeExpression( allParametersExpression, nameof( ParameterExtensions.AddAll ),
                            new CodePrimitiveExpression( parameter.name ), new CodePropertyReferenceExpression( null, propertyName ) );
                        conditional.TrueStatements.Add( new CodeMethodInvokeExpression( new CodePropertyReferenceExpression( null, propertyName ), nameof( ICollection<object>.Add ), valueExpression ) );
                        backingField.InitExpression = MakeCollectionImplementation( property.Type );
                        property.Type = MakeCollection( property.Type );
                        backingField.Type = property.Type;
                    }
                    SupportQuantities( parameter.Items, parameter.name, property, codeType, parentNamespace, allParametersExpression, conditional.TrueStatements, parameterRef );
                    conditional.TrueStatements.Add( new CodeMethodReturnStatement( new CodePrimitiveExpression( true ) ) );
                    addParameter.Statements.Add( conditional );
                    codeType.Members.Add( property );
                    codeType.Members.Add( backingField );
                }

                if(allParametersExpression.Method.MethodName != "AllParameters")
                {
                    var allParameters = new CodeMemberMethod
                    {
                        Name = "AllParameters",
                        Attributes = MemberAttributes.Family | MemberAttributes.Override,
                        ReturnType = new CodeTypeReference( typeof( IEnumerable<> ).Name, new CodeTypeReference( typeof( Parameter ).Name ) )
                    };
                    allParameters.WriteInheritDoc();
                    allParameters.Statements.Add( new CodeMethodReturnStatement( allParametersExpression ) );
                    codeType.Members.Add( allParameters );
                }

                if(addParameter.Statements.Count > 0)
                {
                    addParameter.Statements.Add( new CodeMethodReturnStatement( new CodeMethodInvokeExpression( new CodeBaseReferenceExpression(), addParameter.Name, parameterRef ) ) );
                    codeType.Members.Add( addParameter );
                }
            }
        }

        private void SupportQuantities( object[] items, string parameter, CodeMemberProperty property, CodeTypeDeclaration codeType, CodeNamespace parentNamespace, CodeMethodInvokeExpression addExpression, CodeStatementCollection condition, CodeExpression parameterRef )
        {
            var quantities = items?.OfType<QuantityType>().Where( q => q.Unit != null );
            if(quantities != null && quantities.Any())
            {
                if(quantities.Count() == 1 && quantities.First().Unit.Length == 1)
                {
                    // unit is clear, no property needed
                    var quantity = quantities.First();
                    var unit = quantity.Unit[0];
                    var unitString = GetUnitString( quantity, unit );
                    addExpression.Parameters.Add( new CodeMethodInvokeExpression( new CodeTypeReferenceExpression( nameof( UnitSerializer ) ), nameof( UnitSerializer.FromString ), new CodePrimitiveExpression( unitString ) ) );
                }
                else
                {
                    var enumeration = quantities.Count() == 1
                        ? GetOrCreateEnumerationForQuantity( quantities.First(), parentNamespace )
                        : GenerateCustomUnitEnumeration( codeType, parameter, quantities, parentNamespace );

                    var unitProperty = new CodeMemberProperty
                    {
                        Name = property.Name + "Unit",
                        Type = enumeration,
                        Attributes = MemberAttributes.Public | MemberAttributes.Final,
                        HasGet = true,
                        HasSet = true,
                    };
                    unitProperty.WriteDocumentation( $"Gets or sets the unit for {parameter}" );

                    codeType.Members.Add( unitProperty );
                    codeType.Members.Add( CodeGenerationHelper.CreateBackingField( unitProperty ) );

                    var unitPropertyRef = new CodePropertyReferenceExpression( null, unitProperty.Name );

                    addExpression.Parameters.Add( new CodeMethodInvokeExpression(
                        new CodeTypeReferenceExpression( nameof( UnitSerializer ) ), nameof( UnitSerializer.FromEnum ), unitPropertyRef ) );
                    condition.Add( new CodeAssignStatement( unitPropertyRef, new CodeMethodInvokeExpression(
                        new CodeMethodReferenceExpression( new CodeTypeReferenceExpression( nameof( UnitSerializer ) ), nameof( UnitSerializer.ToEnum ), unitProperty.Type ),
                        new CodePropertyReferenceExpression( parameterRef, nameof( Parameter.Unit ) ) ) ) );
                }
            }
        }

        private string GetUnitString( QuantityType quantity, UnitType unit )
        {
            return UnitSerializer.ToString( new Unit
            {
                Label = unit.label,
                Quantity = quantity.name,
                Components = unit.SIUnit?.Select( SerializeUnit ).ToArray()
            } );
        }

        private CodeTypeReference GetOrCreateEnumerationForQuantity( QuantityType quantity, CodeNamespace codeNamespace )
        {
            var enumerationName = quantity.name.ToPascalCase() + "Unit";
            var enumeration = codeNamespace.Types.OfType<CodeTypeDeclaration>().FirstOrDefault( t => t.Name == enumerationName );
            if(enumeration == null)
            {
                enumeration = GenerateEnum( quantity.Unit.Select( un => new LiteralInfo( GetLabel( un ), GetUnitString( quantity, un ), un.label ) ), enumerationName, $"Denotes the different units for a {quantity.name}", "Unit" );
                codeNamespace.Types.Add( enumeration );
            }
            else
            {
                foreach(var unit in quantity.Unit)
                {
                    var fieldName = GetLabel( unit );
                    if(!enumeration.Members.OfType<CodeMemberField>().Any( f => f.Name == fieldName ))
                    {
                        enumeration.Members.Add( CreateEnumerationLiteral( "Unit", new LiteralInfo( fieldName, GetUnitString( quantity, unit ), unit.label ) ) );
                    }
                }
            }
            return new CodeTypeReference( enumerationName );
        }

        private string GetLabel( UnitType unit, string prefix = null )
        {
            return prefix + unit.label
                .Replace( "/", " per " )
                .ToPascalCase();
        }

        private CodeTypeReference GenerateCustomUnitEnumeration( CodeTypeDeclaration codeType, string parameter, IEnumerable<QuantityType> quantities, CodeNamespace codeNamespace )
        {
            var enumeration = GenerateEnum( from quant in quantities
                                            from unit in quant.Unit ?? Array.Empty<UnitType>()
                                            select new LiteralInfo( GetLabel( unit, $"{quant.name} in".ToPascalCase() ), null, $"{quant.name} in {unit.label}" ),
                                           GetTypeName( codeType.Name, parameter ) + "Unit", $"Denotes the different units of {parameter}" );
            codeNamespace.Types.Add( enumeration );
            return new CodeTypeReference( enumeration.Name );
        }

        private UnitComponent SerializeUnit( SIUnitType arg )
        {
            return new UnitComponent
            {
                Exponent = arg.exponent,
                Factor = arg.factor,
                Offset = arg.offset,
                Dimension = ConvertDimension( arg.Value )
            };
        }

        private SiUnit ConvertDimension( SIUnitNameList value )
        {
            switch(value)
            {
                case SIUnitNameList.Item1:
                    return SiUnit.One;
                case SIUnitNameList.m:
                    return SiUnit.Meter;
                case SIUnitNameList.kg:
                    return SiUnit.Kilograms;
                case SIUnitNameList.s:
                    return SiUnit.Seconds;
                case SIUnitNameList.A:
                    return SiUnit.Ampere;
                case SIUnitNameList.K:
                    return SiUnit.Kelvin;
                case SIUnitNameList.mol:
                    return SiUnit.Mole;
                case SIUnitNameList.cd:
                    return SiUnit.Candela;
                default:
                    throw new ArgumentOutOfRangeException( nameof( value ) );
            }
        }

        private struct LiteralInfo
        {
            public string Name { get; }

            public string SerializationName { get; }

            public string Documentation { get; }

            public LiteralInfo( string name, string serialization, string documentation )
            {
                Name = name;
                SerializationName = serialization;
                Documentation = documentation;
            }

            public LiteralInfo( AllowedValueType allowedValue )
            {
                SerializationName = allowedValue.Item?.ToString();
                Name = SerializationName?.ToPascalCase();
                Documentation = allowedValue.Documentation?.Value;
            }
        }

        private CodeTypeDeclaration GenerateEnum( IEnumerable<LiteralInfo> allowedValues, string name, string documentation, string serializationAttribute = "XmlEnum" )
        {
            var enumeration = new CodeTypeDeclaration
            {
                Name = name,
                Attributes = MemberAttributes.Public | MemberAttributes.Final,
                IsEnum = true
            };
            foreach(var literal in allowedValues)
            {
                CodeMemberField field = CreateEnumerationLiteral( serializationAttribute, literal );
                enumeration.Members.Add( field );
            }
            enumeration.WriteDocumentation( documentation );
            return enumeration;
        }

        private static CodeMemberField CreateEnumerationLiteral( string serializationAttribute, LiteralInfo literal )
        {
            var field = new CodeMemberField
            {
                Attributes = MemberAttributes.Public | MemberAttributes.Const,
                Name = literal.Name,
                Type = new CodeTypeReference( typeof( int ) )
            };
            field.WriteDocumentation( literal.Documentation );
            if(literal.SerializationName != null)
            {
                field.CustomAttributes.Add( new CodeAttributeDeclaration( serializationAttribute, new CodeAttributeArgument( new CodePrimitiveExpression( literal.SerializationName ) ) ) );
            }

            return field;
        }

        /// <summary>
        /// Adds the given categories
        /// </summary>
        /// <param name="blueprints">The categories</param>
        /// <param name="codeType">The code declaration to which properties should be added</param>
        /// <param name="ns">The namespace to which the generated code should be added to</param>
        public void AddCategories( CategoryBlueprintType[] blueprints, CodeTypeDeclaration codeType, CodeNamespace ns )
        {
            if(blueprints != null)
            {
                var addSubCategory = new CodeMemberMethod
                {
                    Name = "AddCategory",
                    Attributes = MemberAttributes.Family | MemberAttributes.Override,
                    ReturnType = new CodeTypeReference( typeof( bool ) )
                };
                addSubCategory.WriteInheritDoc();
                var createCategory = new CodeMemberMethod
                {
                    Name = "CreateCategory",
                    Attributes = MemberAttributes.Family | MemberAttributes.Override,
                    ReturnType = new CodeTypeReference( nameof( Category ) )
                };
                createCategory.WriteInheritDoc();
                var nameParameterRef = new CodeArgumentReferenceExpression( "name" );
                createCategory.Parameters.Add( new CodeParameterDeclarationExpression( typeof( string ), nameParameterRef.ParameterName ) );
                var parameterRef = new CodeArgumentReferenceExpression( "category" );
                addSubCategory.Parameters.Add( new CodeParameterDeclarationExpression( nameof( Category ), parameterRef.ParameterName ) );
                var parameterName = new CodePropertyReferenceExpression( parameterRef, nameof( Category.Name ) );

                var allSubcategoriesExpression = new CodeMethodInvokeExpression( new CodeBaseReferenceExpression(), "AllCategories" );
                foreach(var category in blueprints)
                {
                    var propertyName = category.name.ToPascalCase();
                    var isCollection = CodeGenerationHelper.MeansCollection( category.maxOccurs );
                    var property = new CodeMemberProperty
                    {
                        Name = propertyName,
                        Attributes = MemberAttributes.Public | MemberAttributes.Final,
                        HasGet = true,
                        HasSet = !isCollection,
                        Type = new CodeTypeReference( GetTypeName( codeType.Name, category.name ) )
                    };
                    property.WriteDocumentation( category.Documentation?.Value );
                    var conditional = new CodeConditionStatement( new CodeBinaryOperatorExpression(
                        parameterName, CodeBinaryOperatorType.ValueEquality, new CodePrimitiveExpression( category.name ) ) );
                    var valueExpression = new CodeCastExpression( property.Type, parameterRef );
                    addSubCategory.Statements.Add( conditional );
                    createCategory.Statements.Add( new CodeConditionStatement(
                        new CodeBinaryOperatorExpression( nameParameterRef, CodeBinaryOperatorType.ValueEquality, new CodePrimitiveExpression( category.name ) ),
                        new CodeMethodReturnStatement( new CodeObjectCreateExpression( property.Type ) ) ) );
                    var backingField = CodeGenerationHelper.CreateBackingField( property );
                    if(!isCollection)
                    {
                        property.HasSet = true;
                        allSubcategoriesExpression = new CodeMethodInvokeExpression( allSubcategoriesExpression, nameof( CategoryExtensions.Add ),
                            new CodePropertyReferenceExpression( null, propertyName ) );
                        conditional.TrueStatements.Add( new CodeAssignStatement( new CodePropertyReferenceExpression( null, propertyName ), valueExpression ) );
                    }
                    else
                    {
                        backingField.InitExpression = MakeCollectionImplementation( property.Type );
                        property.Type = MakeCollection( property.Type );
                        backingField.Type = property.Type;
                        allSubcategoriesExpression = new CodeMethodInvokeExpression( allSubcategoriesExpression, nameof( CategoryExtensions.AddAll ),
                            new CodePropertyReferenceExpression( null, propertyName ) );
                        conditional.TrueStatements.Add( new CodeMethodInvokeExpression( new CodePropertyReferenceExpression( null, propertyName ), nameof( ICollection<object>.Add ), valueExpression ) );
                    }
                    conditional.TrueStatements.Add( new CodeMethodReturnStatement( new CodePrimitiveExpression( true ) ) );
                    codeType.Members.Add( property );
                    codeType.Members.Add( backingField );

                    AddCategory( category, ns, codeType.Name );
                }

                if(allSubcategoriesExpression.Method.MethodName != "AllCategories")
                {
                    var allParameters = new CodeMemberMethod
                    {
                        Name = "AllCategories",
                        Attributes = MemberAttributes.Family | MemberAttributes.Override,
                        ReturnType = new CodeTypeReference( typeof( IEnumerable<> ).Name, new CodeTypeReference( typeof( Category ).Name ) )
                    };
                    allParameters.WriteInheritDoc();
                    allParameters.Statements.Add( new CodeMethodReturnStatement( allSubcategoriesExpression ) );
                    codeType.Members.Add( allParameters );
                }

                if(addSubCategory.Statements.Count > 0)
                {
                    addSubCategory.Statements.Add( new CodeMethodReturnStatement( new CodeMethodInvokeExpression( new CodeBaseReferenceExpression(), addSubCategory.Name, parameterRef ) ) );
                    codeType.Members.Add( addSubCategory );
                    createCategory.Statements.Add( new CodeMethodReturnStatement( new CodeMethodInvokeExpression( new CodeBaseReferenceExpression(), createCategory.Name, nameParameterRef ) ) );
                    codeType.Members.Add( createCategory );
                }
            }
        }


        internal void AddSeriesSets( SeriesSetBlueprintType[] blueprints, CodeNamespace ns, CodeTypeDeclaration codeType )
        {
            if(blueprints != null)
            {
                var addSeriesSet = new CodeMemberMethod
                {
                    Name = "AddSeriesSet",
                    Attributes = MemberAttributes.Family | MemberAttributes.Override,
                    ReturnType = new CodeTypeReference( typeof( bool ) )
                };
                addSeriesSet.WriteInheritDoc();
                var createSeriesSet = new CodeMemberMethod
                {
                    Name = "CreateSeriesSet",
                    Attributes = MemberAttributes.Family | MemberAttributes.Override,
                    ReturnType = new CodeTypeReference( nameof( SeriesSet ) )
                };
                createSeriesSet.WriteInheritDoc();
                var nameParameterRef = new CodeArgumentReferenceExpression( "name" );
                createSeriesSet.Parameters.Add( new CodeParameterDeclarationExpression( typeof( string ), nameParameterRef.ParameterName ) );
                var parameterRef = new CodeArgumentReferenceExpression( "seriesSet" );
                addSeriesSet.Parameters.Add( new CodeParameterDeclarationExpression( nameof( SeriesSet ), parameterRef.ParameterName ) );
                var parameterName = new CodePropertyReferenceExpression( parameterRef, nameof( SeriesSet.Name ) );

                var allSeriesExpression = new CodeMethodInvokeExpression( new CodeBaseReferenceExpression(), "AllSeriesSets" );
                foreach(var seriesSet in blueprints)
                {
                    var propertyName = seriesSet.name.ToPascalCase();
                    if(propertyName == codeType.Name)
                    {
                        propertyName += "Series";
                    }

                    var property = new CodeMemberProperty
                    {
                        Name = propertyName,
                        Attributes = MemberAttributes.Public | MemberAttributes.Final,
                        HasGet = true,
                        Type = new CodeTypeReference( GetTypeName( codeType.Name, seriesSet.name ) )
                    };
                    property.WriteDocumentation( seriesSet.Documentation?.Value );
                    var conditional = new CodeConditionStatement( new CodeBinaryOperatorExpression(
                        parameterName, CodeBinaryOperatorType.ValueEquality, new CodePrimitiveExpression( seriesSet.name ) ) );
                    var valueExpression = new CodeCastExpression( property.Type, parameterRef );

                    property.HasSet = true;
                    allSeriesExpression = new CodeMethodInvokeExpression( allSeriesExpression, nameof( CategoryExtensions.Add ),
                        new CodePropertyReferenceExpression( null, propertyName ) );
                    conditional.TrueStatements.Add( new CodeAssignStatement( new CodePropertyReferenceExpression( null, propertyName ), valueExpression ) );

                    conditional.TrueStatements.Add( new CodeMethodReturnStatement( new CodePrimitiveExpression( true ) ) );
                    addSeriesSet.Statements.Add( conditional );
                    createSeriesSet.Statements.Add( new CodeConditionStatement(
                        new CodeBinaryOperatorExpression( nameParameterRef, CodeBinaryOperatorType.ValueEquality, new CodePrimitiveExpression( seriesSet.name ) ),
                        new CodeMethodReturnStatement( new CodeObjectCreateExpression( property.Type ) ) ) );
                    codeType.Members.Add( property );
                    codeType.Members.Add( CodeGenerationHelper.CreateBackingField( property ) );

                    AddSeriesSet( seriesSet, ns, codeType.Name );
                }

                if(allSeriesExpression.Method.MethodName != "AllSeriesSets")
                {
                    var allParameters = new CodeMemberMethod
                    {
                        Name = "AllSeriesSets",
                        Attributes = MemberAttributes.Family | MemberAttributes.Override,
                        ReturnType = new CodeTypeReference( typeof( IEnumerable<> ).Name, new CodeTypeReference( typeof( SeriesSet ).Name ) )
                    };
                    allParameters.WriteInheritDoc();
                    allParameters.Statements.Add( new CodeMethodReturnStatement( allSeriesExpression ) );
                    codeType.Members.Add( allParameters );
                }

                if(addSeriesSet.Statements.Count > 0)
                {
                    addSeriesSet.Statements.Add( new CodeMethodReturnStatement( new CodeMethodInvokeExpression( new CodeBaseReferenceExpression(), addSeriesSet.Name, parameterRef ) ) );
                    codeType.Members.Add( addSeriesSet );
                    createSeriesSet.Statements.Add( new CodeMethodReturnStatement( new CodeMethodInvokeExpression( new CodeBaseReferenceExpression(), createSeriesSet.Name, nameParameterRef ) ) );
                    codeType.Members.Add( createSeriesSet );
                }
            }
        }

        /// <summary>
        /// Gets or sets the default code reference for integer blueprints
        /// </summary>
        public CodeTypeReference IntType
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the default code reference for float blueprints
        /// </summary>
        public CodeTypeReference FloatType
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the default code reference for numeric blueprints
        /// </summary>
        public CodeTypeReference NumericType
        {
            get;
            set;
        }

        private CodeTypeReference ConvertType( SeriesTypeType parameterType, bool isOptional )
        {
            CodeTypeReference baseType;
            switch(parameterType)
            {
                case SeriesTypeType.Int:
                    return IntType;
                case SeriesTypeType.Float:
                    return FloatType;
                case SeriesTypeType.Numeric:
                    return NumericType;
                case SeriesTypeType.String:
                    baseType = new CodeTypeReference( typeof( string ) );
                    break;
                case SeriesTypeType.Boolean:
                    baseType = isOptional ? new CodeTypeReference( typeof( bool? ) ) : new CodeTypeReference( typeof( bool ) );
                    break;
                case SeriesTypeType.DateTime:
                    baseType = isOptional ? new CodeTypeReference( typeof( DateTime? ) ) : new CodeTypeReference( typeof( DateTime ) );
                    break;
                case SeriesTypeType.EmbeddedXML:
                    baseType = new CodeTypeReference( typeof( XmlElement ) );
                    break;
                case SeriesTypeType.PNG:
                case SeriesTypeType.SVG:
                default:
                    throw new NotImplementedException();
            }
            return baseType;
        }

        internal CodeTypeReference MakeCollection( CodeTypeReference baseType )
        {
            return new CodeTypeReference( typeof( ICollection<> ).Name, baseType );
        }

        internal CodeExpression MakeCollectionImplementation( CodeTypeReference baseType )
        {
            return new CodeObjectCreateExpression( new CodeTypeReference( typeof( List<> ).Name, baseType ) );
        }
    }
}
