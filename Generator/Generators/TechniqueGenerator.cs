﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tecan.AnIML.Categories;
using Tecan.AnIML.ExperimentSteps;
using Tecan.AnIML.Generator.Helpers;
using Tecan.AnIML.Techniques.Serialization;

namespace Tecan.AnIML.Generator.Generators
{
    public class TechniqueGenerator
    {
        private readonly CategoryGenerator _categoryGenerator;

        public string Url
        {
            get;
            set;
        }

        public string Sha
        {
            get;
            set;
        }

        public TechniqueGenerator( CategoryGenerator categoryGenerator )
        {
            _categoryGenerator = categoryGenerator;
        }

        public CodeCompileUnit GenerateCode( TechniqueType technique, string ns )
        {
            var codeNamespace = new CodeNamespace
            {
                Name = string.IsNullOrEmpty( ns ) ? technique.name.ToPascalCase() : ns
            };

            var stepClass = CreateStepClass( technique );
            var templateClass = CreateTemplateClass( technique );
            codeNamespace.Types.Add( stepClass );
            codeNamespace.Types.Add( templateClass );

            if(technique.SampleRoleBlueprint != null || technique.ExperimentDataRoleBlueprint != null)
            {
                AddInfrastructure( technique, codeNamespace, stepClass, templateClass );
            }

            if(technique.MethodBlueprint != null)
            {
                AddMethod( technique, codeNamespace, stepClass, templateClass );
            }

            if(technique.ResultBlueprint != null)
            {
                AddResults( technique, codeNamespace, stepClass, templateClass );
            }

            var unit = new CodeCompileUnit();
            unit.Namespaces.Add( new CodeNamespace
            {
                Imports =
                {
                    new CodeNamespaceImport("System"),
                    new CodeNamespaceImport("System.Collections.Generic"),
                    new CodeNamespaceImport("System.Xml.Serialization"),
                    new CodeNamespaceImport("Tecan.AnIML"),
                    new CodeNamespaceImport("Tecan.AnIML.Categories"),
                    new CodeNamespaceImport("Tecan.AnIML.ExperimentSteps"),
                    new CodeNamespaceImport("Tecan.AnIML.Units")
                }
            } );
            unit.Namespaces.Add( codeNamespace );
            return unit;
        }

        private CodeTypeDeclaration CreateTemplateClass( TechniqueType technique )
        {
            var templateClass = new CodeTypeDeclaration
            {
                Name = technique.name.ToPascalCase() + "Template",
                Attributes = MemberAttributes.Public,
                BaseTypes =
                {
                    new CodeTypeReference(nameof(Template))
                }
            };
            templateClass.WriteDocumentation( technique.Documentation?.Value );

            templateClass.Members.Add( (new CodeConstructor
            {
                Attributes = MemberAttributes.Public,
                Statements =
                {
                    new CodeAssignStatement
                            {
                                Left = new CodePropertyReferenceExpression(null, nameof(Template.Technique)),
                                Right = new CodeObjectCreateExpression(nameof(Technique),
                                    new CodePrimitiveExpression(technique.name),
                                    new CodePrimitiveExpression(Url),
                                    new CodePrimitiveExpression(Sha))
                            }
                }
            }).WriteDocumentation( $"Creates a template for a {technique.name}" ) );

            return templateClass;
        }

        private CodeTypeDeclaration CreateStepClass( TechniqueType technique )
        {
            var stepClass = new CodeTypeDeclaration
            {
                Name = technique.name.ToPascalCase(),
                Attributes = MemberAttributes.Public,
                BaseTypes =
                {
                    new CodeTypeReference(nameof(ExperimentStep))
                }
            };

            var templateRef = new CodeArgumentReferenceExpression( "template" );
            stepClass.Members.Add( (new CodeConstructor
            {
                Attributes = MemberAttributes.Public,
                Parameters =
                {
                    new CodeParameterDeclarationExpression(stepClass.Name + "Template", templateRef.ParameterName)
                },
                Statements =
                {
                    new CodeConditionStatement
                    {
                        Condition = new CodeBinaryOperatorExpression(templateRef, CodeBinaryOperatorType.IdentityEquality, new CodePrimitiveExpression()),
                        TrueStatements =
                        {
                            new CodeAssignStatement
                            {
                                Left = new CodePropertyReferenceExpression(null, nameof(ExperimentStep.Technique)),
                                Right = new CodeObjectCreateExpression(nameof(Technique),
                                    new CodePrimitiveExpression(technique.name),
                                    new CodePrimitiveExpression(Url),
                                    new CodePrimitiveExpression(Sha))
                            }
                        },
                        FalseStatements =
                        {
                            new CodeAssignStatement(new CodePropertyReferenceExpression(null, nameof(ExperimentStep.TemplateUsed)), templateRef)
                        }
                    }
                }
            }).WriteDocumentation( $"Creates a new {technique.name}", parameters: new Dictionary<string, string>
            {
                [templateRef.ParameterName] = "The template to use or null"
            } ) );
            stepClass.Members.Add( (new CodeConstructor
            {
                Attributes = MemberAttributes.Public,
                ChainedConstructorArgs =
                {
                    new CodePrimitiveExpression()
                }
            }).WriteDocumentation( $"Creates a new {technique.name}" ) );

            stepClass.WriteDocumentation( technique.Documentation?.Value );
            return stepClass;
        }

        private void AddInfrastructure( TechniqueType technique, CodeNamespace codeNamespace, CodeTypeDeclaration stepClass, CodeTypeDeclaration templateClass )
        {
            var infrastructureClass = new CodeTypeDeclaration
            {
                Name = technique.name.ToPascalCase() + "Infrastructure",
                Attributes = MemberAttributes.Public,
                IsClass = true,
                IsPartial = true,
                BaseTypes =
                    {
                        new CodeTypeReference(nameof(Infrastructure))
                    }
            };
            infrastructureClass.WriteDocumentation( $"Denotes the samples and experiment data used for {technique.name}" );

            codeNamespace.Types.Add( infrastructureClass );

            var createInfrastructure = new CodeMemberMethod
            {
                Name = "CreateInfrastructure",
                Attributes = MemberAttributes.Family | MemberAttributes.Override,
                ReturnType = new CodeTypeReference( nameof( Infrastructure ) )
            };
            createInfrastructure.Statements.Add( new CodeMethodReturnStatement( new CodeObjectCreateExpression( infrastructureClass.Name ) ) );
            createInfrastructure.WriteInheritDoc();
            stepClass.Members.Add( createInfrastructure );
            templateClass.Members.Add( createInfrastructure );

            if(technique.SampleRoleBlueprint != null)
            {
                AddSampleRoles( technique, codeNamespace, infrastructureClass );
            }
            if(technique.ExperimentDataRoleBlueprint != null)
            {
                AddExperimentDataRoles( technique, codeNamespace, infrastructureClass );
            }
        }

        private void AddSampleRoles( TechniqueType technique, CodeNamespace codeNamespace, CodeTypeDeclaration infrastructureClass )
        {
            var addReferencedSample = new CodeMemberMethod
            {
                Name = "AddReferencedSample",
                Attributes = MemberAttributes.Family | MemberAttributes.Override,
                ReturnType = new CodeTypeReference( typeof( bool ) )
            };
            var referencedSampleRef = new CodeArgumentReferenceExpression( "reference" );
            var referencedSampleRoleRef = new CodePropertyReferenceExpression( referencedSampleRef, nameof( SampleReference.Role ) );
            addReferencedSample.Parameters.Add( new CodeParameterDeclarationExpression( nameof( SampleReference ), referencedSampleRef.ParameterName ) );
            addReferencedSample.WriteInheritDoc();
            var createSampleReference = new CodeMemberMethod
            {
                Name = "CreateSampleReference",
                Attributes = MemberAttributes.Family | MemberAttributes.Override,
                ReturnType = new CodeTypeReference( nameof( SampleReference ) )
            };
            var roleRef = new CodeArgumentReferenceExpression( "role" );
            createSampleReference.Parameters.Add( new CodeParameterDeclarationExpression( typeof( string ), roleRef.ParameterName ) );
            createSampleReference.WriteInheritDoc();
            var allReferencedSamples = new CodeMemberMethod
            {
                Name = "AllSampleReferences",
                Attributes = MemberAttributes.Family | MemberAttributes.Override,
                ReturnType = new CodeTypeReference( typeof( IEnumerable<> ).Name, new CodeTypeReference( nameof( SampleReference ) ) )
            };
            var allReferencedSamplesExpression = new CodeMethodInvokeExpression( new CodeBaseReferenceExpression(), allReferencedSamples.Name );
            allReferencedSamples.WriteInheritDoc();

            foreach(var sampleBlueprint in technique.SampleRoleBlueprint)
            {
                var isCollection = CodeGenerationHelper.MeansCollection( sampleBlueprint.maxOccurs );
                var property = new CodeMemberProperty
                {
                    Name = sampleBlueprint.name.ToPascalCase(),
                    Attributes = MemberAttributes.Public | MemberAttributes.Final,
                    Type = new CodeTypeReference( sampleBlueprint.name.ToPascalCase() ),
                    HasGet = true,
                    HasSet = !isCollection,
                };
                property.WriteDocumentation( sampleBlueprint.Documentation?.Value );
                infrastructureClass.Members.Add( property );
                var backingField = CodeGenerationHelper.CreateBackingField( property );
                infrastructureClass.Members.Add( backingField );

                var castedExpression = new CodeCastExpression( property.Type, referencedSampleRef );
                CodeStatement assignment;

                if(isCollection)
                {
                    backingField.InitExpression = _categoryGenerator.MakeCollectionImplementation( property.Type );
                    property.Type = _categoryGenerator.MakeCollection( property.Type );
                    backingField.Type = property.Type;
                    assignment = new CodeExpressionStatement( new CodeMethodInvokeExpression( new CodePropertyReferenceExpression( null, property.Name ), nameof( ICollection<string>.Add ), castedExpression ) );
                }
                else
                {
                    assignment = new CodeAssignStatement( new CodePropertyReferenceExpression( null, property.Name ), castedExpression );
                }

                addReferencedSample.Statements.Add( new CodeConditionStatement
                {
                    Condition = new CodeBinaryOperatorExpression( referencedSampleRoleRef, CodeBinaryOperatorType.ValueEquality, new CodePrimitiveExpression( sampleBlueprint.name ) ),
                    TrueStatements =
                        {
                            assignment,
                            new CodeMethodReturnStatement(new CodePrimitiveExpression(true))
                        }
                } );
                createSampleReference.Statements.Add( new CodeConditionStatement
                {
                    Condition = new CodeBinaryOperatorExpression( roleRef, CodeBinaryOperatorType.ValueEquality, new CodePrimitiveExpression( sampleBlueprint.name ) ),
                    TrueStatements =
                        {
                            new CodeMethodReturnStatement(new CodeObjectCreateExpression(sampleBlueprint.name.ToPascalCase()))
                        }
                } );
                allReferencedSamplesExpression = new CodeMethodInvokeExpression( allReferencedSamplesExpression, isCollection ? "AddAll" : "Add", new CodePropertyReferenceExpression( null, property.Name ) );

                AddSampleRoleClass( codeNamespace, sampleBlueprint );
            }

            addReferencedSample.Statements.Add( new CodeMethodReturnStatement( new CodeMethodInvokeExpression( new CodeBaseReferenceExpression(), addReferencedSample.Name, referencedSampleRef ) ) );
            createSampleReference.Statements.Add( new CodeMethodReturnStatement( new CodeMethodInvokeExpression( new CodeBaseReferenceExpression(), createSampleReference.Name, roleRef ) ) );
            allReferencedSamples.Statements.Add( new CodeMethodReturnStatement( allReferencedSamplesExpression ) );

            infrastructureClass.Members.Add( addReferencedSample );
            infrastructureClass.Members.Add( createSampleReference );
            infrastructureClass.Members.Add( allReferencedSamples );
        }

        private void AddExperimentDataRoles( TechniqueType technique, CodeNamespace codeNamespace, CodeTypeDeclaration infrastructureClass )
        {
            var addReferencedExperimentData = new CodeMemberMethod
            {
                Name = "AddExperimentDataReference",
                Attributes = MemberAttributes.Family | MemberAttributes.Override,
                ReturnType = new CodeTypeReference( typeof( bool ) )
            };
            var referencedDataRef = new CodeArgumentReferenceExpression( "reference" );
            var referencedDataRoleRef = new CodePropertyReferenceExpression( referencedDataRef, nameof( ExperimentDataReference.Role ) );
            addReferencedExperimentData.Parameters.Add( new CodeParameterDeclarationExpression( nameof( ExperimentDataReference ), referencedDataRef.ParameterName ) );
            addReferencedExperimentData.WriteInheritDoc();
            var createExperimentDataReference = new CodeMemberMethod
            {
                Name = "CreateExperimentDataReference",
                Attributes = MemberAttributes.Family | MemberAttributes.Override,
                ReturnType = new CodeTypeReference( nameof( ExperimentDataReference ) )
            };
            var roleRef = new CodeArgumentReferenceExpression( "role" );
            createExperimentDataReference.Parameters.Add( new CodeParameterDeclarationExpression( typeof( string ), roleRef.ParameterName ) );
            createExperimentDataReference.WriteInheritDoc();
            var allReferencedExperimentDatas = new CodeMemberMethod
            {
                Name = "AllExperimentDataReferences",
                Attributes = MemberAttributes.Family | MemberAttributes.Override,
                ReturnType = new CodeTypeReference( typeof( IEnumerable<> ).Name, new CodeTypeReference( nameof( ExperimentDataReference ) ) )
            };
            var allReferencedExperimentDataExpression = new CodeMethodInvokeExpression( new CodeBaseReferenceExpression(), allReferencedExperimentDatas.Name );
            allReferencedExperimentDatas.WriteInheritDoc();

            foreach(var experimentDataBlueprint in technique.ExperimentDataRoleBlueprint)
            {
                var isCollection = CodeGenerationHelper.MeansCollection( experimentDataBlueprint.maxOccurs );
                var property = new CodeMemberProperty
                {
                    Name = experimentDataBlueprint.name.ToPascalCase(),
                    Attributes = MemberAttributes.Public | MemberAttributes.Final,
                    Type = new CodeTypeReference( nameof( ExperimentDataReference ) ),
                    HasGet = true,
                    HasSet = !isCollection,
                };
                property.WriteDocumentation( experimentDataBlueprint.Documentation?.Value );
                infrastructureClass.Members.Add( property );
                var backingField = CodeGenerationHelper.CreateBackingField( property );
                infrastructureClass.Members.Add( backingField );

                var castedExpression = new CodeCastExpression( property.Type, referencedDataRef );
                CodeStatement assignment;

                if(isCollection)
                {
                    backingField.InitExpression = _categoryGenerator.MakeCollectionImplementation( property.Type );
                    property.Type = _categoryGenerator.MakeCollection( property.Type );
                    backingField.Type = property.Type;
                    assignment = new CodeExpressionStatement( new CodeMethodInvokeExpression( new CodePropertyReferenceExpression( null, property.Name ), nameof( ICollection<string>.Add ), castedExpression ) );
                }
                else
                {
                    assignment = new CodeAssignStatement( new CodePropertyReferenceExpression( null, property.Name ), castedExpression );
                }

                addReferencedExperimentData.Statements.Add( new CodeConditionStatement
                {
                    Condition = new CodeBinaryOperatorExpression( referencedDataRoleRef, CodeBinaryOperatorType.ValueEquality, new CodePrimitiveExpression( experimentDataBlueprint.name ) ),
                    TrueStatements =
                        {
                            assignment,
                            new CodeMethodReturnStatement(new CodePrimitiveExpression(true))
                        }
                } );
                createExperimentDataReference.Statements.Add( new CodeConditionStatement
                {
                    Condition = new CodeBinaryOperatorExpression( roleRef, CodeBinaryOperatorType.ValueEquality, new CodePrimitiveExpression( experimentDataBlueprint.name ) ),
                    TrueStatements =
                        {
                            new CodeMethodReturnStatement(new CodeObjectCreateExpression(experimentDataBlueprint.name.ToPascalCase()))
                        }
                } );
                allReferencedExperimentDataExpression = new CodeMethodInvokeExpression( allReferencedExperimentDataExpression, isCollection ? "AddAll" : "Add", new CodePropertyReferenceExpression( null, property.Name ) );
            }

            addReferencedExperimentData.Statements.Add( new CodeMethodReturnStatement( new CodeMethodInvokeExpression( new CodeBaseReferenceExpression(), addReferencedExperimentData.Name, referencedDataRef ) ) );
            createExperimentDataReference.Statements.Add( new CodeMethodReturnStatement( new CodeMethodInvokeExpression( new CodeBaseReferenceExpression(), createExperimentDataReference.Name, roleRef ) ) );
            allReferencedExperimentDatas.Statements.Add( new CodeMethodReturnStatement( allReferencedExperimentDataExpression ) );

            infrastructureClass.Members.Add( addReferencedExperimentData );
            infrastructureClass.Members.Add( createExperimentDataReference );
            infrastructureClass.Members.Add( allReferencedExperimentDatas );
        }

        private void AddResults( TechniqueType technique, CodeNamespace codeNamespace, CodeTypeDeclaration stepClass, CodeTypeDeclaration templateClass )
        {
            var createMethod = new CodeMemberMethod
            {
                Name = "CreateResult",
                Attributes = MemberAttributes.Family | MemberAttributes.Override,
                ReturnType = new CodeTypeReference( nameof( Result ) )
            };
            var addResultMethod = new CodeMemberMethod
            {
                Name = "AddResult",
                Attributes = MemberAttributes.Family | MemberAttributes.Override,
                ReturnType = new CodeTypeReference( typeof( bool ) )
            };
            var nameRef = new CodeArgumentReferenceExpression( "name" );
            createMethod.Parameters.Add( new CodeParameterDeclarationExpression( typeof( string ), nameRef.ParameterName ) );
            createMethod.WriteInheritDoc();
            var resultRef = new CodeArgumentReferenceExpression( "result" );
            addResultMethod.Parameters.Add( new CodeParameterDeclarationExpression( nameof( Result ), resultRef.ParameterName ) );
            addResultMethod.WriteInheritDoc();
            var baseRef = new CodeBaseReferenceExpression();
            var allResults = new CodeMethodInvokeExpression( baseRef, "AllResults" );

            var allResultsMethod = new CodeMemberMethod
            {
                Name = allResults.Method.MethodName,
                Attributes = MemberAttributes.Family | MemberAttributes.Override,
                ReturnType = new CodeTypeReference( typeof( IEnumerable<Result> ) )
            };
            allResultsMethod.WriteInheritDoc();
            foreach(var result in technique.ResultBlueprint)
            {
                var resultType = AddResult( codeNamespace, createMethod, nameRef, result );
                var type = new CodeTypeReference( resultType.Name );
                var propertyName = result.name.ToPascalCase();
                var isCollection = result.maxOccurs != null && result.maxOccurs != "1";

                if(isCollection)
                {
                    propertyName += "s";
                    type = new CodeTypeReference( typeof( IList<object> ).Name, type );
                }


                if(propertyName == stepClass.Name)
                {
                    propertyName += "Result";
                }

                var field = new CodeMemberField
                {
                    Name = "_" + char.ToLowerInvariant( propertyName[0] ) + propertyName.Substring( 1 ),
                    Type = type,
                    Attributes = MemberAttributes.Private
                };
                var property = new CodeMemberProperty
                {
                    Name = propertyName,
                    Type = type,
                    Attributes = MemberAttributes.Public | MemberAttributes.Final,
                    HasGet = true,
                    HasSet = !isCollection,
                    GetStatements =
                    {
                        new CodeMethodReturnStatement(new CodeFieldReferenceExpression(null, field.Name))
                    }
                };
                property.WriteDocumentation( result.Documentation?.Value ?? $"(The technique did not provide documentation for the result {result.name})" );

                var propertyRef = new CodePropertyReferenceExpression( null, propertyName );

                if(isCollection)
                {
                    allResults = new CodeMethodInvokeExpression( allResults, "AddAll", propertyRef );
                    field.InitExpression = new CodeObjectCreateExpression( new CodeTypeReference( typeof( List<object> ).Name, new CodeTypeReference( resultType.Name ) ) );
                }
                else
                {
                    allResults = new CodeMethodInvokeExpression( allResults, "Add", propertyRef );
                    property.SetStatements.Add( new CodeAssignStatement( new CodeFieldReferenceExpression( null, field.Name ), new CodePropertySetValueReferenceExpression() ) );
                }

                stepClass.Members.Add( field );
                templateClass.Members.Add( field );
                stepClass.Members.Add( property );
                templateClass.Members.Add( property );

                addResultMethod.Statements.Add( new CodeConditionStatement(
                    new CodeBinaryOperatorExpression( new CodePropertyReferenceExpression( resultRef, nameof( Result.Name ) ), CodeBinaryOperatorType.ValueEquality, new CodePrimitiveExpression( result.name ) ),
                    isCollection
                        ? (CodeStatement)new CodeExpressionStatement( new CodeMethodInvokeExpression( propertyRef, "Add", new CodeCastExpression( resultType.Name, resultRef ) ) )
                        : new CodeAssignStatement( propertyRef, new CodeCastExpression( resultType.Name, resultRef ) ),
                    new CodeMethodReturnStatement( new CodePrimitiveExpression( true ) ) ) );
            }

            if(createMethod.Statements.Count > 0)
            {
                createMethod.Statements.Add( new CodeMethodReturnStatement( new CodeMethodInvokeExpression( baseRef, createMethod.Name, nameRef ) ) );
                addResultMethod.Statements.Add( new CodeMethodReturnStatement( new CodeMethodInvokeExpression( baseRef, addResultMethod.Name, resultRef ) ) );
                stepClass.Members.Add( createMethod );
                templateClass.Members.Add( createMethod );
                stepClass.Members.Add( addResultMethod );
                templateClass.Members.Add( addResultMethod );
                allResultsMethod.Statements.Add( new CodeMethodReturnStatement( allResults ) );
                stepClass.Members.Add( allResultsMethod );
                templateClass.Members.Add( allResultsMethod );
            }
        }

        private CodeTypeDeclaration AddResult( CodeNamespace codeNamespace, CodeMemberMethod createMethod, CodeArgumentReferenceExpression nameRef, ResultBlueprintType result )
        {
            var resultClass = new CodeTypeDeclaration
            {
                Name = result.name.ToPascalCase(),
                Attributes = MemberAttributes.Public
            };
            resultClass.BaseTypes.Add( nameof( Result ) );
            resultClass.WriteDocumentation( result.Documentation?.Value );

            var constructor = new CodeConstructor { Attributes = MemberAttributes.Public };
            constructor.BaseConstructorArgs.Add( new CodePrimitiveExpression( result.name ) );
            resultClass.Members.Add( constructor );

            codeNamespace.Types.Add( resultClass );

            _categoryGenerator.AddCategories( result.CategoryBlueprint, resultClass, codeNamespace );

            if(result.SeriesSetBlueprint != null)
            {
                var seriesType = _categoryGenerator.AddSeriesSet( result.SeriesSetBlueprint, codeNamespace, resultClass.Name );
                var propertyName = result.SeriesSetBlueprint.name.ToPascalCase();
                if(propertyName == resultClass.Name)
                {
                    propertyName += "SeriesSet";
                }
                var baseRef = new CodeBaseReferenceExpression();
                resultClass.Members.Add( new CodeMemberProperty
                {
                    Name = propertyName,
                    Attributes = MemberAttributes.Public | MemberAttributes.Final,
                    Type = new CodeTypeReference( seriesType.Name ),
                    HasGet = true,
                    HasSet = true,
                    GetStatements =
                    {
                        new CodeMethodReturnStatement(new CodeCastExpression(seriesType.Name, new CodePropertyReferenceExpression(baseRef, nameof(Result.SeriesSet))))
                    },
                    SetStatements =
                    {
                        new CodeAssignStatement(new CodePropertyReferenceExpression(baseRef, nameof(Result.SeriesSet)), new CodePropertySetValueReferenceExpression())
                    }
                }.WriteDocumentation( result.SeriesSetBlueprint.Documentation?.Value ) );

                resultClass.Members.Add( new CodeMemberMethod
                {
                    Name = "CreateSeriesSet",
                    Attributes = MemberAttributes.Family | MemberAttributes.Override,
                    ReturnType = new CodeTypeReference( nameof( SeriesSet ) ),
                    Parameters =
                    {
                        new CodeParameterDeclarationExpression(typeof(string), "name")
                    },
                    Statements =
                    {
                        new CodeConditionStatement(
                            new CodeBinaryOperatorExpression(nameRef, CodeBinaryOperatorType.ValueEquality, new CodePrimitiveExpression(result.SeriesSetBlueprint.name)),
                            new CodeMethodReturnStatement(new CodeObjectCreateExpression(seriesType.Name))),
                        new CodeMethodReturnStatement(new CodeMethodInvokeExpression(baseRef, "CreateSeriesSet", nameRef))
                    }
                }.WriteInheritDoc() );
            }

            createMethod.Statements.Add( new CodeConditionStatement(
                new CodeBinaryOperatorExpression( nameRef, CodeBinaryOperatorType.ValueEquality, new CodePrimitiveExpression( result.name ) ),
                new CodeMethodReturnStatement( new CodeObjectCreateExpression( resultClass.Name ) ) ) );

            return resultClass;
        }

        private void AddMethod( TechniqueType technique, CodeNamespace codeNamespace, CodeTypeDeclaration stepClass, CodeTypeDeclaration templateClass )
        {
            var methodClass = new CodeTypeDeclaration
            {
                Attributes = MemberAttributes.Public,
                Name = technique.name.ToPascalCase() + "Method"
            };
            methodClass.BaseTypes.Add( nameof( Method ) );
            methodClass.WriteDocumentation( $"Denotes the method properties for {technique.name}" );

            codeNamespace.Types.Add( methodClass );

            _categoryGenerator.AddCategories( technique.MethodBlueprint, methodClass, codeNamespace );

            var createMethod = new CodeMemberMethod
            {
                Name = "CreateMethod",
                Attributes = MemberAttributes.Family | MemberAttributes.Override,
                ReturnType = new CodeTypeReference( nameof( Method ) )
            };
            createMethod.WriteInheritDoc();
            createMethod.Statements.Add( new CodeMethodReturnStatement( new CodeObjectCreateExpression( methodClass.Name ) ) );
            stepClass.Members.Add( createMethod );
            templateClass.Members.Add( createMethod );
        }

        private void AddSampleRoleClass( CodeNamespace codeNamespace, SampleRoleBlueprintType sampleBlueprint )
        {
            var sampleRoleClass = new CodeTypeDeclaration
            {
                Name = sampleBlueprint.name.ToPascalCase(),
                Attributes = MemberAttributes.Public,
                IsClass = true,
                IsPartial = true,
                BaseTypes =
                        {
                            new CodeTypeReference(nameof(SampleReference))
                        }
            };
            sampleRoleClass.WriteDocumentation( sampleBlueprint.Documentation?.Value );
            codeNamespace.Types.Add( sampleRoleClass );

            sampleRoleClass.Members.Add( new CodeConstructor
            {
                Attributes = MemberAttributes.Public,
                BaseConstructorArgs =
                {
                    new CodePrimitiveExpression(sampleBlueprint.name),
                },
                Statements =
                {
                    new CodeAssignStatement
                    {
                        Left = new CodePropertyReferenceExpression(null, nameof(SampleReference.Purpose)),
                        Right = new CodeFieldReferenceExpression(new CodeTypeReferenceExpression(nameof(Purpose)), sampleBlueprint.samplePurpose.ToString().ToPascalCase())
                    }
                }
            } );

            if(sampleBlueprint.CategoryBlueprint != null)
            {
                _categoryGenerator.AddCategories( sampleBlueprint.CategoryBlueprint, sampleRoleClass, codeNamespace );
            }
        }
    }
}
