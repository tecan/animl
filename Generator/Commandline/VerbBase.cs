﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tecan.AnIML.Generator.Commandline
{
    /// <summary>
    /// Denotes an abstract command line verb
    /// </summary>
    public abstract class VerbBase
    {
        /// <summary>
        /// Actually executes the verb
        /// </summary>
        protected abstract void ExecuteCore();

        /// <summary>
        /// Wraps the execution of the verb and catches exceptions
        /// </summary>
        /// <returns>The application exit code</returns>
        public int Execute()
        {
            try
            {
                ExecuteCore();
                return 0;
            }
            catch(Exception ex)
            {
                Console.Error.WriteLine( ex.Message );
                return 1;
            }
        }
    }
}
