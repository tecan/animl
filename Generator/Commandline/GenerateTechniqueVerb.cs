﻿using CommandLine;
using System;
using System.CodeDom;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Tecan.AnIML.Generator.Generators;
using Tecan.AnIML.Generator.Helpers;
using Tecan.AnIML.Serialization;

namespace Tecan.AnIML.Generator.Commandline
{
    /// <summary>
    /// Denotes a code generator verb to generate the serialization code for a specific AnIML technique
    /// </summary>
    [Verb( "generate-serializer", HelpText = "Generates the code for a given AniML technique definition" )]
    public class GenerateTechniqueVerb : VerbBase
    {
        /// <summary>
        /// The path to the technique
        /// </summary>
        [Value( 0, Required = true, HelpText = "The path to the AniML technique definition" )]
        public string Technique
        {
            get;
            set;
        }

        /// <summary>
        /// The target where the code should be generated to
        /// </summary>
        [Value( 1, Required = true, HelpText = "The path where the code should be generated to" )]
        public string Target
        {
            get;
            set;
        }

        /// <summary>
        /// The namespace of the generated code
        /// </summary>
        [Option( 'n', "namespace", Required = false, Default = "Tecan.AnIML", HelpText = "Denotes the namespace to be used in the generated code" )]
        public string Namespace
        {
            get;
            set;
        }

        /// <summary>
        /// Overrides the hash of the technique document
        /// </summary>
        [Option( "sha", Required = false, HelpText = "Overrides the hash of the technique document" )]
        public string Sha
        {
            get;
            set;
        }

        /// <summary>
        /// The Url that should be used for the technique, defaults to file location
        /// </summary>
        [Option( 'u', "url", Required = false, HelpText = "The Url that should be used for the technique, defaults to file location" )]
        public string Uri
        {
            get;
            set;
        }

        /// <summary>
        /// The static type to be used for integer blueprints. Defaults to none (generate object properties and decide at runtime)
        /// </summary>
        [Option( 'i', "integer-type", Required = false, HelpText = "The static type to be used for integer blueprints. Defaults to none (generate object properties and decide at runtime)" )]
        public string IntegerType
        {
            get;
            set;
        }

        /// <summary>
        /// The static type to be used for numeric blueprints. Defaults to none (generate object properties and decide at runtime)
        /// </summary>
        [Option( 'z', "numeric-type", Required = false, HelpText = "The static type to be used for numeric blueprints. Defaults to none (generate object properties and decide at runtime)" )]
        public string NumericType
        {
            get;
            set;
        }

        /// <summary>
        /// The static type to be used for float blueprints. Defaults to none (generate object properties and decide at runtime)
        /// </summary>
        [Option( 'f', "float-type", Required = false, HelpText = "The static type to be used for float blueprints. Defaults to none (generate object properties and decide at runtime)" )]
        public string FloatType
        {
            get;
            set;
        }

        /// <inheritdoc />
        protected override void ExecuteCore()
        {
            var technique = TechniqueSerializer.Load( Technique );
            Console.WriteLine( "Technique loaded" );
            var codeGenerator = new TechniqueGenerator( new CategoryGenerator
            {
                FloatType = CreateCodeTypeReference( FloatType, typeof( float ), typeof( double ) ),
                IntType = CreateCodeTypeReference( IntegerType, typeof( int ), typeof( long ) ),
                NumericType = CreateCodeTypeReference( NumericType, typeof( float ), typeof( double ), typeof( int ), typeof( long ) ),
            } );
            codeGenerator.Url = Uri ?? new Uri( Path.GetFullPath( Technique ) ).AbsoluteUri;
            codeGenerator.Sha = Sha ?? CalculateSha( Technique );
            var generatedCode = codeGenerator.GenerateCode( technique, Namespace );
            Console.WriteLine( "Code generation complete" );
            CodeGenerationHelper.GenerateCSharp( generatedCode, Target );
        }

        private CodeTypeReference CreateCodeTypeReference( string spec, params Type[] allowedTypes )
        {
            if(string.IsNullOrEmpty( spec ))
            {
                return null;
            }
            var type = allowedTypes.FirstOrDefault( t => string.Equals( t.Name, spec, StringComparison.OrdinalIgnoreCase )
                        || string.Equals( t.FullName, spec, StringComparison.OrdinalIgnoreCase ) );
            if(type != null)
            {
                return new CodeTypeReference( type );
            }
            throw new InvalidOperationException( $"{spec} is not supported. Allowed types are {string.Join( ", ", allowedTypes.Select( t => t.Name ) )}" );
        }

        private string CalculateSha( string technique )
        {
            using(var fs = File.OpenRead( technique ))
            {
                var sha = new SHA256Managed();
                var hash = sha.ComputeHash( fs );
                return BitConverter.ToString( hash ).Replace( "-", string.Empty ).ToLowerInvariant();
            }
        }
    }
}
