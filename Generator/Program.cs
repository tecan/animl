﻿using CommandLine;
using System;
using System.Collections.Generic;
using System.Text;
using Tecan.AnIML.Generator.Commandline;

namespace Tecan.AnIML.Generator
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Environment.ExitCode = Parser.Default.ParseArguments( args, typeof( GenerateTechniqueVerb ) )
                .MapResult( ( VerbBase verb ) => verb.Execute(), _ => 1 );
        }
    }
}
