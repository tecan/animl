﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Tecan.AnIML.Generator.Generators;
using Tecan.AnIML.Generator.Helpers;
using Tecan.AnIML.Serialization;

namespace Generator.Tests.Chromatography
{
    [TestFixture]
    internal class ChromatographyFixture : CodeGeneratorFixtureBase
    {
        public override string Name => "Chromatography";

        public override string TechniqueName => "chromatography.atdd";
    }
}
