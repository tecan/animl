﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using Tecan.AnIML.Generator.Generators;
using Tecan.AnIML.Generator.Helpers;
using Tecan.AnIML.Serialization;

namespace Generator.Tests
{
    public abstract class CodeGeneratorFixtureBase
    {
        public abstract string Name { get; }

        public abstract string TechniqueName { get; }

        [OneTimeSetUp]
        public void SetCurrentDirectory()
        {
            Environment.CurrentDirectory = Path.Combine( Path.GetDirectoryName( typeof( CodeGeneratorFixtureBase ).Assembly.Location ), Name );
        }

        [Test]
        public void GenerateTechnique_Succeeds()
        {
            var technique = TechniqueSerializer.Load( TechniqueName, xxe: true );
            var codeGenerator = new TechniqueGenerator( new CategoryGenerator() );

            var generatedCode = codeGenerator.GenerateCode( technique, "Tecan.AnIML.Test" );

            string csharp = Generate( generatedCode );
            string reference = File.ReadAllText( "Reference.cs" );
            Assert.That( csharp, Is.Not.Empty );
            Assert.That( EliminateWhitespaces( EliminateUnitXml( csharp ) ), Is.EqualTo( EliminateWhitespaces( EliminateUnitXml( reference ) ) ) );
        }

        private static string Generate( System.CodeDom.CodeCompileUnit generatedCode )
        {
            var tmpFile = Path.GetTempFileName();
            try
            {
                CodeGenerationHelper.GenerateCSharp( generatedCode, tmpFile );
                var csharp = File.ReadAllText( tmpFile );
                return csharp;
            }
            finally
            {
                File.Delete( tmpFile );
            }
        }

        private static string EliminateUnitXml( string input )
        {
            return Regex.Replace( input, "\\<Unit[^\\]]+\\>", "..." );
        }

        private static string EliminateWhitespaces( string input )
        {
            return Regex.Replace( Regex.Replace( input, "//[^/].*", string.Empty ), @"\s+", string.Empty );
        }
    }
}
